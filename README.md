# timcl
A miscellaneous group of OpenCL benchmarks
(stuff that other benchmarks miss or I have a gripe with)


## timcl-rand
Various random GPU random number generators

## timcl-gops
Throughputs on various operations

## timcl-mtpt. Memory Throughput
A bandwidth test.


# TODO
...
* rename gops to alu-tpt (make alu-lat separate)
*