#ifndef TYPE
#error must define TYPE as the floating point type
#endif

#if (SIZEOF_TYPE == 2)
#pragma OPENCL EXTENSION cl_khr_fp16 : enable
#elif (SIZEOF_TYPE == 8)
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif


#if (0)
    #define EXP(a) native_exp(a)
    #define LOG(a) native_log(a)
    #define SQRT(a) native_sqrt(a)
#else
// -cl-fast-relaxed-math __FAST_RELAXED_MATH__
    #define EXP(a) exp(a)
    #define LOG(a) log(a)
    #define SQRT(a) sqrt(a)
#endif

///////////////////////////////////////////////////////////////////////////////
// Rational approximation of cumulative normal distribution function
///////////////////////////////////////////////////////////////////////////////
TYPE CND(TYPE d) {
    const TYPE       A1 = (TYPE)0.31938153f;
    const TYPE       A2 = (TYPE)-0.356563782f;
    const TYPE       A3 = (TYPE)1.781477937f;
    const TYPE       A4 = (TYPE)-1.821255978f;
    const TYPE       A5 = (TYPE)1.330274429f;
    const TYPE RSQRT2PI = (TYPE)0.39894228040143267793994605993438f;

    TYPE
        K = (TYPE)1.0f / ((TYPE)1.0f + (TYPE)0.2316419f*fabs(d));

    TYPE
        cnd = RSQRT2PI*EXP(-(TYPE)(0.5f)*d*d)*
          (K*(A1 + K*(A2 + K*(A3 + K*(A4 + K*A5)))));

    if(d > 0)
        cnd = (TYPE)1.0f - cnd;

    return cnd;
}


///////////////////////////////////////////////////////////////////////////////
// Black-Scholes formula for both call and put
///////////////////////////////////////////////////////////////////////////////
void BlackScholesStep(
    __global TYPE *call, // Call option price
    __global TYPE *put,  // Put option price
    TYPE S,              // Current stock price
    TYPE X,              // Option strike price
    TYPE T,              // Option years
    TYPE R,              // Riskless rate of return
    TYPE V)              // Stock volatility
{
    TYPE sqrtT = SQRT(T);
    TYPE    d1 = (LOG(S/X) + (R + (TYPE)(0.5f)*V*V)*T)/(V*sqrtT);
    TYPE    d2 = d1 - V*sqrtT;
    TYPE CNDD1 = CND(d1);
    TYPE CNDD2 = CND(d2);

    // Calculate Call and Put simultaneously
    TYPE expRT = EXP(-R*T);
    *call = (S*CNDD1 - X*expRT*CNDD2);
    *put  = (X*expRT*((TYPE)1.0f - CNDD2) - S*((TYPE)1.0f - CNDD1));
}



__kernel void BlackScholesBatch(
    __global TYPE *d_Call, // Call option price
    __global TYPE *d_Put,  // Put option price
    __global TYPE *d_S,    // Current stock price
    __global TYPE *d_X,    // Option strike price
    __global TYPE *d_T,    // Option years
    TYPE R,                // Riskless rate of return
    TYPE V,                // Stock volatility
    uint optN)
{
    for(uint opt = get_global_id(0); opt < optN; opt += get_global_size(0))
        BlackScholesStep(
            &d_Call[opt],
            &d_Put[opt],
            d_S[opt],
            d_X[opt],
            d_T[opt],
            R,
            V
        );
}
