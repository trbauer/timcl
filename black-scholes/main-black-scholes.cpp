#include "mincl.hpp"

#include <cstdlib>
#include <iomanip>

using namespace mincl;

///////////////////////////////////////////////////////////////////////////////
// Rational approximation of cumulative normal distribution function
///////////////////////////////////////////////////////////////////////////////
template <typename T>
static T CND(T d) {
    const T       A1 = 0.31938153;
    const T       A2 = -0.356563782;
    const T       A3 = 1.781477937;
    const T       A4 = -1.821255978;
    const T       A5 = 1.330274429;
    const T RSQRT2PI = 0.39894228040143267793994605993438;

    T K = 1.0 / (1.0 + 0.2316419*std::fabs(d));

    T
      cnd = RSQRT2PI*std::exp(-0.5*d*d)*
        (K*(A1 + K*(A2 + K*(A3 + K*(A4 + K*A5)))));

    if (d > 0)
        cnd = 1.0 - cnd;

    return cnd;
}

///////////////////////////////////////////////////////////////////////////////
// Black-Scholes formula for both call and put
///////////////////////////////////////////////////////////////////////////////
/*
template <typename REAL>
static void BlackScholesStepCPU(
    REAL& call, // Call option price
    REAL& put,  // Put option price
    REAL Sf,    // Current stock price
    REAL Xf,    // Option strike price
    REAL Tf,    // Option years
    REAL Rf,    // Riskless rate of return
    REAL Vf     // Stock volatility
)
{
    REAL S = Sf, X = Xf, T = Tf, R = Rf, V = Vf;

    REAL sqrtT = std::sqrt(T);
    REAL    d1 = (std::log(S / X) + (R + 0.5 * V * V) * T) / (V * sqrtT);
    REAL    d2 = d1 - V * sqrtT;
    REAL CNDD1 = CND(d1);
    REAL CNDD2 = CND(d2);

    //Calculate Call and Put simultaneously
    REAL expRT = std::exp(- R * T);
    call = (REAL)(S * CNDD1 - X * expRT * CNDD2);
    put  = (REAL)(X * expRT * (1.0 - CNDD2) - S * (1.0 - CNDD1));
}
*/
template <typename REAL>
static void BlackScholesStepCPU(
    REAL& call, // Call option price
    REAL& put,  // Put option price
    REAL Sf,    // Current stock price
    REAL Xf,    // Option strike price
    REAL Tf,    // Option years
    REAL Rf,    // Riskless rate of return
    REAL Vf     // Stock volatility
)
{
    double S = Sf, X = Xf, T = Tf, R = Rf, V = Vf;

    double sqrtT = std::sqrt(T);
    double    d1 = (std::log(S/X) + (R + 0.5*V*V)*T) / (V*sqrtT);
    double    d2 = d1 - V*sqrtT;
    double CNDD1 = CND(d1);
    double CNDD2 = CND(d2);

    //Calculate Call and Put simultaneously
    double expRT = std::exp(-R*T);
    call = (float)(S*CNDD1 - X*expRT*CNDD2);
    put = (float)(X*expRT * (1.0 - CNDD2) - S*(1.0 - CNDD1));
}



///////////////////////////////////////////////////////////////////////////////
// Process an array of optN options
///////////////////////////////////////////////////////////////////////////////
template <typename T>
static void BlackScholesBatchCPU(
    T *h_Call, // Call option price
    T *h_Put,  // Put option price
    const T *h_S,    // Current stock price
    const T *h_X,    // Option strike price
    const T *h_T,    // Option years
    T R,       // Riskless rate of return
    T V,       // Stock volatility
    unsigned optionCount)
{
    for(unsigned i = 0; i < optionCount; i++)
        BlackScholesStepCPU<T>(
            h_Call[i],
            h_Put[i],
            h_S[i],
            h_X[i],
            h_T[i],
            R,
            V
        );
}


///////////////////////////////////////////////////////////////////////////////
// OPT_COUNT gets a little heavy for the CPU to validate
static const int64_t DEFAULT_OPTION_COUNT = 16*1024*1024;
static const int64_t DEFAULT_GLOBAL_SIZE = 64*1024;

static const float        R = 0.02f;
static const float        V = 0.30f;
static const float        INIT_PUT_CALL = -1.0f;

static float randFloat(float low, float high) {
  auto r = rand();
  float t = (float)r / (float)RAND_MAX;
  return (1.0f - t) * low + t * high;
}

template <typename T>
static T absHelper(T t);
template <> static double absHelper(double x) {return std::fabs(x);}
template <> static float absHelper(float x) {return std::fabs(x);}
template <> static half absHelper(half x) {return x.abs();}

template <typename T>
static void runTestOnDevice(
  cl_device_id dev_id,
  const char *kernel_file,
  std::string extra_build_opts,
  int64_t option_count,
  int64_t global_size,
  int seed,
  bool check_error)
{
  std::stringstream build_opts;
  if (!extra_build_opts.empty()) {
    build_opts << extra_build_opts << ' ';
  }
  build_opts << "-DTYPE=" << typeNameToString<T>();
  build_opts << " -DSIZEOF_TYPE=" << sizeof(T);

  config cfg(dev_id, kernel_file, build_opts.str().c_str());
  std::cout << "  build-opts: " << build_opts.str() << "\n";
#if 0
  std::stringstream ss;
  ss << "BlackScholes-" << typeNameToString<T>();
  auto nm = get_device_info_string(dev_id, CL_DEVICE_NAME);
  if (nm.find("GeForce") != std::string::npos ||
    nm.find("GTX") != std::string::npos ||
    nm.find("RTX") != std::string::npos)
  {
    ss << ".ptx";
  } else {
    ss << ".bin";
  }
  t.save_binary(ss.str());
#endif

  auto k = cfg.get_kernel("BlackScholesBatch");

  // we create these with initial values so we can measure
  // CPU error vs GPU error
  T
    *h_CallCPU = (T *)malloc(option_count*sizeof(T)),
    *h_PutCPU  = (T *)malloc(option_count*sizeof(T)),
    *h_S = (T *)malloc(option_count*sizeof(T)),
    *h_X = (T *)malloc(option_count*sizeof(T)),
    *h_T = (T *)malloc(option_count*sizeof(T));

  srand(seed);
  for(unsigned i = 0; i < option_count; i++) {
    h_CallCPU[i] = (T)INIT_PUT_CALL;
    h_PutCPU[i]  = (T)INIT_PUT_CALL;
    h_S[i] = (T)randFloat(5.0f, 30.0f);
    h_X[i] = (T)randFloat(1.0f, 100.0f);
    h_T[i] = (T)randFloat(0.25f, 10.0f);
  }

  buffer<T>
    b_Call = cfg.buffer_allocate_init_const<T>(
      option_count, INIT_PUT_CALL, CL_MEM_READ_WRITE),
    b_Put = cfg.buffer_allocate_init_const<T>(
      option_count, INIT_PUT_CALL, CL_MEM_READ_WRITE),
    b_S = cfg.buffer_allocate_init_mem<T>(option_count, h_S, CL_MEM_READ_ONLY),
    b_X = cfg.buffer_allocate_init_mem<T>(option_count, h_X, CL_MEM_READ_ONLY),
    b_T = cfg.buffer_allocate_init_mem<T>(option_count, h_T, CL_MEM_READ_ONLY);

  k.set_arg_mem(0, b_Call);
  k.set_arg_mem(1, b_Put);
  k.set_arg_mem(2, b_S);
  k.set_arg_mem(3, b_X);
  k.set_arg_mem(4, b_T);
  k.set_arg_uniform(5, (T)R);
  k.set_arg_uniform(6, (T)V);
  k.set_arg_uniform(7, (cl_uint)option_count);

  /////////////////////////////////////////////////////////////////////////////
  // call GPU
  auto d = k.dispatch(global_size);
  auto elapsed_ns = d.sync();
  double elapsed_s = elapsed_ns/1000.0/1000.0/1000.0;

  /////////////////////////////////////////////////////////////////////////////
  // report perf info
  std::cout << "  elapsed:    " << std::setprecision(4) << std::fixed <<
    elapsed_s << " s\n";
  std::cout << "  throughput: " << std::setprecision(4) << std::fixed <<
    (option_count/1000.0/1000.0/elapsed_s) << " MOpts/S\n";

  if (check_error) {
    ///////////////////////////////////////////////////////////////////////////
    // compute CPU version and compare difference
    BlackScholesBatchCPU<T>(
      h_CallCPU, h_PutCPU, h_S, h_X, h_T, R, V, (unsigned)option_count);
    //
    // measure error
    mincl::read<T,T>(b_Call, b_Put,
      [&] (const T *h_CallGPU, const T *h_PutGPU) {
        double deltaCall = 0, deltaPut = 0, sumCall = 0, sumPut = 0;
        for(unsigned int i = 0; i < option_count; i++) {
          sumCall = (double)absHelper(h_CallCPU[i]);
          sumPut += (double)absHelper(h_PutCPU[i]);
          deltaCall += (double)absHelper(h_CallCPU[i] - h_CallGPU[i]);
          deltaPut += (double)absHelper(h_PutCPU[i] - h_PutGPU[i]);
        }
        double L1call = deltaCall/sumCall;
        double L1put = deltaPut/sumPut;
        std::cout << "  relative error L1 (call, put) = " <<
          std::fixed << std::scientific << std::setprecision(3) <<
          "(" << L1call << ", " << L1put << ")\n";
      });
  }
}



int main(int argc, char **argv)
{
  static const char *DEFAULT_KERNEL_FILE = "black-scholes.cl";
  std::string kernel_file = DEFAULT_KERNEL_FILE;
  std::string extra_build_opts;
  std::vector<cl_device_id> devices;
  std::vector<std::string> args;
  int device_index = 0;
  bool check_error = false;
  int64_t option_count = DEFAULT_OPTION_COUNT;
  int64_t global_size = DEFAULT_GLOBAL_SIZE;
  int seed = 2020;

  for (int ai = 1; ai < argc; ai++) {
    std::string arg = argv[ai];
    std::string arg_val;
    auto off = arg.find('=');
    if (off != std::string::npos) {
      arg_val = arg.substr(off+1);
    }
    auto parseIntArg = [&] () {
      return mincl::parse_int64(arg_val, "malformed integer");
    };
    if (arg == "-h" || arg == "--help") {
      std::cout <<
        "Black-Scholes Tester " << BLACKSCHOLES_VERSION_STRING << "\n" <<
        " " << arg << " OPTIONS\n" <<
        "where OPTIONS are:\n" <<
        "  -b=STRING      adds extra build options\n"
        "                 TYPE is defined as float, double, or half\n"
        "                 SIZEOF_TYPE is 4, 8, or 2 (respectively)\n"
        "  -d=INT         given a device index or name substring\n"
        "  -e             compute error vs CPU\n"
        "  -f=FILE        sets the .cl file"
          " (defaults to " << DEFAULT_KERNEL_FILE << ")\n"
        "  -g=WORKITEMS   sets the global size"
          " (defaults to " << DEFAULT_GLOBAL_SIZE << ")\n"
        "  -h             prints the help banner\n"
        "  -n=OPTCOUNT    option count"
          " (defaults to " << DEFAULT_OPTION_COUNT << ")\n"
        "  -s=SEED        the random number generator seed for initial data\n"
        ;
      exit(EXIT_SUCCESS);
    } else if (arg.substr(0,3) == "-b=") {
      if (!extra_build_opts.empty())
        extra_build_opts += ' ';
      extra_build_opts += arg_val;
    } else if (arg.substr(0,3) == "-d=") {
      devices.push_back(find_device(arg_val));
    } else if (arg == "-e") {
      check_error = true;
    } else if (arg.substr(0,3) == "-g=") {
      global_size = parseIntArg();
    } else if (arg.substr(0,3) == "-f=") {
      kernel_file = arg_val;
    } else if (arg.substr(0,3) == "-n=") {
      option_count = parseIntArg();
    } else if (arg.substr(0,3) == "-s=") {
      seed = (int)parseIntArg();
    } else if (arg == "fp16" || arg == "fp32" || arg == "fp64") {
      args.push_back(arg);
    } else {
      fatal(arg, ": unxpected argument");
    }
  }
  if (devices.empty()) {
    fatal("-d option required");
  }

  if (args.empty()) {
    fatal("one of fp16, fp32, or fp64 argument required");
  }
  std::cout << "each of the " << global_size << " work items will process " <<
    (option_count/global_size) << " options\n";
  for (cl_device_id dev_id : devices) {
    std::string exts = get_device_info_string(dev_id, CL_DEVICE_EXTENSIONS);
    for (auto arg : args) {
      std::cout << "  ==== " << arg << " on " << get_device_name(dev_id) << "\n";
      if (arg == "fp16") {
        if (exts.find("cl_khr_fp16") == std::string::npos) {
          std::cerr << "device does not support fp16\n";
          return EXIT_FAILURE;
        }
        runTestOnDevice<half>(
          dev_id,
          kernel_file.c_str(),
          extra_build_opts,
          option_count,
          global_size,
          seed,
          check_error);
      } else if (arg == "fp32") {
        runTestOnDevice<float>(
          dev_id,
          kernel_file.c_str(),
          extra_build_opts,
          option_count,
          global_size,
          seed,
          check_error);
      } else if (arg == "fp64") {
        if (exts.find("cl_khr_fp64") == std::string::npos) {
          std::cerr << "device does not support fp64\n";
          return EXIT_FAILURE;
        }
        runTestOnDevice<double>(
          dev_id,
          kernel_file.c_str(),
          extra_build_opts,
          option_count,
          global_size,
          seed,
          check_error);
      } else {
        fatal(arg, "unexpected argument (must be fp16, fp32, or fp64)");
      }
    }
  }
  return 0;
}