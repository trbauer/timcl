#include "mincl.hpp"
#include "opts.hpp"

#include <cstdint>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>


// benchmark gops_opts
struct gops_opts : common_opts {
  //
  /////////////////////////////////////////////////////////////////////////////
  // enables ILP
  // (e.g. 4 ==> only every fourth instruction depends on it's last result)
  int                         ilp = 4;
  //
  // each warp/SIMD thread does this much work
  uint64_t                    loop_trips = 16;
  uint64_t                    loop_ops_per_trip = 64; // 1k in I$ given 16B instructions
  uint64_t                    total_work_items = 8ULL*1024*1024;

  bool                        normalize_by_alu = false;

  static gops_opts parseOpts(int argc, const char *argv[]);
};

gops_opts gops_opts::parseOpts(int argc, const char* argv[])
{
  static const gops_opts DEFAULTS;

  gops_opts os;
  for (int ai = 1; ai < argc; ai++) {
    std::string arg = argv[ai];
    std::string key = arg, val = "";
    auto eq_ix = arg.find('=');
    if (eq_ix != std::string::npos) {
      key = arg.substr(0, eq_ix + 1); // include the = so we can distinguish flags
      val = arg.substr(eq_ix + 1);
    }

    if (arg == "-h" || arg == "--help") {
      std::cout <<
        "GigaOp per second\n" <<
        argv[0] << " " << GOPS_VERSION_STRING << "\n" <<
        " " << arg << " OPTIONS\n" <<
        "where COMMON OPTIONS are:\n" <<
        commonOptsHelp() <<
        "\n"
        "and where BENCHMARK OPTIONS are:\n"
        "  -ltc=INT       loop trip count (defaults " <<
          DEFAULTS.loop_trips << ")\n"
        "  -opl=INT       ops per loop (defaults " <<
          DEFAULTS.loop_ops_per_trip << ")\n"
        "  -ilp=INT       enables instruction-level parallelism via concurrent\n"
        "                 parallel data flows"
        "(defaults " << DEFAULTS.ilp << ")\n"
        "                 this must divide: lts*opl\n"
        "  -twi=INT       total work items "
        "(defaults to " << DEFAULTS.total_work_items << ")\n"
        "  --norm=alu     normalize by ALU\n"
        ;
      exit(EXIT_SUCCESS);
    ///////////////////////////////////////////////////////////////////////////
    } else if (key == "-ilp=") {
      os.ilp = (int)mincl::parse_uint64(val);
    } else if (key == "-ltc=") {
      os.loop_trips = mincl::parse_uint64(val);
    } else if (key == "-opl=") {
      os.loop_ops_per_trip = mincl::parse_uint64(val, "integer", true);
    } else if (key == "-twi=") {
      os.total_work_items = mincl::parse_uint64(val, "integer", true);
    } else if (key == "-norm=") {
      if (val == "alu")
        os.normalize_by_alu = true;
      else
        mincl::fatal(arg, ": only -norm=alu");
      ///////////////////////////////////////////////////////////////////////////
    } else {
      os.parseCommonOpt(arg, key, val);
    }
  }

  if (os.devices.empty()) {
    mincl::fatal("at least one -d=.. option required");
  }
  if (os.loop_ops_per_trip % os.ilp != 0) {
    mincl::fatal("-ilp=.. must divide ops per loop (-opl=..)");
  }
  return os;
}



template <typename T>
static double computeGigaOpsPerSecond(
  const gops_opts &os,
  cl_device_id dev_id,
  std::stringstream &verbose_output)
{
  const int NUM_INPUTS = 4;
  std::string type = mincl::typeNameToString<T>();

  std::string kernel_name = "ops_"; kernel_name += type;
  std::stringstream ss;
  if (type.find("half") != std::string::npos) {
    ss << "#pragma OPENCL EXTENSION cl_khr_fp16 : enable\n";
  } else if (type.find("double") != std::string::npos) {
    ss << "#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n";
  }
  ss <<
    "\n"
    "__kernel void " << kernel_name <<
      "(__global " << type << "* dst" << ")\n" <<
    "{\n";
  for (int i = 0; i < NUM_INPUTS; i++) {
    ss <<
      "  " << type << " arg" << i << " = (" << type << ")(\n";
    for (int v = 0; v < mincl::vectorElements<T>(); v++) {
      ss <<
        "    (" << mincl::elementTypeNameToString<T>() <<
        ")(get_local_id(0)+" << (i+1)*(v+1) << ")";
      if (v < mincl::vectorElements<T>() - 1)
        ss << ",\n";
    }
    ss << ");\n";
  }

  const int parallel_sums = os.ilp;
  for (int i = 0; i < parallel_sums; i++) {
    ss <<  "  " << type << " sum" << i << " = arg" << (i % NUM_INPUTS) << ";\n";
  }
  ss << "\n";
  ss << "  for (int l_ix = 0; l_ix < " << os.loop_trips << "; l_ix++) {\n";
  for (uint64_t i = 0; i < os.loop_ops_per_trip; i++) {
    ss << "    sum" << (i % parallel_sums) <<
      " += arg" << (i % NUM_INPUTS) << "*sum" << (i % parallel_sums) << ";\n";
  }
  ss << "  }\n";
  ss << "\n";
  ss <<
    "  dst[get_global_id(0)] = ";
  for (int i = 0; i < parallel_sums; i++) {
    if (i > 0)
      ss << " + ";
    ss << "sum" << i;
  }
  ss << ";\n";
  ss << "}\n";

  // std::cout << "\n" << ss.str() << "\n";
  std::string build_opts; // = "-cl-fast-relaxed-math";
  if (!os.extra_build_options.empty()) {
    if (!build_opts.empty())
      build_opts += " ";
    build_opts += os.extra_build_options;
  }

  if (os.debug()) {
    std::ofstream ofs(kernel_name + ".cl");
    ofs << "// build options: " << build_opts << "\n";
    ofs << ss.str();
    ofs.flush();
  }

  // dump the source
  // std::cout << ss.str();
  mincl::config cfg(
    dev_id,
    "<builtin>",
    ss.str(),
    build_opts.c_str(),
    CL_QUEUE_PROFILING_ENABLE);
  if (os.debug()) {
    std::string ext = ".bin";
    if (mincl::get_device_name(dev_id).find("GeForce") != std::string::npos)
      ext = ".ptx";
    cfg.save_binary(kernel_name + ext);
  }
  //
  auto k = cfg.get_kernel(kernel_name);
  auto b = cfg.buffer_allocate<T>(os.total_work_items);
  k.set_arg_mem(0, b);

  ///////////////////////
  // one warmup dispatch
  auto d0 = k.dispatch(os.total_work_items);
  d0.sync();

  ///////////////////////
  // timed dispatch
  auto d1 = k.dispatch(os.total_work_items);
  d1.sync();
  double s_elapsed = d1.get_prof_ns()/1000.0/1000.0/1000.0;

  int64_t total_ops_per_dispatch =
    os.total_work_items * os.loop_ops_per_trip * os.loop_trips *
      mincl::vectorElements<T>();
  if (os.verbose()) {
    if (mincl::vectorElements<T>() > 1) {
      verbose_output << mincl::vectorElements<T>() << " (vector) * ";
    }
    verbose_output << total_ops_per_dispatch << " ops in " <<
      std::fixed << std::setprecision(5) << s_elapsed << " s";
  }

  double gops = total_ops_per_dispatch/1000.0/1000.0/1000.0/s_elapsed; // GOPS
  if (os.normalize_by_alu) {
    gops /= mincl::get_device_alu_count(dev_id);
  }

  return gops;
}

///////////////////////////////////////////////////////////////////////////////
const static int DEVICE_NAME_COLUMN_WIDTH = 32;
const static int TYPE_NAME_COLUMN_WIDTH = 16;

template <typename T>
static void testTypeOnDevices(const gops_opts &os)
{
  std::string type_name = mincl::typeNameToString<T>();
  std::cout << std::setw(TYPE_NAME_COLUMN_WIDTH) <<
    mincl::typeNameToString<T>();

  std::stringstream verbose_outputs;

  for (const auto &dev : os.devices) {
    std::stringstream verbose_output;
    //
    std::string exts = mincl::get_device_info_string(dev, CL_DEVICE_EXTENSIONS);
    //
    bool test_is_fp16 = type_name.find("half") != std::string::npos;
    bool dev_supports_fp16 = exts.find("cl_khr_fp16") != std::string::npos;
    bool test_is_fp64 = type_name.find("double") != std::string::npos;
    bool dev_supports_fp64 = exts.find("cl_khr_fp64") != std::string::npos;
    //
    std::cout << "  ";
    double gops;
    if (test_is_fp16 && !dev_supports_fp16) {
      std::cout << std::setw(TYPE_NAME_COLUMN_WIDTH) << "no cl_khr_fp16";
    } else if (test_is_fp64 && !dev_supports_fp64) {
      std::cout << std::setw(TYPE_NAME_COLUMN_WIDTH) << "no cl_khr_fp64";
    } else {
      gops = computeGigaOpsPerSecond<T>(os, dev, verbose_output);
      std::cout << std::setw(TYPE_NAME_COLUMN_WIDTH) <<
        std::fixed << std::setprecision(2) << gops;
    }

    if (verbose_output.tellp() > 0) {
      verbose_outputs <<
        "[" << mincl::get_device_info_string(dev, CL_DEVICE_NAME) << "] ";
      auto str = verbose_output.str();
      verbose_outputs << str;
      if (str[str.size() - 1] != '\n' && str[str.size() - 1] != '\r')
        verbose_outputs << "\n";
    }
  }
  std::cout << "\n"; // "  GOPS\n";
  std::cout << verbose_outputs.str();
}


///////////////////////////////////////////////////////////////////////////////
int main(int argc, const char **argv)
{
  gops_opts os = gops_opts::parseOpts(argc, argv);

  if (os.verbose()) {
    std::cout << "each of the " << os.total_work_items <<
      " work items will perform " << os.loop_ops_per_trip * os.loop_trips <<
      " ops\n";
  }
  //
  std::cout << std::setw(TYPE_NAME_COLUMN_WIDTH) << "TYPE";
  for (const auto &dev : os.devices) {
    std::cout << "  " << std::setw(DEVICE_NAME_COLUMN_WIDTH) <<
      mincl::get_device_info_string(dev, CL_DEVICE_NAME);
  }
  std::cout << "\n";
  //
  ///////////////////////////////////////////
  //        D1        D2
  // T1     ......    .......
  // T2     ......    .......
  // T3     ......    .......
  // T4     ......    .......
  // T5     ......    .......
//   testTypeOnDevices<mincl::half>(os); // cl_half typedefs to ushort
  testTypeOnDevices<cl_float>(os);
  testTypeOnDevices<cl_double>(os);
  //
  std::cout << "\n";
  //
  testTypeOnDevices<cl_uchar>(os);
  testTypeOnDevices<cl_ushort>(os);
  testTypeOnDevices<cl_uint>(os);
  testTypeOnDevices<cl_ulong>(os);
  //
  testTypeOnDevices<cl_float4>(os); // sweet spots
  testTypeOnDevices<cl_uchar4>(os); //
#ifdef cl_half2
  testTypeOnDevices<cl_half2>(os); // does device have 2xF16
#endif
  testTypeOnDevices<cl_short2>(os); // does the device have 2xI16

  return EXIT_SUCCESS;
}
