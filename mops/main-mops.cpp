// This benchmark reduces a buffer of floats to a smaller buffer
// (DFT_PER_WI reduced to a single float for each work item).
#include "mincl.hpp"

#include <cstdlib>
#include <iomanip>

using namespace mincl;



// TODO:
//  * add more subgroups to hammer same memory
//    using same memory (broadcast); initial index is: gid % 32
//    separate memory (partitioned); initial index is: parition * gid
//    PROBLEM: need inline asm to determine clocks
//  * local memory
//  * collapse to single DW in cacheline; all work items use lane 0 addr
//  * scale up to produce curve with excel
//      find locations
//           ns
//       1m - XXX
//       2m - XXX
//       4m - XXX
//       8m -
//      16m - 261
//      64m - 260
//
// global options that plumb down to all tests
struct opts {
  std::string extra_build_opts;                           // -b
  bool broadcast_lane = false;                            // -bcl
  bool check_error = true;                                // --no-errchk
  std::string csv_output_file;                            // -csv
  bool device_cycle_counter = false;                      // -dcc
  unsigned iterations = 1;                                // -i=...
  std::string kernel_file;                                // -ovf=...
  int verbosity = 0;                                      // -q/-v/-v2/-v3
  bool warmup = true;                                     // --no-warmup
  bool save_bin = false;                                  // --save-bin
  size_t walks_per_buffer = 128;                          // -walks=INT
  size_t num_subgroups = 1;                               // -nsg=INT

  bool quiet() const {return verbosity <= -1;}
  bool verbose() const {return verbosity >= 1;}
  bool debug() const {return verbosity >= 2;}
};

static void print_header(const opts &opts, std::ostream &os,
                         std::ofstream &csv) {
  os <<
    coll("Device", 32) << " | " <<
    colr("NumLoads/WrkItm", 16) << " | " <<
    colr("BufferSize(B)", 16) << " | " <<
    colr("Elapsed(s)", 16) << " | " <<
    colr("Latency(ns/walk)", 16) << " | " <<
    colr("Latency(c/walk)", 16) << " |";
  if (opts.device_cycle_counter)
    os << colr("DevLat(c/walk)", 16) << " |";
  os << "\n";
  if (csv.is_open()) {
    csv <<
      "Device,"
      "NumLoads/WrkItm,"
      "BufferSize(B),"
      "Elapsed(s),"
      "Latency(ns/walk),"
      "Latency(c/walk),";
    if (opts.device_cycle_counter)
      csv << "DevLat(c/walk),";
    csv << "\n";
  }
}
static void print_data_row(const opts &opts, std::ostream &os,
                           std::ofstream &csv, std::string dev_name,
                           size_t num_loads, size_t walk_buffer_size,
                           double elapsed_s, double ns_per_walk,
                           double clk_per_walk, double dckl_per_walk) {
  os <<
      coll(dev_name, 32) << " | " <<
      colr(num_loads, 16) << " | " <<
      colr(walk_buffer_size, 16) << " | " <<
      frac(elapsed_s, 6, 16) << " | " <<
      frac(ns_per_walk, 4, 16) << " | " <<
      frac(clk_per_walk, 4, 16) << " | ";
  if (opts.device_cycle_counter)
    os << frac(dckl_per_walk, 4, 16) << " | ";
  os <<"\n";
  if (csv.is_open()) {
    csv <<
      dev_name << "," <<
      num_loads << "," <<
      walk_buffer_size << "," <<
      frac(elapsed_s, 6, 16) << "," <<
      frac(ns_per_walk, 4, 16) << "," <<
      frac(clk_per_walk, 4, 16) << ",";
    if (opts.device_cycle_counter)
      csv << frac(dckl_per_walk, 4, 16) << ",";
    csv << "\n";
  }
}

// GOAL:
// each subgroup loads a single int per item and walks that index
//    128B (int x SIMD32)
//
// int inp[N];
//
// SEQUENTIAL:
// inp[0] -> 32
// inp[1] -> 33
// inp[2] -> 34
// ...
// inp[i] -> (i + 32) % N (sequential walk)
// ...
// inp[N-1] -> (N - 1 + 32) % N  (wraps to beginning)
//
// SEQUENTIAL_STRIDED (256 B)
// === THREAD 0 ===
// inp[0] -> 64
// inp[1] -> 65
// ...
// inp[31] -> 95
// === THREAD 1 ===
// inp[32] -> 96
// ...
//
// if idx >= inp_size
//    idx -= inp_size
//



static std::tuple<double,double> test_lat_glb(
  std::ostream &os,
  std::ofstream &csv,
  opts opts,
  cl_device_id dev_id,
  size_t walk_buffer_size)
{
  std::stringstream vos;

  using T = int32_t;
  using TE = cl_type_info<T>::etype;
  auto type_name = cl_type_info<T>::name();
  if (walk_buffer_size == 0 || walk_buffer_size % (32 * sizeof(T))) {
    fatal("total_bytes not a positive multiple of element 32 x ", type_name,
          "'s");
  }
  const size_t total_elems = walk_buffer_size / sizeof(T);
  const size_t total_chunks = walk_buffer_size / (32 * sizeof(T));

  size_t num_loads = opts.walks_per_buffer * total_chunks;
//  os << "num_loads: " << num_loads << " = (" << opts.walks_per_buffer << ") * ("
//     << total_chunks << ")\n";

  static const unsigned SUBGROUP_SIZE = 32;
  const size_t global_size = SUBGROUP_SIZE * opts.num_subgroups;
  if (opts.num_subgroups > 1 && !opts.device_cycle_counter) {
    fatal("-nsg=... > 1 requires -dcc");
  }

  std::string kernel_name = "mlat_glb";

  std::string kernel_src;
  if (opts.kernel_file.empty()) {
    // clang-format off
    std::stringstream ss;
    if (is_nvidia_gpu(dev_id)) {
        // asm("mov.u32 %0, %%laneid;" : "=r"(laneid));
      ss << "ushort get_lane_id() {\n";
      ss << "  uint dst;\n";
      ss << "  asm(\"mov.u32 %0, %%laneid;\" : \"=r\"(dst));\n";
      ss << "  return (ushort)dst;\n";
      ss << "}\n";
    } else {
      ss << "ushort get_lane_id() {return (ushort)get_sub_group_local_id();}\n";
    }
    if (opts.device_cycle_counter) {
      if (is_nvidia_gpu(dev_id)) {
        ss << "long get_cycle_count() {\n";
        ss << "  long dst;\n";
        ss << "  asm volatile(\"mov.u64 %0, %%clock64;\" : \"=l\"(dst));\n";
        ss << "  return dst;\n";
        ss << "}\n";
      } else if (is_intel_gpu(dev_id)) {
        ss << "uint2 __builtin_IB_read_cycle_counter(void) __attribute__((const));\n";
        // ss << "long __builtin_IB_read_cycle_counter(void) __attribute__((const));\n";
        ss << "long get_cycle_count() {\n";
        ss << "  return as_long(__builtin_IB_read_cycle_counter());\n";
        ss << "}\n";
        //
        /*
        //  mov (M1_NM,1) call_i432:ud(0,1)<1> %tsc(0,1)<0;1,0>
        ss << "  long dst;\n";
        ss <<
          "  __asm volatile(\n"
          "       \"{\\n\"\n"
          "       \"  mov (M1_NM,1) %0(0,0)<1> %%tsc(0,0)<0;1,0>\\n\"\n"
          "       \"}\\n\"\n"
          "     :: \"=rw\"(dst)\n"
          "     );\n";
        ss << "  return dst;\n";
        ss << "}\n";
        */
      } else {
        fatal("-dcc: unsupported device for device cycle counter");
      }
      ss << "\n";
    }
    ss << "kernel void " << kernel_name << "(\n";
    ss << "        global " << type_name << " * restrict oup,\n";
    ss << "        global long * restrict oup_cycs,\n";
    ss << "  const global " << type_name << " * restrict inp)\n";
    ss << "{\n";
    ss << "  const uint gid = get_global_id(0);\n";
    ss << "\n";
    ss << "  " << type_name << " idx = "
        "(" << type_name << ")(gid % " << SUBGROUP_SIZE << ");\n";
    if (opts.device_cycle_counter) {
      ss << "  const long start = get_cycle_count();\n";
    }
    ss << "  for (int ld_idx = 0; ld_idx < " << num_loads << "; ld_idx++) {\n";
    // ss << "    if (gid == 0) printf(\"gid[%d] walks [%d]\\n\", gid, idx);\n";
    ss << "    idx = inp[idx];\n";
    ss << "  }\n";
    //
    // keep things deterministic (only 1 write to each address)
    // ss << "  if (get_lane_id() == 0)  // only first group emits ref values\n";
    ss << "  oup[gid] = idx;\n";
    if (opts.device_cycle_counter) {
      // all subgroups report the cycle count, but only one lane
      ss << "  if (get_lane_id() == 0) // all sgs emit a single cycle count\n";
      ss << "    oup_cycs[gid / " << SUBGROUP_SIZE << "] = "
          "(get_cycle_count() - start);\n";
    }
    ss << "}\n";
    kernel_src = ss.str();
    // clang-format on
  }
  if (opts.verbose()) {
    vos << "/////////////////////////////////////////////////////\n";
    vos << "// build-opts: " << opts.extra_build_opts << "\n";
    vos << "// dispatching <global_size = " << global_size << ">\n";
    if (!kernel_src.empty()) {
      vos << kernel_src;
    } else {
      vos << "[file contents loaded from " << opts.kernel_file << "]\n";
    }
    vos << "/////////////////////////////////////////////////////\n";
  }

  config *cfg = nullptr;
  auto qprops = CL_QUEUE_PROFILING_ENABLE;
  if (!opts.kernel_file.empty()) {
    cfg = new config(dev_id, opts.kernel_file, opts.extra_build_opts, qprops);
  } else {
    cfg = new config(dev_id, opts.kernel_file, kernel_src,
                     opts.extra_build_opts, qprops);
    if (opts.save_bin) {
      cfg->save_binary(kernel_name);
    }
  }

  auto k = cfg->get_kernel(kernel_name);

  const auto get_walk_value = [&](size_t ix) {
    size_t next_ix = (ix + SUBGROUP_SIZE) % (total_chunks * SUBGROUP_SIZE);
    if (opts.broadcast_lane) {
      // align down to the initial lowest lane in the subgroup
      //   65 -> 64
      next_ix -= next_ix % SUBGROUP_SIZE;
    }
    return next_ix;
  };

  // create the initial sequence
  T *h_inp = new (std::nothrow) T[total_elems];
  if (h_inp == nullptr) {
    mincl::fatal("host allocation of staging buffer failed");
  }
  // the identity map element [n] maps to n, keeping workitems in loops
  if (opts.debug())
    vos << "======== inputs\n";
  for (size_t i = 0; i < total_elems; i++) {
    T val = cl_type_info<T>::bcast(TE(get_walk_value(i)));
    h_inp[i] = val;
    if (opts.debug())
      vos << "inp[" << i << "] = " << format_t<T>(val, 4) << "\n";
  }

  // 256B = 32 x int
  //  buf[i] = (i + 32)
  //
  // buf[0] = 32
  // buf[1] = 33
  // ...
  // buf[31] = 63
  // buf[32] = 0
  if (walk_buffer_size % (SUBGROUP_SIZE * sizeof(T)) != 0) {
    fatal(walk_buffer_size, ": walk buffer size must be multiple of ",
          SUBGROUP_SIZE * sizeof(T), " B");
  }

  if (opts.debug()) {
    vos << "======== walk per work item\n";
    for (size_t gid = 0; gid < global_size; gid++) {
      size_t idx = gid;
      vos << "gid(" << gid << "): {";
      std::unordered_map<size_t,int> visited;
      for (int ld_idx = 0; ld_idx < (int)num_loads; ld_idx++) {
        if (ld_idx > 0)
          vos << ", ";
        auto itr = visited.find(idx);
        if (itr != visited.end()) {
          vos << "***CYCLES to " << idx << "***...";
          break;
        } else {
          vos << idx;
          visited.emplace(idx, ld_idx);
        }
      }
      vos << "}\n";
    }
  }

  constexpr T t_zero = cl_type_info<T>::bcast((TE)0);
  size_t num_subgroups = std::max<size_t>(1u, global_size / SUBGROUP_SIZE);
  buffer<T> b_oup = cfg->buffer_allocate_init_const<T>(global_size, t_zero,
                                                       CL_MEM_WRITE_ONLY);
  buffer<cl_long> b_oup_cycs = cfg->buffer_allocate_init_const<cl_long>(
                num_subgroups, t_zero,
                CL_MEM_WRITE_ONLY);
  buffer<T> b_inp = cfg->buffer_allocate_init_mem<T>(total_elems, h_inp);

  k.set_arg_mem(0, b_oup);
  k.set_arg_mem(1, b_oup_cycs);
  k.set_arg_mem(2, b_inp);

  /////////////////////////////////////////////////////////////////////////////
  if (opts.warmup) {
    // call GPU with one warmup run first
    // NOTE: warmup clobbers the destination buffer
    k.dispatch(global_size).sync();
  }
  double min_elapsed_s = DBL_MAX;
  for (unsigned i = 0; i < opts.iterations; i++) {
    auto d1 = k.dispatch(global_size, SUBGROUP_SIZE);
    auto elapsed_ns = d1.sync();
    double elapsed_s = elapsed_ns / 1000.0 / 1000.0 / 1000.0;
    min_elapsed_s = std::min(min_elapsed_s, elapsed_s);
    if (opts.verbosity >= 1) {
      os << "run[" << i << "]: " << frac(elapsed_s, 6) << " s\n";
    }
  }
  const double elapsed_s = min_elapsed_s;

  /////////////////////////////////////////////////////////////////////////////
  // report perf info
  const double ns_per_walk = elapsed_s * 1e9 / num_loads;
  const double freq_hz = (double)get_device_frequency_mhz(dev_id) * 1e6;
  const double total_c = freq_hz * elapsed_s;
  const double clk_per_walk = total_c / num_loads;
  double total_dc = 0.0;
  if (opts.device_cycle_counter) {
    cl_long min_cyc = 0;
    mincl::read<cl_long>(b_oup_cycs,
      [&] (const cl_long *h_oup) {
        cl_long min_dc = 0x7FFFFFFFFFFFFFFFi64;
        for (size_t i = 0; i < b_oup_cycs.size(); i++) {
          if (h_oup[i] > 0) { // WA for Intel BS
            min_dc = std::min<cl_long>(min_dc, h_oup[i]);
          }
        }
        if (min_dc > 0) {
          total_dc = (double)min_dc;
        }
      });
  }
  const double dclk_per_walk = total_dc / num_loads;
  print_data_row(opts, os, csv, get_device_name(dev_id),
    num_loads,
    walk_buffer_size,
    elapsed_s,
    ns_per_walk,
    clk_per_walk,
    dclk_per_walk);

  if (opts.check_error || opts.debug()) {
    ///////////////////////////////////////////////////////////////////////////
    // check for error
    mincl::read<T>(b_oup,
      [&] (const T *h_oup) {
        // resynthesize the values that would be in the input buffer
        for (size_t gid = 0; gid < global_size; gid++) {
          T ix = T(gid % SUBGROUP_SIZE);
          for (unsigned i = 0; i < num_loads; i++) {
            ix = h_inp[ix];
          }
          T expected_oup = ix;

          if (opts.debug() || expected_oup != h_oup[gid])
            vos << "oup[" << gid << "] == " << format_t(h_oup[gid], 4) << "\n";
          if (expected_oup != h_oup[gid]) {
            os << vos.str();
            fatal("gid[", gid, "]: final walk value is incorrect; expected ",
                  expected_oup, " got ", h_oup[gid]);
          }
        }
      });
  }

  os << vos.str();

  delete[] h_inp;
  delete cfg;

  return std::make_tuple(ns_per_walk, clk_per_walk);
}



///////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
  opts opts;
  std::vector<cl_device_id> devices; // -d=..
  std::vector<size_t> tests;

  for (int ai = 1; ai < argc; ai++) {
    std::string arg = argv[ai];
    std::string arg_key, arg_val;
    auto off = arg.find('=');
    if (off != std::string::npos) {
      arg_val = arg.substr(off + 1);
      arg_key = arg.substr(0, off + 1);
    }

    auto bad_opts = [&](std::string msg) {
      fatal(arg, ": ", msg);
    };
    auto parse_opt_val_int = [&]() {
      return mincl::parse_int64(arg_val, "malformed integer");
    };
    auto parse_opt_val_uint = [&]() {
      return mincl::parse_uint64(arg_val, "malformed integer");
    };
    auto parse_opt_val_uint_positive = [&]() {
      auto i = parse_opt_val_uint();
      if (i == 0)
        bad_opts("value must be positive");
      return i;
    };
    if (arg == "-h" || arg == "--help") {
      const ::opts DFT;
      // clang-format off
      std::cout <<
        "Memory Operations " << MOPS_VERSION_STRING << "\n" <<
        " " << argv[0] << " [OPTIONS] SIZE+\n" <<
        "where OPTIONS are:\n" <<
        "  -b=STRING         adds extra build options\n"
        "  -bcl              broadcast lane; loads load single DW per lane\n"
        "  -csv=FILE         emit data also in CSV\n"
        "  -d=DEV            specifies device index or device name substring\n"
        "  -dcc              use device cycle counter\n"
        "  -i=ITRS           iterations per test (takes min value)\n"
        "  -nsg=SIZE         num subgroups to run in parallel\n"
        "  --no-errchk       disables error checking\n"
        "  --no-warmup       disables the warmup dispatch\n"
        "  -ovf=FILE         overrides the input kernel file\n"
        "                      (e.g. will search for kernel bw_f32x4 for float4)\n"
        "                      (defaults to internally generated source)\n"
        "                      (use -v2 to see example source)\n"
        "  -w/--walks=SIZE   the number of times each work item walks the full buffer\n"
        "                    (higher values will reduce overhead interference)\n"
        "                      (defaults to " << DFT.walks_per_buffer << ")\n"
        "  -q/-v/-v2/...     quiet/verbose/debug/...\n"
        "  --save-bin        saves the kernel binary (ptx or binary)\n"
        "TEST inputs a list of sizes keyworkds which expand to auto-generates sizes:\n"
        "    \"all\" is \"sall\" + \"mall\" + \"lall\"\n"
        "    \"sall\" is a list of small sizes\n"
        "    \"mall\" ... medium sizes\n"
        "    \"lall\" ... large sizes\n"
        "  The buffers are walked (-w=SIZE) times.  Hence larger buffers take more time\n"
        "EXAMPLES:\n"
        "  % " << argv[0] << " 128  384  4k\n"
        "    walks 128, 384, and 4096 bytes\n"
        ;
      // clang-format on
      exit(EXIT_SUCCESS);
    } else if (arg == "-bcl") {
      opts.broadcast_lane = true;
    } else if (arg_key == "-b=") {
      if (!opts.extra_build_opts.empty())
        opts.extra_build_opts += ' ';
      opts.extra_build_opts += arg_val;
    } else if (arg_key == "-csv=" || arg_key == "--csv-output=") {
      opts.csv_output_file = arg_val;
    } else if (arg == "-dcc") {
      opts.device_cycle_counter = true;
    } else if (arg_key == "-d=") {
      devices.push_back(find_device(arg_val));
    } else if (arg_key == "-i=") {
      opts.iterations = (unsigned)parse_uint64(arg_val, "itrs", true);
      if (opts.iterations == 0) {
        bad_opts("iterations must be positive");
      }
    } else if (arg_key == "-nsg=") {
      opts.num_subgroups =
          (unsigned)parse_uint64(arg_val, "num_subgroups", true);
      if (opts.num_subgroups == 0) {
        bad_opts("num_subgroups must be positive");
      }
    } else if (arg == "--no-errchk") {
      opts.check_error = false;
    } else if (arg == "--no-warmup") {
      opts.warmup = false;
    } else if (arg_key == "-ovf=") {
      opts.kernel_file = arg_val;
    } else if (arg == "-q") {
      opts.verbosity = -1;
    } else if (arg == "--save-bin") {
      opts.save_bin = true;
    } else if (arg == "-v") {
      opts.verbosity = 1;
    } else if (arg == "-v2") {
      opts.verbosity = 2;
    } else if (arg == "-v3") {
      opts.verbosity = 3;
    } else if (arg_key == "-w=" || arg_key == "--walks=") {
      opts.walks_per_buffer = parse_uint64(arg_val, "walks", true);
      if (opts.walks_per_buffer == 0) {
        bad_opts("walks_per_buffer must be positive");
      }
    } else if (arg.substr(0, 1) == "-") {
      bad_opts("invalid option");
    } else {
      auto add_sall = [&]() {
          for (size_t i = 128; i <= 2024; i += 128) { // 8 itrs
            tests.push_back(i);
          }
        };
      auto add_mall = [&]() {
          for (size_t i = 2 * 1024; i < 1024 * 1024; i += 8 * 1024) { // 32 itrs
            tests.push_back(i - 256);
            tests.push_back(i - 128);
            tests.push_back(i);
            tests.push_back(i + 128);
            tests.push_back(i + 256);
          }
        };

      auto add_lall = [&]() {
          for (size_t i = 2 * 1024 * 1024; i < 32 * 1024 * 1024; i += 1024 * 1024) { // 32 itrs
            tests.push_back(i - 2048);
            tests.push_back(i - 1024);
            tests.push_back(i);
            tests.push_back(i + 1024);
            tests.push_back(i + 2048);
          }
        };

      // parsing test size
      if (arg == "sall") {
        add_sall();
      } else if (arg == "mall") {
        add_mall();
      } else if (arg == "lall") {
        add_lall();
      } else if (arg == "all") {
        add_sall();
        add_mall();
        add_lall();
      } else {
        auto size = parse_uint64(arg, "size", true);
        tests.push_back(size);
      }
    }
  }
  if (devices.empty()) {
    devices.push_back(find_device("0"));
  }

  if (tests.empty()) {
    fatal("no tests given (try -h)");
  }

  std::ofstream csv;
  if (!opts.csv_output_file.empty()) {
    csv.open(opts.csv_output_file);
    if (!csv.is_open())
      fatal(opts.csv_output_file, ": failed to open file for write");

  }
  print_header(opts, std::cout, csv);

  for (cl_device_id dev_id : devices) {
    for (auto walk_size : tests) {
      // auto [ns_p_w,c_p_w] = test_lat_glb(std::cout, opts, dev_id, walk_size);
      (void)test_lat_glb(std::cout, csv, opts, dev_id, walk_size);
    }
  }

  return EXIT_SUCCESS;
}