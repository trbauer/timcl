// This benchmark reduces a buffer of floats to a smaller buffer
// (DFT_PER_WI reduced to a single float for each work item).
#include "mincl.hpp"

#include <cstdlib>
#include <iomanip>

using namespace mincl;

/// The default workgroup size.
constexpr int DFT_WG_SIZE = 32;

// The number of loads per work item
// Each work item stores the sum of the loaded elements.
constexpr int DFT_PER_WI = 64;

// each work item will access
enum class mem_access_order {
  // Workitems advance the size of the workgroup keeping each warp
  // accessing full cachelines.

  // Each workgroup starts accessing at:
  //  num_walks * local_size + group_id
  // They access a block of:
  //   local_size * [0..num_walks-1] + [0..local_size-1]
  //
  // E.g. num_walks == 32 and get_local_size() == 32
  // WORKITEM 0          WORKITEM 1       ...
  // 0,32,64...          1,33,65,..
  coalesced,
  //
  // Workitems each access a packed block of elements ([0..num_walks-1]).
  // This makes each vector load access many cachelines in a single load.
  //
  // E.g. num_walks == 32
  // WORKITEM 0          WORKITEM 1       ...
  // 0,1,2,3,4...31      32,33...63
  scattered,
};

// global options that plumb down to all tests
struct opts {
  std::string extra_build_opts;                           // -b
  bool check_access_map = false;                          // --check-bounds
  std::string csv_output_file;                            // -csv=...
  bool check_error = true;                                // --no-errchk
  unsigned iterations = 1;                                // -i=...
  size_t local_size = DFT_WG_SIZE;                        // -wgs=SIZE
  mem_access_order acc_ord = mem_access_order::coalesced; // -mao=...
  std::string kernel_file;                                // -ovf=...
  int verbosity = 0;                                      // -q/-v/-v2/-v3
  size_t num_walks = DFT_PER_WI;                          // -pwi=INT
  bool save_bin = false;                                  // --save-bin
  bool warmup = true;                                     // --no-warmup
};


static size_t get_idx(const opts &opts, size_t gid, unsigned i) {
  size_t idx = 0;
  if (opts.acc_ord == mem_access_order::scattered) {
    idx = opts.num_walks * gid + i;
  } else if (opts.acc_ord == mem_access_order::coalesced) {
    size_t grid = gid / opts.local_size, lid = gid % opts.local_size;
    // idx = num_walks * local_size * grid + local_size * i + lid;
    idx = opts.local_size * (opts.num_walks * grid + i) + lid;
  } else {
    fatal_internal("unsupported memory access pattern");
  }
  return idx;
}
static const char *mem_access_order_str(mem_access_order acc_ord) {
  switch (acc_ord) {
  case mem_access_order::scattered:
    return "scattered";
  case mem_access_order::coalesced:
    return "coalesced";
  default:
    fatal_internal("unsupported memory access pattern");
    return "?";
  }
}

static std::string get_kernel_name(
  std::string type_id, mem_access_order acc_ord)
{
  std::stringstream ss;
  ss << "mtpt_" << type_id << "_" << mem_access_order_str(acc_ord);
  return ss.str();
}

static void check_access_map(
  std::ostream &os,
  const opts &opts,
  size_t total_elems)
{
  const size_t global_size = total_elems / opts.num_walks;

  if (opts.verbosity >= 2)
    os << "########### ACCESS MAP for [" << total_elems << "] ###########\n";
  // compute and show the access map; * means OOB; ! means reaccess of same val
  // output example:
  //  <1>  [1]  [33]  !65!  *97*
  // global workitem <1> accesses [1,33,65,97],
  //     but [65] is re-accessed by another and *97* is out of bounds
  // each buffer element should be read by exactly one item ("owned")
  // any buffer element untouched is "orphaned"
  int first_oob = -1;
  // index buffer index, value is first use
  std::vector<size_t> owners;
  bool any_err = false;
  owners.resize(total_elems, (size_t)-1);
  for (size_t gid = 0; gid < global_size; gid++) {
    if (opts.verbosity >= 2)
      os << coll(format("  wi<",gid,">: "), 20);
    std::stringstream ssi;
    for (unsigned i = 0; i < opts.num_walks; i++) {
      size_t idx = get_idx(opts, gid, i);
      if (idx == (size_t)-1)
        fatal_internal("invalid buffer index");
      bool err_oob = idx >= total_elems;
      if (err_oob && first_oob < 0)
        first_oob = (int)gid;
      bool err_repacc = false;
      if (idx < owners.size()) {
        if (owners[idx] == (size_t)-1) {
          owners[idx] = idx;
        } else {
          ssi << " [" << idx << "] owned by <" << owners[idx] << ">\n";
          err_repacc = true;
        }
      }
      any_err |= err_repacc | err_oob;
      char lb = err_repacc ? '!' : err_oob ? '*' : '[';
      char rb = err_oob ? '*' : err_repacc ? '!' : ']';
      if (opts.verbosity >= 2)
        os << colr(format(" ", lb, idx, rb), 8);
    } // num_walks
    if (opts.verbosity >= 2) {
      os << "\n";
    }
    if (!ssi.str().empty()) {
      os << ssi.str();
    }
  } // global_size
  if (first_oob >= 0) {
    if (opts.verbosity >= 0)
      os << "test has OOB elements (starting " << first_oob << ")!\n";
  }
  for (size_t idx = 0; idx < owners.size(); idx++) {
    if (owners[idx] == (size_t)-1) {
      if (opts.verbosity >= 0)
        os << "[" << idx << "] orphaned\n";
      any_err = true;
    }
  }
  if (any_err) {
    fatal_internal("access map found errors");
  }
}

static void print_header(std::ostream &os) {
  os <<
    coll("Device", 32) << " | " <<
    coll("Kernel", 24) << " | " <<
    colr("BufferSize(MB)", 16) << " | " <<
    colr("EstTpt(GB/s)", 16) << "  | " <<
    colr("EstTpt(B/c)", 16) << "\n";
}

static void print_header_csv(std::ofstream &os) {
  os <<
    "Device,"
    "Kernel,"
    "BufferSize(MB),"
    "EstTpt(GB/s),"
    "EstTpt(B/c)\n";
}
static void print_data_row(
  std::ostream &os,
  std::ofstream &csv,
  const std::string &dev_name,
  const std::string &kernel_name,
  double total_mb,
  double total_gbs,
  double bytes_per_c)
{
  os <<
      coll(dev_name, 32) << " | " <<
      coll(kernel_name, 24) << " | " <<
      frac(total_mb, 4, 16) << " | " <<
      frac(total_gbs, 4, 16) << " | " <<
      frac(bytes_per_c, 1, 16) << "\n";
  if (csv.is_open()) {
    csv <<
        dev_name << "," <<
        kernel_name << "," <<
        frac(total_mb, 4, 16) << "," <<
        frac(total_gbs, 4, 16) << "," <<
        frac(bytes_per_c, 1, 16) << "\n";
  }
}


template <typename T>
static void run_test(
  std::ostream &os,
  std::ofstream &csv,
  opts opts,
  cl_device_id dev_id,
  std::string kernel_name,
  size_t total_bytes)
{
  using TE = cl_type_info<T>::etype;

  const char *type_name = typeNameToString<T>();
  const size_t total_elems = total_bytes / sizeof(T);
  if (opts.verbosity >= 1) {
    os << "buffer size " << frac(total_bytes / 1024.0 / 1024.0, 4) << " MB is "
       << type_name << "[" << total_elems << "];\n";
  }
  if (opts.local_size == 0)
    fatal("local_size must be > 0");
  if (opts.num_walks == 0)
    fatal("num_walks (-pwi) must be > 0");
  if (total_bytes % sizeof(T))
    fatal("total_bytes not multiple of element size");
  if (total_elems % opts.num_walks)
    fatal("total elements (", total_elems,
          ") must be a multiple of num_walks (",
          opts.num_walks, ")");

  const size_t global_size = total_elems / opts.num_walks;
  if (global_size == 0)
    fatal("global_size must be > 0");

  if (opts.local_size > global_size)
    opts.local_size = global_size; // small single workgroup

  if (opts.verbosity >= 1) {
    os << "  global_size = " << global_size
       << ", local_size = " << opts.local_size << ", num_walks = " << opts.num_walks
       << "\n";
  }

  if (global_size % opts.local_size)
    fatal("global_size (", global_size,
          ") must be a multiple greater of workgroup size (", opts.local_size,
          ")");
  if (total_elems % opts.local_size)
    fatal("total elements (", total_elems,
          ") must be a multiple of workgroup size (", opts.local_size, ")");

  if (opts.check_access_map)
    check_access_map(os, opts, total_elems);

  // kind of a failsafe check
  if (get_idx(opts, global_size - 1, (unsigned)opts.num_walks - 1) > total_elems) {
    fatal_internal("last workitem is OOB (try --show-amap)");
  }

  std::string kernel_src;
  if (opts.kernel_file.empty()) {
    // clang-format off
    std::stringstream ss;
    ss << "#define PER_WI " << opts.num_walks << "\n"
       << "#define LOCAL_SIZE (" << opts.local_size
       << ") // constexpr(get_local_size(0))\n"
       << "\n";
    ss << "__attribute__((reqd_work_group_size(LOCAL_SIZE, 1, 1)))\n";
    ss << "kernel void " << kernel_name << "(\n";
    ss << "        global " << type_name << " * restrict oup,\n";
    ss << "  const global " << type_name << " * restrict inp)\n";
    ss << "{\n";
    ss << "  const uint gid = get_global_id(0);\n";
    if (opts.acc_ord == mem_access_order::coalesced) {
      ss << "  const uint grid = get_group_id(0);\n";
      ss << "  const uint lid = get_local_id(0);\n";
    }
    ss << "\n";

    ss << "  " << type_name << " sum = (" << type_name << ")(0);\n";
    ss << "  for (int i = 0; i < PER_WI; i++) {\n";
    if (opts.acc_ord == mem_access_order::scattered) {
      ss << "    uint idx = PER_WI * gid + i;\n";
    } else if (opts.acc_ord == mem_access_order::coalesced) {
      ss << "    uint idx = LOCAL_SIZE * PER_WI * grid + LOCAL_SIZE * i + lid;\n";
    } else {
      fatal_internal("unsupported memory access pattern");
    }
    ss << "    " << type_name << " val = inp[idx];\n";
    ss << "    sum += val;\n";
    ss << "  }\n";
    ss << "  oup[gid] = sum;\n";

    ss << "}\n";
    kernel_src = ss.str();
    // clang-format on
  }
  if (opts.verbosity >= 2) {
    os << "/////////////////////////////////////////////////////\n";
    os << "// build-opts: " << opts.extra_build_opts << "\n";
    os << "// dispatching <global_size = " << global_size
              << ", local_size = " << opts.local_size << ">\n";
    if (!kernel_src.empty()) {
      os << kernel_src;
    } else {
      os << "[file contents loaded from " << opts.kernel_file << "]\n";
    }
    os << "/////////////////////////////////////////////////////\n";
  }

  config *cfg = nullptr;
  auto qprops = 0;
//  auto qprops = CL_QUEUE_PROFILING_ENABLE;
  if (!opts.kernel_file.empty()) {
    cfg = new config(dev_id, opts.kernel_file, opts.extra_build_opts, qprops);
  } else {
    cfg = new config(dev_id, opts.kernel_file, kernel_src,
                     opts.extra_build_opts, qprops);
    if (opts.save_bin) {
      cfg->save_binary(kernel_name);
    }
  }

  auto k = cfg->get_kernel(kernel_name);

  // create the initial sequence
  T *h_inp = (T *)malloc(total_elems * sizeof(T));
  if (h_inp == nullptr) {
    mincl::fatal("host allocation of staging buffer failed");
  }
  for(size_t i = 0; i < total_elems; i++) {
    T val = cl_type_info<T>::bcast(TE(i));
    h_inp[i] = val;
    if (opts.verbosity >= 3)
      os << "inp[" << i << "] " << format_t<T>(val, 4) << "\n";
  }

  const T t_zero = cl_type_info<T>::bcast((TE)0);
  buffer<T> b_oup = cfg->buffer_allocate_init_const<T>(global_size, t_zero,
                                                    CL_MEM_WRITE_ONLY),
            b_inp = cfg->buffer_allocate_init_mem<T>(total_elems, h_inp,
                                                  CL_MEM_READ_ONLY);
  free(h_inp);

  k.set_arg_mem(0, b_oup);
  k.set_arg_mem(1, b_inp);

  /////////////////////////////////////////////////////////////////////////////
  if (opts.warmup) {
    // call GPU with one warmup run first
    // NOTE: warmup clobbers the destination buffer
    k.dispatch(global_size, opts.local_size).sync();
  }
  double min_elapsed_s = DBL_MAX;
  for (unsigned i = 0; i < opts.iterations; i++) {
    auto d1 = k.dispatch(global_size, opts.local_size);
    auto elapsed_ns = d1.sync();
    double elapsed_s = elapsed_ns / 1000.0 / 1000.0 / 1000.0;
    min_elapsed_s = std::min(min_elapsed_s, elapsed_s);
    if (opts.verbosity >= 1) {
      os << "run[" << i << "]: " << frac(elapsed_s, 6) << " s\n";
    }
  }
  const double elapsed_s = min_elapsed_s;

  /////////////////////////////////////////////////////////////////////////////
  // report perf info
  const auto dev_name = get_device_name(dev_id);
  double total_mb = total_bytes / 1024.0 / 1024.0;
  double total_gb = total_bytes / 1024.0 / 1024.0 / 1024.0;
  const double freq_hz = (double)get_device_frequency_mhz(dev_id) * 1e6;
  const auto total_gbs = total_gb / elapsed_s;
  const auto bytes_per_c = total_bytes / freq_hz / min_elapsed_s;
  print_data_row(os, csv, dev_name, kernel_name, total_mb, total_gbs, bytes_per_c);

  if (opts.verbosity >= 2) {
    os << "  elapsed:    " << frac(elapsed_s, 6) << " s\n";
    os << "  read:       " << frac(total_mb) << " MB\n";
  }

  if (opts.check_error) {
    ///////////////////////////////////////////////////////////////////////////
    // check for error
    T expected_sum = t_zero;
    // In case the reduction function isn't commutative (e.g. float add)
    // and we are doing error checking, iterate the sequence in the
    // same order to get the same error as will be computed on the device.
    for (size_t gid = 0; gid < global_size; gid++) {
      T acc = t_zero;
      for (unsigned i = 0; i < opts.num_walks; i++) {
        // resynthesize the values that would be in the input buffer
        acc += cl_type_info<T>::bcast(TE(get_idx(opts, gid, i)));
      }
      expected_sum += acc;
    }

    mincl::read<T>(b_oup,
      [&] (const T *h_oup) {
        T actual_sum = t_zero;
        for (size_t i = 0; i < global_size; i++) {
          actual_sum += h_oup[i];
          if (opts.verbosity >= 3)
            os << "oup[" << i << "] " << format_t(h_oup[i], 4) << "\n";
        }
        if (opts.verbosity >= 3 || actual_sum != expected_sum) {
          os << "actual   sum: " << format_t(actual_sum, 4) << "\n";
          os << "expected sum: " << format_t(expected_sum, 4) << "\n";
        }
        if (actual_sum != expected_sum) {
          fatal("kernel sum mismatch expected");
        }
      });
  }

  delete cfg;
}

///////////////////////////////////////////////////////////////////////////////
static std::pair<const char *,size_t> type_ids[] {
  {"f32", 4},
  {"f32x4", 4 * 4},
  {"i32", 4},
  {"i32x4", 4 * 4},
//  {"u8", 1},
//  {"u8x4", 4},
};
static size_t type_id_elem_size(std::string s) {
  for (auto [ty,sz] : type_ids) {
    if (ty == s) return sz;
  }
  return 0;
}
static bool is_type_id(std::string s) {
  return type_id_elem_size(s) != 0;
}
static std::string type_id_list(char sep) {
  std::stringstream ss;
  bool first = true;
  for (auto [ty,_] : type_ids) {
    if (first)
      first = false;
    else ss << sep;
      ss << ty;
  }
  return ss.str();
}

struct test {
  const std::string type;
  const size_t elem_size;
  std::vector<size_t> sizes;
  test(std::string ty, size_t esz) : type(ty), elem_size(esz) { }

  void populate_default_sizes(const opts &opts) {
    auto small = opts.local_size * opts.num_walks * elem_size; // 1 workgroup
    sizes.emplace_back(small);
    sizes.emplace_back(small * 512);
    sizes.emplace_back(small * 1024);
    sizes.emplace_back(small * 4 * 1024);
  }
};

///////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
  opts opts;
  std::vector<cl_device_id> devices; // -d=..
  std::vector<test> tests;

  for (int ai = 1; ai < argc; ai++) {
    std::string arg = argv[ai];
    std::string arg_key, arg_val;
    auto off = arg.find('=');
    if (off != std::string::npos) {
      arg_val = arg.substr(off + 1);
      arg_key = arg.substr(0, off + 1);
    }

    auto badOpt = [&](std::string msg) {
      fatal(arg, ": ", msg);
    };
    auto parseIntArg = [&]() {
      return mincl::parse_int64(arg_val, "malformed integer");
    };
    auto parseUIntArg = [&]() {
      return mincl::parse_uint64(arg_val, "malformed integer");
    };
    auto parseUIntArgNotZero = [&]() {
      auto i = parseUIntArg();
      if (i == 0)
        badOpt("value must be positive");
      return i;
    };
    if (arg == "-h" || arg == "--help") {
      const ::opts DFT;
      // clang-format off
      std::cout <<
        "Memory Throughput Tester " << MTPT_VERSION_STRING << "\n" <<
        " " << argv[0] << " [OPTIONS] TEST+\n" <<
        "where OPTIONS are:\n" <<
        "  -b=STRING        adds extra build options\n"
        "  --check-bounds   computes the access map and checks for indexing errors\n"
        "                   (this is not done at runtime: use -v2 to show it)"
        "  -csv=FILE        emit data also in CSV\n"
        "  -d=DEV           specifies device index or name substring\n"
        "  -lsz=SIZE        workgroup size (local size)\n"
        "                     (defaults to " << DFT.local_size << ")\n"
        "  -mao=ORD         sets memory access order (c or s)\n"
        "                   c = coalesced subgroup loads\n"
        "                     (each workitem strides down by workgroup size so that\n"
        "                     the subgroup's load is coalesced)\n"
        "                   s = scattered subgroup loads\n"
        "                     (each workitem loads a packed block over multiple loads)\n"
        "  --no-errchk      disables error checking\n"
        "  --no-warmup      disables the warmup dispatch\n"
        "  -ovf=FILE        overrides the input kernel file\n"
        "                     (e.g. will search for kernel bw_f32x4 for float4)\n"
        "                     (defaults to internally generated source)\n"
        "                     (use -v2 to see example source)\n"
        "  -pwi=SIZE        num to reduce per work item\n"
        "                   *** this is the read/write compression factor ***\n"
        "                   higher values will reduce interference from writes\n"
        "                     (defaults to " << DFT.num_walks << ")\n"
        "  -q/-v/-v2/...    quiet/verbose/debug/...\n"
        "  --save-bin       saves the kernel binary (ptx or binary)\n"
        "                     (defaults to " << DFT.local_size << ")\n"
        "TEST inputs are a type id and a list of sizes:\n"
        "  TYPE(:SIZE(,SIZE)*)?"
        "    where TYPE=" << type_id_list('|') << "\n"
        "          SIZE=INT(:[kmg])?\n"
        "  There are various constraints on buffer size and type combination\n"
        "  (mustn't be ragged).\n"
        "  If the size list is absent we pick a default set.\n"
        "EXAMPLES:\n"
        "  % " << argv[0] << " f32  f32x4:1k,2m\n"
        "    float on default sizes and f32x4 *(float4) on 1 KB and 2 MB bufs\n"
        "  % " << argv[0] << " f32x4:64  -d=1   -pwi=2\n"
        "    float4 on device #1 with 2 buffer elements per workitem\n"
        ;
      // clang-format on
      exit(EXIT_SUCCESS);
    } else if (arg_key == "-b=") {
      if (!opts.extra_build_opts.empty())
        opts.extra_build_opts += ' ';
      opts.extra_build_opts += arg_val;
    } else if (arg == "--check-bounds") {
      opts.check_access_map = true;
    } else if (arg_key == "-csv=" || arg_key == "--csv-output=") {
      opts.csv_output_file = arg_val;
    } else if (arg_key == "-d=") {
      devices.push_back(find_device(arg_val));
    } else if (arg_key == "-i=") {
      opts.iterations = (unsigned)parseUIntArgNotZero();
    } else if (arg == "--no-errchk") {
      opts.check_error = false;
    } else if (arg == "--no-warmup") {
      opts.warmup = false;
    } else if (arg_key == "-ovf=") {
      opts.kernel_file = arg_val;
    } else if (arg_key == " -lsz=") {
      opts.local_size = (unsigned)parseUIntArgNotZero();
    } else if (arg_key == "-mao=") {
      if (arg_val == "s")
        opts.acc_ord = mem_access_order::scattered;
      else if (arg_val == "c")
        opts.acc_ord = mem_access_order::coalesced;
      else
        badOpt("invalid access order");
    } else if (arg_key == "-pwi=") {
      opts.num_walks = (unsigned)parseUIntArgNotZero();
    } else if (arg == "-q") {
      opts.verbosity = -1;
    } else if (arg == "--save-bin") {
      opts.save_bin = true;
    } else if (arg == "-v") {
      opts.verbosity = 1;
    } else if (arg == "-v2") {
      opts.verbosity = 2;
    } else if (arg == "-v3") {
      opts.verbosity = 3;
    } else if (arg.substr(0,1) == "-") {
      badOpt("invalid option");
    } else {
      // parsing TYPE:SIZE(,SIZE)*
      std::string type;
      auto colon = arg.find(':');
      std::string type_id = arg;
      if (colon != std::string::npos) {
        type_id = arg.substr(0, colon);
      }
      tests.push_back(test(type_id, type_id_elem_size(type_id)));
      test &t = tests.back();
      if (colon != std::string::npos) {
        size_t next_size = colon + 1;
        if (next_size >= arg.size())
          badOpt("malformed SIZE");
        while (true) {
          size_t next_comma = arg.find(',', next_size + 1);
          auto size_tkn = arg.substr(next_size, next_comma - next_size);
          auto size_val = parse_uint64(size_tkn, "SIZE", true);
          t.sizes.emplace_back(size_val);
          if (next_comma == std::string::npos) {
            break;
          }
          next_size = next_comma + 1;
        }
      }
      if (!is_type_id(t.type)) {
        badOpt(format(t.type, ": invalid type"));
      }
    }
  }
  if (devices.empty()) {
    devices.push_back(find_device("0"));
  }

  if (tests.empty()) {
    fatal("no tests given (try -h)");
  }

  // populate some default sizes
  for (auto &t : tests) {
    if (t.sizes.empty()) {
      t.populate_default_sizes(opts);
    }
  }

  std::ofstream ofs;
  if (!opts.csv_output_file.empty()) {
    ofs.open(opts.csv_output_file);
    if (!ofs.is_open())
      fatal(opts.csv_output_file, ": failed to open file");
    print_header_csv(ofs);
  }

  print_header(std::cout);

  for (cl_device_id dev_id : devices) {
    auto runSeq = [&]<typename T>(const test &t){
      for (auto total_size_bytes : t.sizes) {
        std::string kernel_name = get_kernel_name(t.type, opts.acc_ord);
        run_test<T>(std::cout, ofs, opts, dev_id, kernel_name,
                    total_size_bytes);
      }
    };

    std::string exts = get_device_info_string(dev_id, CL_DEVICE_EXTENSIONS);
    for (const auto &t : tests) {
      // std::cout << "========================================================\n";
      if (opts.verbosity >= 1) {
        std::cout << "=> " << t.type << " on " << get_device_name(dev_id) << "\n";
        auto gsize = get_device_info_ulong(dev_id, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE);
        auto gsize_mb = (double)gsize / 1024.0 / 1024.0;
        std::cout << " global cache size: " << gsize_mb << " MB\n";
      }

      if (t.type == "f32") {
        runSeq.template operator()<cl_float>(t);
      } else if (t.type == "f32x4") {
        runSeq.template operator()<cl_float4>(t);
      } else if (t.type == "i32") {
        runSeq.template operator()<cl_int>(t);
      } else if (t.type == "i32x4") {
        runSeq.template operator()<cl_int4>(t);
      } else {
        fatal_internal(t.type, "unexpected argument");
      }
    }
  }
  return 0;
}