#include "algorithm.hpp"

// REFERENCE:
//   M. Matsumoto and T. Nishimura,
//   "Mersenne Twister: A 623-Dimensionally Equidistributed Uniform
//   Pseudo-Random Number Generator",
//   ACM Transactions on Modeling and Computer Simulation,
//   Vol. 8, No. 1, January 1998, pp 3--30.

// Period parameters
#define __N__ 624
#define __M__ 397
#define MATRIX_A 0x9908b0df   // constant vector a
#define UPPER_MASK 0x80000000 // most significant w-r bits
#define LOWER_MASK 0x7fffffff // least significant r bits

// Tempering parameters
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

typedef struct mt19937_stateStruct
{
  uint32_t mt[__N__];
  int mti;
} mt19937state;


void initializeMersenneState(mt19937state* m, uint32_t seed);

uint32_t mersenneRandU32(mt19937state* m);


static void initializeMersenneState(mt19937state* m, uint32_t seed)
{
  int i;

  for (i = 0; i < __N__; i++)
  {
    m->mt[i] = seed & 0xffff0000;
    seed = 69069 * seed + 1;
    m->mt[i] |= (seed & 0xffff0000) >> 16;
    seed = 69069 * seed + 1;
  }
  m->mti = __N__;
}

static uint32_t mersenneRandU32(mt19937state* m)
{
  uint32_t y;
  uint32_t mag01[2];
  mag01[0] = 0x0;
  mag01[1] = MATRIX_A;

  // mag01[x] = x * MATRIX_A  for x=0,1

  if (m->mti >= __N__)
  {
    // generate N words at one time
    int kk = 0;
    while (kk < __N__ - __M__)
    {
      y = (m->mt[kk] & UPPER_MASK) | (m->mt[kk + 1] & LOWER_MASK);
      m->mt[kk] = m->mt[kk + __M__] ^ (y >> 1) ^ mag01[y & 0x1];
      ++kk;
    }
    while (kk < __N__ - 1)
    {
      y = (m->mt [kk] & UPPER_MASK) | (m->mt [kk + 1] & LOWER_MASK);
      m->mt[kk] = m->mt[kk + (__M__ - __N__)] ^ (y >> 1) ^ mag01[y & 0x1];
      ++kk;
    }

    y = (m->mt[__N__ - 1] & UPPER_MASK) | (m->mt [0] & LOWER_MASK);
    m->mt[__N__ - 1] = m->mt[__M__ - 1] ^ (y >> 1) ^ mag01[y & 0x1];

    m->mti = 0;
  }

  y = m->mt[m->mti++];
  y ^= TEMPERING_SHIFT_U(y);
  y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
  y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
  y ^= TEMPERING_SHIFT_L(y);
  return y;
}


const std::vector<uint32_t> &algorithm_mt19937::getRefereeValues()
{
  static std::vector<uint32_t> sum_refs;
  if (sum_refs.empty()) {
    sum_refs.reserve(os.total_work_items);

    for (size_t gid = 0; gid < os.total_work_items; gid++) {
      // kernel: initialize
      mt19937state m {};
      initializeMersenneState(&m, (uint32_t)os.seed + (uint32_t)gid);

      // kernel: mersenneRandU32
      uint32_t sum_ref = 0;
      for (uint32_t i = 0; i < os.sequence_lengths; i++) {
        sum_ref += mersenneRandU32(&m) & 0xFFFF;
      }
      sum_refs.push_back(sum_ref);
    }
  }
  return sum_refs;
}


bool algorithm_mt19937::run(
  cl_device_id dev_id,
  std::stringstream &cell_output,
  std::stringstream &verbose_output)
{
  return run_impl<mt19937state>(dev_id, cell_output, verbose_output);
}
