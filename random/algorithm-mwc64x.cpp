#include "algorithm.hpp"

#include <cstdint>


using uint = uint32_t;
using ulong = uint64_t;
struct uint2 {
  uint x, y;
  uint2(uint _x, uint _y) : x(_x), y(_y) { }
};
inline static uint mul_hi(uint a, uint b) {
  ulong ab64 = (ulong)a * b;
  return (uint)(ab64 >> 32);
}
inline static uint mad_hi(uint a, uint b, uint c) {
  return mul_hi(a, b) + c;
}

// Pre: a<M, b<M
// Post: r=(a+b) mod M
ulong MWC_AddMod64(ulong a, ulong b, ulong M)
{
  ulong v=a+b;
  if ((v >= M) || (v < a))
    v = v - M;
  return v;
}

// Pre: a<M,b<M
// Post: r=(a*b) mod M
// This could be done more efficently, but it is portable, and should
// be easy to understand. It can be replaced with any of the better
// modular multiplication algorithms (for example if you know you have
// double precision available or something).
ulong MWC_MulMod64(ulong a, ulong b, ulong M)
{
  ulong r=0;
  while (a != 0) {
    if (a & 1)
      r = MWC_AddMod64(r, b, M);
    b = MWC_AddMod64(b, b, M);
    a = a >> 1;
  }
  return r;
}


// Pre: a<M, e>=0
// Post: r=(a^b) mod M
// This takes at most ~64^2 modular additions, so probably about 2^15 or so instructions on
// most architectures
ulong MWC_PowMod64(ulong a, ulong e, ulong M)
{
  ulong sqr = a, acc = 1;
  // printf(" H: a:0x%016llx\n", a);
  while (e != 0) {
    // printf(" H: e:0x%016llx  acc:0x%016llx  sqr:0x%016llx\n", e, acc, sqr);
    if (e & 1)
      acc = MWC_MulMod64(acc, sqr, M);
    sqr = MWC_MulMod64(sqr, sqr, M);
    e = e >> 1;
  }
  // printf(" H: acc:0x%016llx\n", acc);
  return acc;
}

uint2 MWC_SkipImpl_Mod64(uint2 curr, ulong A, ulong M, ulong distance)
{
  ulong m = MWC_PowMod64(A, distance, M);
  ulong x = curr.x * (ulong)A + curr.y;
  x = MWC_MulMod64(x, m, M);
  return uint2((uint)(x / A), (uint)(x % A));
}

uint2 MWC_SeedImpl_Mod64(
  ulong A, ulong M,
  uint vecSize, uint vecOffset,
  ulong streamBase, ulong streamGap,
  ulong gid)
{
  // This is an arbitrary constant for starting LCG jumping from. I didn't
  // want to start from 1, as then you end up with the two or three first values
  // being a bit poor in ones - once you've decided that, one constant is as
  // good as any another. There is no deep mathematical reason for it, I just
  // generated a random number.
  const ulong MWC_BASEID = 4077358422479273989ULL;

  // ulong dist = streamBase + (get_global_id(0) * vecSize + vecOffset) * streamGap;
  ulong dist = streamBase + (gid * vecSize + vecOffset) * streamGap;
  // printf(" H: A:0x%016llx\n", A);
  ulong m = MWC_PowMod64(A, dist, M);

  ulong x = MWC_MulMod64(MWC_BASEID, m, M);
  return uint2((uint)(x / A), (uint)(x % A));
}


//! Represents the state of a particular generator
typedef struct{uint x; uint c;} mwc64x_state_t;

// enum {MWC64X_A = 4294883355U};
static const uint MWC64X_A = 4294883355UL;
// enum {MWC64X_M = 18446383549859758079UL};
static const ulong MWC64X_M = 18446383549859758079ULL;


void MWC64X_Step(mwc64x_state_t *s)
{
  uint X = s->x, C = s->c;

  uint Xn = MWC64X_A*X + C;
  uint carry = (uint)(Xn < C);        // The (Xn<C) will be zero or one for scalar
  uint Cn = mad_hi(MWC64X_A, X, carry);

  s->x = Xn;
  s->c = Cn;
}

void MWC64X_Skip(mwc64x_state_t *s, ulong distance)
{
  uint2 tmp = MWC_SkipImpl_Mod64(uint2(s->x, s->c), MWC64X_A, MWC64X_M, distance);
  s->x = tmp.x;
  s->c = tmp.y;
}

void MWC64X_SeedStreams(
  mwc64x_state_t *s, ulong baseOffset, ulong perStreamOffset, ulong gid)
{
  uint2 tmp =
    MWC_SeedImpl_Mod64(MWC64X_A, MWC64X_M, 1, 0, baseOffset, perStreamOffset, gid);
  s->x = tmp.x;
  s->c = tmp.y;
}

//! Return a 32-bit integer in the range [0..2^32)
uint MWC64X_NextUint(mwc64x_state_t *s)
{
  uint res = s->x ^ s->c;
  MWC64X_Step(s);
  return res;
}



const std::vector<uint32_t> &algorithm_mwc64x::getRefereeValues()
{
  static std::vector<uint32_t> sum_refs;
  if (sum_refs.empty()) {
    sum_refs.reserve(os.total_work_items);

    for (size_t gid = 0; gid < os.total_work_items; gid++) {
      // kernel: initialize
      mwc64x_state_t m {};
      MWC64X_SeedStreams(&m, 0, 1, (uint32_t)os.seed + (uint32_t)gid);

      // kernel: mersenneRandU32
      uint32_t sum_ref = 0;
      for (uint32_t i = 0; i < os.sequence_lengths; i++) {
        sum_ref += MWC64X_NextUint(&m) & 0xFFFF;
      }
      sum_refs.push_back(sum_ref);
    }
  }
  return sum_refs;
}


bool algorithm_mwc64x::run(
  cl_device_id dev_id,
  std::stringstream &cell_output,
  std::stringstream &verbose_output)
{
  return run_impl<mwc64x_state_t>(dev_id, cell_output, verbose_output);
}