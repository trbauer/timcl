#include "algorithm.hpp"

#include <cstdint>

struct pcg_state_setseq_64 {    // Internals are *Private*.
    uint64_t state;             // RNG state.  All values are possible.
    uint64_t inc;               // Controls which RNG sequence (stream) is
                                // selected. Must *always* be odd.
};
typedef struct pcg_state_setseq_64 pcg32_random_t;

static uint32_t pcg32_random_r(pcg32_random_t* rng);

static void pcg32_srandom_r(
    pcg32_random_t* rng, uint64_t initstate, uint64_t initseq)
{
  rng->state = 0U;
  rng->inc = (initseq << 1u) | 1u;
  pcg32_random_r(rng);
  rng->state += initstate;
  pcg32_random_r(rng);
}



static uint32_t pcg32_random_r(pcg32_random_t* rng)
{
  uint64_t oldstate = rng->state;
  rng->state = oldstate * 6364136223846793005ULL + rng->inc;
  uint32_t xorshifted = (uint32_t)(((oldstate >> 18u) ^ oldstate) >> 27u);
  uint32_t rot = oldstate >> 59u;
  return (xorshifted >> rot) | (xorshifted << ((~rot + 1) & 31));
}


// pcg32_boundedrand(bound):
// pcg32_boundedrand_r(rng, bound):
//     Generate a uniformly distributed number, r, where 0 <= r < bound
/*
static uint32_t pcg32_boundedrand_r(pcg32_random_t* rng, uint32_t bound)
{
    // To avoid bias, we need to make the range of the RNG a multiple of
    // bound, which we do by dropping output less than a threshold.
    // A naive scheme to calculate the threshold would be to do
    //
    //     uint32_t threshold = 0x100000000ull % bound;
    //
    // but 64-bit div/mod is slower than 32-bit div/mod (especially on
    // 32-bit platforms).  In essence, we do
    //
    //     uint32_t threshold = (0x100000000ull-bound) % bound;
    //
    // because this version will calculate the same modulus, but the LHS
    // value is less than 2^32.

    uint32_t threshold = -bound % bound;

    // Uniformity guarantees that this loop will terminate.  In practice, it
    // should usually terminate quickly; on average (assuming all bounds are
    // equally likely), 82.25% of the time, we can expect it to require just
    // one iteration.  In the worst case, someone passes a bound of 2^31 + 1
    // (i.e., 2147483649), which invalidates almost 50% of the range.  In
    // practice, bounds are typically small and only a tiny amount of the range
    // is eliminated.
    for (;;) {
        uint32_t r = pcg32_random_r(rng);
        if (r >= threshold)
            return r % bound;
    }
}
*/

const std::vector<uint32_t> &algorithm_pcg::getRefereeValues()
{
  static std::vector<uint32_t> sum_refs;
  if (sum_refs.empty()) {
    sum_refs.reserve(os.total_work_items);

    for (size_t gid = 0; gid < os.total_work_items; gid++) {
      // kernel: initialize
      pcg32_random_t m {};
      pcg32_srandom_r(&m, (uint32_t)os.seed, gid);

      // kernel: sumStream
      uint32_t sum_ref = 0;
      for (uint32_t i = 0; i < os.sequence_lengths; i++) {
        sum_ref += pcg32_random_r(&m) & 0xFFFF;
      }
      sum_refs.push_back(sum_ref);
    }
  }
  return sum_refs;
}

bool algorithm_pcg::run(
  cl_device_id dev_id,
  std::stringstream &cell_output,
  std::stringstream &verbose_output)
{
  return run_impl<pcg32_random_t>(dev_id, cell_output, verbose_output);
}
