#include "algorithm.hpp"

#include <cstdint>

using uint = uint32_t;

#if 0
struct xorshift32_state {
  uint32_t a;
};

/* The state word must be initialized to non-zero */
uint32_t xorshift32(struct xorshift32_state *state)
{
  /* Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs" */
  uint32_t x = state->a;
  x ^= x << 13;
  x ^= x >> 17;
  x ^= x << 5;
  return state->a = x;
}

struct xorshift64_state {
  uint64_t a;
};

uint64_t xorshift64(struct xorshift64_state *state)
{
  uint64_t x = state->a;
  x ^= x << 13;
  x ^= x >> 7;
  x ^= x << 17;
  return state->a = x;
}

struct xorshift128_state {
  uint32_t a, b, c, d;
};

/* The state array must be initialized to not be all zero */
uint32_t xorshift128(struct xorshift128_state *state)
{
  /* Algorithm "xor128" from p. 5 of Marsaglia, "Xorshift RNGs" */
  uint32_t t = state->d;

  uint32_t const s = state->a;
  state->d = state->c;
  state->c = state->b;
  state->b = s;

  t ^= t << 11;
  t ^= t >> 8;
  return state->a = t ^ s ^ (s >> 19);
}

struct xorwow_state {
  uint a, b, c, d, e;
  uint counter;
};

// the state array must be initialized to not be all zero in the first four words
uint32_t xorwow(struct xorwow_state *state)
{
  // Algorithm "xorwow" from p. 5 of Marsaglia, "Xorshift RNGs"
  uint32_t t = state->e;
  uint32_t s = state->a;
  state->e = state->d;
  state->d = state->c;
  state->c = state->b;
  state->b = s;
  t ^= t >> 2;
  t ^= t << 1;
  t ^= s ^ (s << 4);
  state->a = t;
  state->counter += 362437;
  return t + state->counter;
}

void init(xorwow_state *state, uint seed)
{
  // from Marsaglia's static initialized constants (P.5)
  // Wikipedia shuffles the state order names (mostly reverses)
  // a == v, b = w, c = z, d = y,  e == x, counter = d
  state->a = 5783321;
  state->b = 88675123;
  state->c = 521288629;
  state->d = 362436069;
  state->e = 123456789;
  state->counter = 6615241;
}

#endif



struct xorwow_state {
  uint x, y, z, w, v, d;
};

void xorwowSeed(xorwow_state *state, uint seed)
{
  //   static unsigned long
  //    x=123456789, y=362436069, z=521288629,
  //    w=88675123, v=5783321, d=6615241;
  state->x = 123456789, state->y = 362436069, state->z = 521288629;
  state->w =  88675123, state->v =   5783321, state->d =   6615241;
  //
  // Here's where I don't know the right thing to do: use the seed
  // to influence the sequence.  JSAT uses parent java.util.Random's class
  // to use raw random values.
  //
  // using this: 0 makes it use the reference values
  state->x ^= (seed >>  2) ^ (seed <<  2);
  state->y ^= (seed >>  4) ^ (seed <<  4);
  state->z ^= (seed >>  6) ^ (seed <<  6);
  state->w ^= (seed >>  8) ^ (seed <<  8);
  state->v ^= (seed >> 10) ^ (seed << 10);
  state->d ^= (seed >> 12) ^ (seed << 12);
}


// Algorithm "xorwow" from p. 5 of Marsaglia, "Xorshift RNGs"
// unsigned long xorwow(){
//   static unsigned long
//    x=123456789, y=362436069, z=521288629,
//    w=88675123, v=5783321, d=6615241;
//   unsigned long t;
//   t = (x � (x >> 2));
//   x = y;
//   y = z;
//   z = w;
//   w = v;
//   v = (v � (v << 4)) � (t � (t << 1));
//   return (d += 362437) + v;
// }

uint xorwowGenerate(xorwow_state *state)
{
  uint t = state->x ^ (state->x >> 2);
  state->x = state->y;
  state->y = state->z;
  state->z = state->w;
  state->w = state->v;
  state->v = (state->v ^ (state->v << 4)) ^ (t ^ (t << 1));
  return (state->d += 362437) + state->v;
}


const std::vector<uint32_t> &algorithm_xorwow::getRefereeValues()
{
  static std::vector<uint32_t> sum_refs;
  if (sum_refs.empty()) {
    sum_refs.reserve(os.total_work_items);

    for (size_t gid = 0; gid < os.total_work_items; gid++) {
      // kernel: initialize
      xorwow_state m {};
      xorwowSeed(&m, (uint32_t)os.seed + (uint32_t)gid);

      // kernel: sumStream
      uint32_t sum_ref = 0;
      for (uint32_t i = 0; i < os.sequence_lengths; i++) {
        sum_ref += xorwowGenerate(&m) & 0xFFFF;
      }
      sum_refs.push_back(sum_ref);
    }
  }
  return sum_refs;
}

bool algorithm_xorwow::run(
  cl_device_id dev_id,
  std::stringstream &cell_output,
  std::stringstream &verbose_output)
{
  return run_impl<xorwow_state>(dev_id, cell_output, verbose_output);
}
