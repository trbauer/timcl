#include "algorithm.hpp"

#include <iostream>
#include <fstream>


std::string algorithm::load_source(
  std::stringstream &verbose_output) const
{
  std::stringstream src;

  auto tryPrefix = [&] (std::string pfx) {
    std::stringstream file;
    file << pfx;
    if (pfx.size() > 0 &&
      pfx[pfx.size() - 1] != '/' &&
      pfx[pfx.size() - 1] != '\\')
    {
      file << '/';
    }
    file << name() << ".cl";

    std::ifstream ifs(file.str(), std::ifstream::in);
    if (!ifs.good())
      return false;

    if (os.debug())
      verbose_output << file.str() << ": found kernel file\n";

    while (ifs.good()) {
      int c = ifs.get();
      if (c != EOF)
        src << (char)c;
      else
        break;
    }
    ifs.close();

    return true;
  };

  auto dirs = os.search_directories;
  dirs.push_back(""); // try CWD
  bool found_file = false;
  for (const auto &pfx : os.search_directories) {
    if (tryPrefix(pfx)) {
      found_file = true;
      break;
    }
  }
  if (!found_file) {
    mincl::fatal(name(), ".cl: cannot find file (use -I=...)");
  }

  return src.str();
}


bool algorithm::referee(
  cl_device_id dev_id,
  const uint32_t *sut_sums,
  std::stringstream &cell_output,
  std::stringstream &verbose_output)
{
  const auto &sum_refs = getRefereeValues();

  bool success = true;
  for (size_t gid = 0; gid < os.total_work_items; gid++) {
    if (sut_sums[gid] != sum_refs[gid]) {
      cell_output << "ERROR";
      verbose_output <<
        "[" << name() << "]: " <<
        "mismatch on element " << gid << ": " <<
        "ref:" << sum_refs[gid] << " vs "
        "sut:" << sut_sums[gid];
      success = false;
      break;
    }
  }

  return success;
}

