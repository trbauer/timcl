#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP

#include "mincl.hpp"
#include "opts.hpp"

#include <string>
#include <sstream>
#include <vector>


struct random_opts : common_opts {
  //
  bool                             fail_fast = false;

  std::vector<std::string>         filters;

  bool                             referee_output = false;

  bool                             save_binaries = false;

  std::vector<std::string>         search_directories;

  uint32_t                         sequence_lengths = 8*1024;

  uint64_t                         total_work_items = 16*1024; // 1024*1024;


  static random_opts parseOpts(int argc, const char *argv[]);

  bool matchesFilter(std::string name) const {
    if (filters.empty())
      return true;
    for (const auto &f : filters) {
      if (name.find(f) != std::string::npos)
        return true;
    }
    return false;
  }
};


struct algorithm {
  random_opts os;

  algorithm(random_opts _os) : os(_os) { }

  virtual const char *name() const = 0;

  virtual bool run(
    cl_device_id dev_id,
    std::stringstream &cell_output,
    std::stringstream &verbose_output) = 0;

  virtual const std::vector<uint32_t> &getRefereeValues() = 0;

  /////////////////////////////////////////////////////////////////////////////
  // provided for algorithm instances
  template <typename S>
  bool run_impl(
    cl_device_id dev_id,
    std::stringstream &cell_output,
    std::stringstream &verbose_output);

  bool referee(
    cl_device_id dev_id,
    const uint32_t *sut_sums,
    std::stringstream &cell_output,
    std::stringstream &verbose_output);

  std::string load_source(std::stringstream &verbose_output) const;
};

struct algorithm_mt19937 : algorithm {
  algorithm_mt19937(random_opts os) : algorithm(os) { }

  virtual const char *name() const override {return "mt19937";}

  virtual bool run(
    cl_device_id dev_id,
    std::stringstream &cell_output,
    std::stringstream &verbose_output) override;

  virtual const std::vector<uint32_t> &getRefereeValues() override;
};

// http://cas.ee.ic.ac.uk/people/dt10/research/rngs-gpu-mwc64x.html
// multiply with carry
struct algorithm_mwc64x : algorithm {
  algorithm_mwc64x(random_opts os) : algorithm(os) { }

  virtual const char *name() const override {return "mwc64x";}

  virtual bool run(
    cl_device_id dev_id,
    std::stringstream &cell_output,
    std::stringstream &verbose_output) override;

  virtual const std::vector<uint32_t> &getRefereeValues() override;
};

//
// Algorithm "xorwow" from p. 5 of Marsaglia, "Xorshift RNGs"
struct algorithm_xorwow : algorithm {
  algorithm_xorwow(random_opts os) : algorithm(os) { }

  virtual const char *name() const override {return "xorwow";}

  virtual bool run(
    cl_device_id dev_id,
    std::stringstream &cell_output,
    std::stringstream &verbose_output) override;

  virtual const std::vector<uint32_t> &getRefereeValues() override;
};

//
// PCG from https://www.pcg-random.org/
// permuted congruental generator
struct algorithm_pcg : algorithm {
  algorithm_pcg(random_opts os) : algorithm(os) { }

  virtual const char *name() const override {return "pcg";}

  virtual bool run(
    cl_device_id dev_id,
    std::stringstream &cell_output,
    std::stringstream &verbose_output) override;

  virtual const std::vector<uint32_t> &getRefereeValues() override;
};


///////////////////////////////////////////////////////////////////////////////
template <typename S>
inline bool algorithm::run_impl(
  cl_device_id dev_id,
  std::stringstream &cell_output,
  std::stringstream &verbose_output)
{
  const auto src = load_source(verbose_output);

  auto build_opts = os.extra_build_options;
  build_opts += " -DRND_SEQ_LEN=";
  build_opts += std::to_string(os.sequence_lengths);

  std::string namestr = name();

  mincl::config cfg(
    dev_id,
    namestr + ".cl",
    src,
    build_opts,
    CL_QUEUE_PROFILING_ENABLE);
  if (os.save_binaries) {
    cfg.save_binary(namestr);
  }

  // ensure that the buffer we create will be the right size
  auto sizeOfBuf = cfg.buffer_allocate<cl_uint>(1);
  auto &kSizeof = cfg.get_kernel(namestr + "_sizeOfState");
  kSizeof.set_arg_mem(0, sizeOfBuf);
  auto dSizeofState = kSizeof.dispatch(1);
  dSizeofState.sync();
  auto sizeOfState = sizeOfBuf[0];
  if (sizeOfState != sizeof(S)) // SPECIFY: > would be okay since it's all device-only
    mincl::fatal(name(), ": mismatch in state size structure ", sizeOfState, " vs ", sizeof(S));

  // initialize memory for the random number generators on the device
  auto ms = cfg.buffer_allocate<S>(os.total_work_items);
  auto sums = cfg.buffer_allocate<cl_uint>(os.total_work_items);
  auto &kInit = cfg.get_kernel(namestr + "_initialize");
  kInit.set_arg_mem(0, ms);
  kInit.set_arg_uniform(1, (cl_uint)os.seed);

  // setup the stream kernel
  auto &kStream = cfg.get_kernel(namestr + "_stream");
  kStream.set_arg_mem(0, ms);
  kStream.set_arg_mem(1, sums);

  // initialize and then stream
  auto dInit = kInit.dispatch(os.total_work_items);
  auto dStream = kStream.dispatch(os.total_work_items);
  const auto nsInit = dInit.sync();
  const auto nsStream = dStream.sync();

  double elapsed_s = (nsInit + nsStream)/1000.0/1000.0/1000.0;
  double rate_g_s =
    os.total_work_items*os.sequence_lengths/elapsed_s/1000.0/1000.0/1000.0;

  bool data_correct = true;
  if (os.referee_output) {
    sums.read(
      [&] (const cl_uint *sums) {
        data_correct = referee(dev_id, sums, cell_output, verbose_output);
      });
  }

  verbose_output << "[" << mincl::get_device_name(dev_id) << "] " <<
    os.total_work_items*os.sequence_lengths << " streams in " <<
    std::fixed <<  std::setprecision(3) <<
    (nsInit + nsStream)/1000.0/1000.0 << " ms\n";

  if (data_correct) {
    cell_output << std::fixed << std::setprecision(2) << rate_g_s;
  }

  return data_correct;
}

#endif // ALGORITHM_HPP