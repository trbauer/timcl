#include "algorithm.hpp"
#include "mincl.hpp"
#include "opts.hpp"

#include <cstdint>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

//
// TODO: enable randomness checks? Test01 http://simul.iro.umontreal.ca/testu01/tu01.html
// TODO: make mt19937 use STL parameters http://www.cplusplus.com/reference/random/

random_opts random_opts::parseOpts(int argc, const char* argv[])
{
  static const random_opts DEFAULTS;

  random_opts os;
  for (int ai = 1; ai < argc; ai++) {
    std::string arg = argv[ai];
    std::string key = arg, val = "";
    auto eq_ix = arg.find('=');
    if (eq_ix != std::string::npos) {
      key = arg.substr(0, eq_ix + 1); // include the = so we can distinguish flags
      val = arg.substr(eq_ix + 1);
    }

    if (arg == "-h" || arg == "--help") {
      std::cout <<
        "Random Ops Rate\n" <<
        argv[0] << " " << RANDOM_VERSION_STRING << "\n" <<
        " " << arg << " OPTIONS\n" <<
        "where COMMON OPTIONS are:\n" <<
        commonOptsHelp() <<
        "\n"
        "and where BENCHMARK OPTIONS are:\n"
        "  --fail-fast            fail fast\n"
        "  -f/--filter=STRING     adds an algorithm filter\n"
        "  -I/--include=DIR       adds a search directory\n"
        "  --referee              referees the algorithm outputs\n"
        "  --save-binaries        saves the program binaries\n"
        "  -seq=INT               generation sequence length"
                                    " (defaults to " <<
                                      DEFAULTS.sequence_lengths << ")\n"
        "  -twi=INT               total work items"
                                    " (defaults to " <<
                                      DEFAULTS.total_work_items << ")\n"
        "    --total-work-items=INT   ... same as -twi=.."
        "EXAMPLES:\n"
        "  % " << argv[0] << " -d=0 -I=random --filter=xorwow"
        "    Run the xorwow generator on device 0; search the directory "
                                                    "\"random\" for the\n"
        "    OpenCL file xorwow.cl containing the implementation\n"
        "\n"
        "  % " << argv[0] << " -d=0 -I=random --filter=xorwow -twi=1 -seq=8 --referee"
        "    as above, but run only 1 work item (-twi) and sum a sequence"
                                                    "length of 8 (-seq)\n"
        "    refereeing the output with a host implementation\n"
        ;
      exit(EXIT_SUCCESS);
    ///////////////////////////////////////////////////////////////////////////
    } else if (key == "-f=" || key == "--filter=") {
      os.filters.push_back(val);
    } else if (key == "--fail-fast") {
      os.fail_fast = true;
    } else if (
      key == "-I" || key == "-include" ||
      key == "-f" || key == "--filter" ||
      key == "-seq" || key == "-twi")
    {
      mincl::fatal("options should be of the form -OPT=DIR");
    } else if (key == "-I=" || key == "--include=") {
      os.search_directories.push_back(val);
    } else if (key == "--referee") {
      os.referee_output = true;
    } else if (key == "--save-binaries") {
      os.save_binaries = true;
  } else if (key == "-seq=" || key == "--sequence-length=") {
      os.sequence_lengths = (uint32_t)mincl::parse_uint64(val, "integer", true);
    } else if (key == "-twi=" || key == "--total-work-items=") {
      os.total_work_items = mincl::parse_uint64(val, "integer", true);
    // } else if (val.empty()) {
    //  // it's an arg
    //  os.filters.emplace_back(arg);
    ///////////////////////////////////////////////////////////////////////////
    } else {
      os.parseCommonOpt(arg, key, val);
    }
  }

  if (os.devices.empty()) {
    mincl::fatal("at least one -d=.. option required");
  }

  return os;
}


const static int DEVICE_NAME_COLUMN_WIDTH = 32;
const static int ALGORITHM_COLUMN_WIDTH = 16;

int main(int argc, const char *argv[])
{
  random_opts os = random_opts::parseOpts(argc, argv);

  algorithm_mt19937 mt19937(os);
  algorithm_mwc64x mwc64x(os);
  algorithm_pcg pcg(os);
  algorithm_xorwow xorwow(os);
  algorithm *algs[] {
      &mt19937,
      &mwc64x,
      &pcg,
      &xorwow
  };

  std::cout << "  " << std::setw(ALGORITHM_COLUMN_WIDTH) << "";
  for (cl_device_id dev_id : os.devices) {
    std::cout << "  " << std::setw(DEVICE_NAME_COLUMN_WIDTH) <<
      std::right << mincl::get_device_info_string(dev_id, CL_DEVICE_NAME);
  }
  std::cout << "\n";

  bool success = true, matched_anything = false;

  for (algorithm *alg : algs) {
    if (!os.matchesFilter(alg->name())) {
      continue;
    }
    matched_anything = true;

    std::cout << "  " << std::setw(ALGORITHM_COLUMN_WIDTH - 2) << alg->name();
    std::cout << "  ";

    std::stringstream verbose_output;
    for (cl_device_id dev_id : os.devices) {
      std::stringstream cell_output;

      success &= alg->run(dev_id, cell_output, verbose_output);

      std::cout << "  " << std::setw(DEVICE_NAME_COLUMN_WIDTH) <<
        std::right << cell_output.str();
    }

    std::cout << "  (G uint/s)\n";
    auto verb_str = verbose_output.str();
    if (os.verbose() && !verb_str.empty())
      std::cout << verbose_output.str() << "\n";
  }
  if (!matched_anything) {
      std::cout << "failed to match any algorithm; they are:\n";
      for (const algorithm *alg : algs) {
          std::cout << "  * " << alg->name() << "\n";
      }
      success = false;
  }

  return success ? EXIT_SUCCESS : EXIT_FAILURE;
}

