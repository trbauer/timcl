/* Period parameters */
#define __N__ 624
#define __M__ 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

typedef struct mt19937_stateStruct
{
  uint mt[__N__];
  int mti;
} mt19937state;

kernel void mt19937_sizeOfState(global uint *sizeOfState)
{
  sizeOfState[0] = sizeof(mt19937state);
}

void initializeMersenneState(global mt19937state* m, uint seed)
{
  int i;

  for (i = 0; i < __N__; i++)
  {
    m->mt[i] = seed & 0xffff0000;
    seed = 69069 * seed + 1;
    m->mt[i] |= (seed & 0xffff0000) >> 16;
    seed = 69069 * seed + 1;
  }
  m->mti = __N__;
}


kernel void mt19937_initialize(global mt19937state* ms, uint seed)
{
  uint id = get_global_id(0);
  initializeMersenneState(ms + id, id + seed);
}

uint mersenneRandU32(private mt19937state* m)
{
  uint y;
  uint mag01[2];
  mag01[0] = 0x0;
  mag01[1] = MATRIX_A;

  /* mag01[x] = x * MATRIX_A  for x=0,1 */

  if (m->mti >= __N__)
  { /* generate N words at one time */
    int kk = 0;
    while (kk < __N__ - __M__)
    {
      y = (m->mt[kk] & UPPER_MASK) | (m->mt[kk + 1] & LOWER_MASK);
      m->mt[kk] = m->mt[kk + __M__] ^ (y >> 1) ^ mag01[y & 0x1];
      ++kk;
    }
    while (kk < __N__ - 1)
    {
      y = (m->mt[kk] & UPPER_MASK) | (m->mt[kk + 1] & LOWER_MASK);
      m->mt[kk] = m->mt[kk + (__M__ - __N__)] ^ (y >> 1) ^ mag01[y & 0x1];
      ++kk;
    }

    y = (m->mt[__N__ - 1] & UPPER_MASK) | (m->mt[0] & LOWER_MASK);
    m->mt[__N__ - 1] = m->mt[__M__ - 1] ^ (y >> 1) ^ mag01[y & 0x1];

    m->mti = 0;
  }

  y = m->mt[m->mti++];
  y ^= TEMPERING_SHIFT_U(y);
  y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
  y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
  y ^= TEMPERING_SHIFT_L(y);

  return y;
}

#ifndef RND_SEQ_LEN
#error "RND_SEQ_LEN not defined"
#endif

kernel void mt19937_stream(const global mt19937state* ms, global uint *sums)
{
  const uint id = get_global_id(0);

  mt19937state m = ms[id];

  uint sum = 0;
  for (uint i = 0; i < RND_SEQ_LEN; i++) {
    sum += mersenneRandU32(&m) & 0xFFFF;
  }

  sums[id] = sum;
}
