// Part of MWC64X by David Thomas, dt10@imperial.ac.uk
// This is provided under BSD, full license is with the main package.
// See http://www.doc.ic.ac.uk/~dt10/research

// Pre: a < M, b < M
// Post: r = (a+b) mod M
ulong MWC_AddMod64(ulong a, ulong b, ulong M)
{
  ulong v = a + b;
  if ((v >= M) || (v < a))
    v = v - M;
  return v;
}

// Pre: a < M, b < M
// Post: r = (a*b) mod M
// This could be done more efficently, but it is portable, and should
// be easy to understand. It can be replaced with any of the better
// modular multiplication algorithms (for example if you know you have
// double precision available or something).
ulong MWC_MulMod64(ulong a, ulong b, ulong M)
{
  ulong r = 0;
  while (a != 0) {
    if (a & 1)
      r = MWC_AddMod64(r, b, M);
    b = MWC_AddMod64(b, b, M);
    a = a >> 1;
  }
  return r;
}


// Pre: a < M, e >= 0
// Post: r = (a^b) mod M
// This takes at most ~64^2 modular additions, so probably about 2^15 or so instructions on
// most architectures
ulong MWC_PowMod64(ulong a, ulong e, ulong M)
{
  // printf("\n");
  // printf(" D: a:0x%016llx\n", a);
  ulong sqr = a, acc = 1;
  while (e != 0) {
    // printf(" D: e:0x%016llx  acc:0x%016llx  sqr:0x%016llx\n", e, acc, sqr);
    if (e & 1)
      acc = MWC_MulMod64(acc, sqr, M);
    sqr = MWC_MulMod64(sqr, sqr, M);
    e = e >> 1;
  }
  // printf(" D: acc:0x%016llx\n", acc);
  return acc;
}

uint2 MWC_SkipImpl_Mod64(uint2 curr, ulong A, ulong M, ulong distance)
{
  ulong m = MWC_PowMod64(A, distance, M);
  ulong x = curr.x * (ulong)A + curr.y;
  x = MWC_MulMod64(x, m, M);
  return (uint2)((uint)(x / A), (uint)(x % A));
}

uint2 MWC_SeedImpl_Mod64(
  ulong A, ulong M,
  uint vecSize, uint vecOffset,
  ulong streamBase, ulong streamGap,
  ulong gid)
{
  // This is an arbitrary constant for starting LCG jumping from. I didn't
  // want to start from 1, as then you end up with the two or three first values
  // being a bit poor in ones - once you've decided that, one constant is as
  // good as any another. There is no deep mathematical reason for it, I just
  // generated a random number.
  // enum {MWC_BASEID = 4077358422479273989UL};
  const ulong MWC_BASEID = 4077358422479273989UL;

  ulong dist = streamBase + (gid * vecSize + vecOffset) * streamGap;

  // printf("\n");
  // printf(" D: A:0x%016llx\n", A);
  ulong m = MWC_PowMod64(A, dist, M);

  ulong x = MWC_MulMod64(MWC_BASEID, m, M);
  return (uint2)((uint)(x / A), (uint)(x % A));
}


//! Represents the state of a particular generator
typedef struct{uint x; uint c;} mwc64x_state_t;

// replace enums with the intended proper types; host and device compilers
// seems to do different things with these literal values
//
// enum {MWC64X_A = 4294883355U};
constant uint MWC64X_A = 4294883355UL;
// enum {MWC64X_M = 18446383549859758079UL};
constant ulong MWC64X_M = 18446383549859758079UL;

void MWC64X_Step(private mwc64x_state_t *s)
{
  uint X = s->x, C = s->c;

  uint Xn = MWC64X_A*X + C;
  uint carry = (uint)(Xn < C);        // The (Xn<C) will be zero or one for scalar
  uint Cn = mad_hi(MWC64X_A, X, carry);

  s->x = Xn;
  s->c = Cn;
}

void MWC64X_Skip(private mwc64x_state_t *s, ulong distance)
{
  uint2 tmp = MWC_SkipImpl_Mod64((uint2)(s->x, s->c), MWC64X_A, MWC64X_M, distance);
  s->x = tmp.x;
  s->c = tmp.y;
}

void MWC64X_SeedStreams(
  private mwc64x_state_t *s,
  ulong baseOffset,
  ulong perStreamOffset,
  ulong gid)
{
  uint2 tmp = MWC_SeedImpl_Mod64(
    MWC64X_A, MWC64X_M,
    1, 0,
    baseOffset, perStreamOffset,
    gid);
  s->x = tmp.x;
  s->c = tmp.y;
}

//! Return a 32-bit integer in the range [0..2^32)
uint MWC64X_NextUint(private mwc64x_state_t *s)
{
  uint res = s->x ^ s->c;
  MWC64X_Step(s);
  return res;
}

///////////////////////////////////////////////////////////////////////////////
kernel void mwc64x_sizeOfState(global uint *sizeOfState)
{
  sizeOfState[0] = sizeof(mwc64x_state_t);
}

__kernel void mwc64x_initialize(__global mwc64x_state_t* ms, uint seed)
{
  uint id = get_global_id(0);
  mwc64x_state_t m = ms[id];
  MWC64X_SeedStreams(&m, 0, 1, seed + get_global_id(0));
  ms[id] = m;
}


__kernel void mwc64x_stream(
  const __global mwc64x_state_t* ms,
  __global uint *sums)
{
  const uint id = get_global_id(0);

  mwc64x_state_t m = ms[id];

  uint sum = 0;
  for (uint i = 0; i < RND_SEQ_LEN; i++) {
    sum += MWC64X_NextUint(&m) & 0xFFFF;
  }

  sums[id] = sum;
}
