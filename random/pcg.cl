struct pcg_state_setseq_64 {
    ulong state;
    ulong inc;
};
typedef struct pcg_state_setseq_64 pcg32_random_t;

kernel void pcg_sizeOfState(global uint *sizeOfState)
{
  sizeOfState[0] = sizeof(pcg32_random_t);
}

///////////////////////////////////////////////////////////////////////////////
static uint pcg32_random_r(pcg32_random_t* rng);

static void pcg32_srandom_r(pcg32_random_t* rng, ulong initstate, ulong initseq)
{
  rng->state = 0U;
  rng->inc = (initseq << 1u) | 1u;
  pcg32_random_r(rng);
  rng->state += initstate;
  pcg32_random_r(rng);
}

kernel void pcg_initialize(global pcg32_random_t* ms, uint seed)
{
  uint id = get_global_id(0);
  pcg32_random_t m = ms[id];
  pcg32_srandom_r(&m, seed, get_global_id(0));
  ms[id] = m;
}

///////////////////////////////////////////////////////////////////////////////
static uint pcg32_random_r(pcg32_random_t* rng)
{
  ulong oldstate = rng->state;
  rng->state = oldstate * 6364136223846793005UL + rng->inc;
  uint xorshifted = (uint)(((oldstate >> 18u) ^ oldstate) >> 27u);
  uint rot = oldstate >> 59u;
  return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}

kernel void pcg_stream(const global pcg32_random_t *ms, global uint *sums)
{
  const uint id = get_global_id(0);

  pcg32_random_t m = ms[id];
  uint sum = 0;
  for (uint i = 0; i < RND_SEQ_LEN; i++) {
    sum += pcg32_random_r(&m) & 0xFFFF;
  }

  sums[id] = sum;
}
