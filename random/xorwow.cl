typedef struct xorwow_state {
  uint x, y, z, w, v, d;
} xorwow_state_t;

void xorwowSeed(private xorwow_state_t *state, uint seed)
{
  //   static unsigned long
  //    x=123456789, y=362436069, z=521288629,
  //    w=88675123, v=5783321, d=6615241;
  state->x = 123456789, state->y = 362436069, state->z = 521288629;
  state->w =  88675123, state->v =   5783321, state->d =   6615241;
  //
  // Here's where I don't know the right thing to do: use the seed
  // to influence the sequence.  JSAT uses parent java.util.Random's class
  // to use raw random values.
  //
  // using this: 0 makes it use the reference values
  state->x ^= (seed >>  2) ^ (seed <<  2);
  state->y ^= (seed >>  4) ^ (seed <<  4);
  state->z ^= (seed >>  6) ^ (seed <<  6);
  state->w ^= (seed >>  8) ^ (seed <<  8);
  state->v ^= (seed >> 10) ^ (seed << 10);
  state->d ^= (seed >> 12) ^ (seed << 12);
}


// Algorithm "xorwow" from p. 5 of Marsaglia, "Xorshift RNGs"
// unsigned long xorwow()
// {
//   static unsigned long
//    x=123456789, y=362436069, z=521288629,
//    w=88675123, v=5783321, d=6615241;
//   unsigned long t;
//   t = (x � (x >> 2));
//   x = y;
//   y = z;
//   z = w;
//   w = v;
//   v = (v � (v << 4)) � (t � (t << 1));
//   return (d += 362437) + v;
// }

uint xorwowGenerate(private xorwow_state_t *state)
{
  uint t = state->x ^ (state->x >> 2);
  state->x = state->y;
  state->y = state->z;
  state->z = state->w;
  state->w = state->v;
  state->v = (state->v ^ (state->v << 4)) ^ (t ^ (t << 1));
  return (state->d += 362437) + state->v;
}

///////////////////////////////////////////////////////////////////////////////
kernel void xorwow_sizeOfState(global uint *sizeOfState)
{
  sizeOfState[0] = sizeof(xorwow_state_t);
}

kernel void xorwow_initialize(global xorwow_state_t *ms, uint seed)
{
  uint id = get_global_id(0);
  xorwow_state_t m = ms[id];
  xorwowSeed(&m, seed + get_global_id(0));
  ms[id] = m;
}


kernel void xorwow_stream(const global xorwow_state_t *ms, global uint *sums)
{
  const uint id = get_global_id(0);

  xorwow_state_t m = ms[id];

  uint sum = 0;
  for (uint i = 0; i < RND_SEQ_LEN; i++) {
    sum += xorwowGenerate(&m) & 0xFFFF;
  }
  // printf("==> %d\n", sum);

  sums[id] = sum;
}
