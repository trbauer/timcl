@echo off

mkdir builds
mkdir builds\vs2019-64
pushd builds\vs2019-64

cmake -G "Visual Studio 16 2019" -A x64 ..\..

popd
