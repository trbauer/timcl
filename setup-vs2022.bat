@echo off
if not exist builds mkdir builds
if not exist builds\vs2022-64 mkdir builds\vs2022-64\
if not exist builds\vs2022-64\install mkdir builds\vs2022-64\install
pushd builds\vs2022-64
@echo ^%  cmake -G "Visual Studio 17 2022" -A x64 -DCMAKE_INSTALL_PREFIX=builds\vs2022-64\install ..\..
cmake -G "Visual Studio 17 2022" -A x64 -DCMAKE_INSTALL_PREFIX=builds\vs2022-64\install ..\..
popd
