#ifndef MINCL_HPP
#define MINCL_HPP

// #include "clm.hpp"

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <cl/cl.h>

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <cstdint>
#include <functional>
#include <fstream>
#include <initializer_list>
#include <iostream>
#include <iomanip>
#include <random>
#include <string>
#include <sstream>
#include <type_traits>
#include <vector>

namespace mincl
{
  ///////////////////////////////////////////////////////
  // text tools
  // EXAMPLES:
  //   auto s = format("foo=", foo_val, "; bar=", bar_val);
  //
  // extra ostream decorators:
  // a hex number of with fill of 8
  //   std::cout << hex(x, 8)
  // a fractional number with precision 5
  //   std::cout << frac(x, 5)
  // formatting with a fraction inline with 3 digits of precions
  //   auto s = format("foo=", foo_val, "; bar=", frac(bar_val, 3));
  //    or
  //   format_to(std::cout, "foo=", foo_val, "; bar=", frac(bar_val, 3));
  // left-aligned column of 32 rendering x (of any type T):
  //   std::cout << coll(x, 32) << ...
  // right-aligned with dots padding
  //   std::cout << "0x" << colr(x, 8, '.')
  // with variable column setting
  //   std::cout << col(x, pad::L, 32)
  //
  template <typename...Ts>
  static void _format_to_h(std::ostream &os) { }
  template <typename T, typename... Ts>
  static void _format_to_h(std::ostream &os, T t, Ts... ts) {
    os << t;
    _format_to_h(os, ts...);
  }

  template <typename...Ts>
  static void format_to(std::ostream &os, Ts...ts) {
    _format_to_h(os, ts...);
  }

  template <typename...Ts>
  static std::string format(Ts...ts) {
    std::stringstream ss; format_to(ss, ts...); return ss.str();
  }

  // hex stream decorator (a right-justified column)
  struct hex
  {
    uint64_t value;
    int columns;
    template <typename T>
    hex(T v, int cls = 2 * sizeof(T)) : value((uint64_t)v), columns(cls) { }
  };
  static inline std::ostream &operator <<(std::ostream &os, hex h) {
    std::stringstream ss;
    ss << std::setw(h.columns) <<
      std::setfill('0') << std::hex << std::uppercase << h.value;
    os << ss.str();
    return os;
  }

  struct frac
  {
    union {
      cl_half f16;
      float f32;
      double f64;
    };
    const enum {F16 = 1, F32, F64} tag;
    const int columns;
    const int prec;
    frac(cl_half v, int _prec = 2, int cols = -1)
        : columns(cols), prec(_prec), tag(F16) {
      f16 = v;
    }
    frac(float v, int _prec = 3, int cols = -1)
        : columns(cols), prec(_prec), tag(F32) {
      f32 = v;
    }
    frac(double v, int _prec = 4, int cols = -1)
        : columns(cols), prec(_prec), tag(F64) {
      f64 = v;
    }
  }; // frac

  static constexpr float half_bits_to_float(uint16_t);

  static inline std::ostream &operator <<(std::ostream &os, frac f) {
    std::stringstream ss;
    if (f.columns >= 0)
      ss << std::setw(f.columns);
    ss << std::setprecision(f.prec) << std::fixed;
    if (f.tag == frac::F32)
      ss << f.f32;
    else if (f.tag == frac::F64)
      ss << f.f64;
    else
      ss << half_bits_to_float(f.f16);
    os << ss.str();
    return os;
  }

  enum class pad {L, R};

  template <typename T>
  struct col
  {
    const pad pd;
    const T &value;
    size_t width;
    char pad_fill;
    col(const T &val, pad p, size_t wid, char f = ' ')
      : pd(p), value(val), width(wid), pad_fill(f) { }
  }; // col
  template <typename T>
  struct coll : col<T>
  {
    coll(const T &val, size_t wid, char f = ' ')
      : col<T>(val, pad::R, wid, f) { }
  };
  template <typename T>
  struct colr : col<T>
  {
    colr(const T &val, size_t wid, char f = ' ')
      : col<T>(val, pad::L, wid, f) { }
  };

  template <typename T>
  static inline std::ostream &operator<< (std::ostream &os, const col<T> &p) {
    auto s = format(p.value);
    std::stringstream ss;
    if (p.pd == pad::L) {
      for (size_t i = s.size(); i < p.width; i++)
        ss << p.pad_fill;
    }
    ss << s;
    if (p.pd == pad::R) {
      for (size_t i = s.size(); i < p.width; i++)
        ss << p.pad_fill;
    }
    os << ss.str();
    return os;
  }

  /////////////////////////////////////////////////////////////////////////////

  // fatal due to user input
  template <typename... Ts> [[noreturn]] static void fatal(Ts... ts) {
    std::string msg = format(ts...);
    std::cerr << msg;
    if (!msg.empty() && msg[msg.size() - 1] != '\n')
      std::cerr << "\n";
    exit(EXIT_FAILURE);
  }

  // fatal due to the impossible
  template <typename... Ts> [[noreturn]] static void fatal_internal(Ts... ts) {
    std::string msg = format(ts...);
    std::cerr << "INTERNAL ERROR: " << msg;
    if (!msg.empty() && msg[msg.size() - 1] != '\n')
      std::cerr << "\n";
    exit(2);
  }


  /////////////////////////////////////////////////////////////////////////////
  struct random_state {
    std::mt19937 gen;

    random_state() : gen(std::random_device()()) { }
    random_state(unsigned seed) : gen(seed) { }
    random_state(const char *seed) {
      std::string str = seed;
      std::seed_seq ss(str.begin(), str.end());
      gen.seed(ss);
    }
  }; // random_state

  template <typename T,typename R = T>
  static void randomize_integral(
    random_state &rnd,
    T *vals,
    size_t elems,
    T lo = std::numeric_limits<T>::min(),
    T hi = std::numeric_limits<T>::max())
  {
    std::uniform_int_distribution<R> d(lo, hi);
    for (size_t i = 0; i < elems; i++) {
      vals[i] = T(d(rnd.gen));
    }
  }

  template <typename T, typename R = T>
  static void randomize_real(
    random_state &rnd,
    T *vals,
    size_t elems,
    T lo = (T)0.0f,
    T hi = (T)1.0f)
  {
    std::uniform_real_distribution<R> d((R)lo,(R)hi);
    for (size_t i = 0; i < elems; i++) {
      vals[i] = T(d(rnd.gen));
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  template <typename T>
  static T* alloc(size_t n) {
    void *ptr = malloc(n * sizeof(T));
    if (!ptr)
      fatal("mincl.hpp: alloc(", n * sizeof(T), ")");
    return (T *)ptr;
  }

#if 0
  template <typename T>
  static T* alloc(size_t n, size_t a = 0) {
    void *ptr = nullptr;
    if (a == 0) {
      ptr = std::malloc(n * sizeof(T));
    } else {
      ptr = _aligned_malloc(n * sizeof(T), a);
      // MSVC not finding this in cstdlib
      // ptr = std::aligned_alloc(n * sizeof(T), a);
    }
    if (!ptr)
      fatal("mincl.hpp: alloc(", n * sizeof(T), ")");
    return (T *)ptr;
  }
#endif
  template <typename T>
  struct auto_alloc {
    T *ptr;
    auto_alloc(size_t n) : ptr(alloc<T>(n * sizeof(T))) {
    }
    auto_alloc(const auto_alloc &) = delete;
    ~auto_alloc() {free(ptr);}
    operator T*() {return ptr;}
  };

  static std::string status_to_symbol(cl_int error);

  #define CL_COMMAND(FUNC,...) \
    do { \
        cl_int __err = FUNC(__VA_ARGS__); \
        if (__err != CL_SUCCESS) \
          mincl::fatal(#FUNC, ": line ", __LINE__, \
            " returned ", mincl::status_to_symbol(__err)); \
    } while (0);
  #define CL_COMMAND_CREATE(ASSIGN_TO,FUNC,...) \
    do { \
        cl_int __err = 0; \
        ASSIGN_TO = FUNC(__VA_ARGS__, &__err); \
        if (__err != CL_SUCCESS) \
          mincl::fatal(#FUNC, ": line ", __LINE__, \
            " returned ", mincl::status_to_symbol(__err)); \
    } while (0);


  struct ndr {
    size_t dims[3];
    size_t num_dims;
    ndr() : num_dims(0) {dims[0] = dims[0] = dims[2] = 0;}
    ndr(size_t gx)  : num_dims(1) {dims[0] = gx; dims[1] = dims[2] = 1;}
    ndr(size_t gx, size_t gy)
      : num_dims(2) {dims[0] = gx; dims[1] = gy; dims[2] = 1;}
    ndr(size_t gx, size_t gy, size_t gz)
      : num_dims(3) {dims[0] = gx; dims[1] = gy; dims[2] = gz;}
  };


  void CL_CALLBACK contextCallbackDispatcher(
    const char *, const void *, size_t, void *);

  /////////////////////////////////////////////////////////////////////////////
  // primitive host-side support for half data type
  static constexpr float     half_bits_to_float(uint16_t);
  static constexpr uint16_t  float_to_half_bits(float);
  //
  struct half {
    uint16_t bits;

    static half from_bits(uint16_t bits) {half h; h.bits = bits; return h;}

    constexpr half() : bits(0) {}
    constexpr half(float f) : bits(float_to_half_bits(f)) { }
    constexpr half(double f) : half((float)f) { }
    constexpr half(int64_t i) : half((double)i) { }
    constexpr half(uint64_t i) : half((double)i) { }
    constexpr operator float() const {return half_bits_to_float(bits);}
    constexpr operator double() const {return (float)*this;}

    explicit operator int() const{return (int)half_bits_to_float(bits);}

    half &operator=(const half &rhs) {bits = rhs.bits; return *this;}

    constexpr half operator+(const half &rhs) const {
      return half(half_bits_to_float(bits) + half_bits_to_float(rhs.bits));}
    constexpr half operator-(const half &rhs) const {
      return half(half_bits_to_float(bits) - half_bits_to_float(rhs.bits));}
    constexpr half operator*(const half &rhs) const {
      return half(half_bits_to_float(bits)*half_bits_to_float(rhs.bits));}
    constexpr half operator/(const half &rhs) const {
      return half(half_bits_to_float(bits)/half_bits_to_float(rhs.bits));}
    constexpr bool operator==(const half &rhs) const {
      return half_bits_to_float(bits) == half_bits_to_float(rhs.bits);}
    constexpr bool operator!=(const half &rhs) const {return !(*this == rhs);}
    constexpr bool operator<(const half &rhs) const {
      return half_bits_to_float(bits) < half_bits_to_float(rhs.bits);}
    constexpr bool operator<=(const half &rhs) const {
      return half_bits_to_float(bits) <= half_bits_to_float(rhs.bits);}
    constexpr bool operator>(const half &rhs) const {return !(*this <= rhs);}
    constexpr bool operator>=(const half &rhs) const {return !(*this < rhs);}

    constexpr bool is_nan() const;
    // TODO: this should a signaling NaN's bit
    half abs() const {return from_bits(bits & 0x7FFF);}
  };
  static_assert(sizeof(half) == sizeof(uint16_t), "wrong size for half");
  static_assert(sizeof(half) == 2, "unexpected size for half");

  // constructors in unions....
  typedef union {
    half CL_ALIGNED(4) s[2];
    struct {half  x, y;};
    struct {half  s0, s1;};
    struct {half  lo, hi;};
  } half2;
#ifdef cl_half2
  static_assert(sizeof(half2) == sizeof(cl_half2), "unexpected half2 size");
#endif
  typedef union {
    half  CL_ALIGNED(8) s[4];
    struct{ half  x, y, z, w; };
    struct{ half  s0, s1, s2, s3; };
    struct{ half2 lo, hi; };
  } half4;
#ifdef cl_half4
  static_assert(sizeof(half4) == sizeof(cl_half4), "unexpected half4 size");
#endif
  typedef union {
    half   CL_ALIGNED(16) s[8];
    struct{half  x, y, z, w;};
    struct{half  s0, s1, s2, s3, s4, s5, s6, s7;};
    struct{half4 lo, hi;};
  } half8;
#ifdef cl_half8
  static_assert(sizeof(half8) == sizeof(cl_half8), "unexpected half8 size");
#endif
  typedef union {
    half  CL_ALIGNED(32) s[16];
    struct {
      half x, y, z, w, __spacer4, __spacer5, __spacer6, __spacer7, __spacer8,
          __spacer9, sa, sb, sc, sd, se, sf;
    };
    struct {half  s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, sA, sB, sC, sD, sE, sF;};
    struct {half8 lo, hi;};
  } half16;
#ifdef cl_half8
  static_assert(sizeof(half16) == sizeof(cl_half16), "unexpected half16 size");
#endif

  /////////////////////////////////////////////////////////////////////////////
  //
  struct buffer_base {
    cl_command_queue   queue;
    size_t             length_in_bytes;
    cl_mem             memobj;
    buffer_base(
      cl_command_queue _queue, size_t _length_in_bytes, cl_mem _memobj)
      : queue(_queue), length_in_bytes(_length_in_bytes), memobj(_memobj) { }
  };

  template <typename E>
  struct buffer_mapping
  {
    const buffer_base  &buffer;
    E                 *elems = nullptr;
    buffer_mapping(
      const buffer_base &b,
      cl_map_flags flags = CL_MAP_READ | CL_MAP_WRITE)
      : buffer(b)
    {
      void *host_ptr = nullptr;
      CL_COMMAND_CREATE(host_ptr,
        clEnqueueMapBuffer,
          buffer.queue, buffer.memobj, CL_TRUE, flags,
          0, buffer.length_in_bytes,
          0, nullptr, nullptr);
      elems = (E*)host_ptr;
    }
    ~buffer_mapping() {
      CL_COMMAND(clEnqueueUnmapMemObject,
        buffer.queue, buffer.memobj, (void *)elems, 0, nullptr, nullptr);
      elems = nullptr;
    }
    E &operator[] (size_t ix) {
      if (ix*sizeof(E) >= buffer.length_in_bytes) {
        fatal("mincl.hpp: out of bounds access on buffer");
      }
      return elems[ix];
    }
    const E &operator[] (size_t ix) const {
      if (ix*sizeof(E) >= buffer.length_in_bytes) {
        fatal("mincl.hpp: out of bounds access on buffer");
      }
      return elems[ix];
    }
  };


  template <typename E = int>
  struct buffer : buffer_base {
    buffer() : buffer(nullptr, 0, nullptr) {}
    buffer(cl_command_queue _queue, size_t _elems, cl_mem _memobj)
      : buffer_base(_queue, _elems * sizeof(E), _memobj)
    {
    }
    size_t size() const {return length_in_bytes / sizeof(E);}

    void read(std::function<void (const E *)> apply) const;

    // like read() but allows a return type
    template <typename R>
    R reduce(std::function<R (const E *)> apply) const {
      void *host_ptr = nullptr;
      CL_COMMAND_CREATE(host_ptr,
        clEnqueueMapBuffer,
        queue, memobj, CL_TRUE, CL_MAP_READ,
        0, length_in_bytes,
        0, nullptr, nullptr);
      R ret = apply((const E*)host_ptr);
      CL_COMMAND(clEnqueueUnmapMemObject,
        queue, memobj, host_ptr, 0, nullptr, nullptr);
      return ret;
    }
    void write(std::function<void (E *)> apply) {
      void *host_ptr = nullptr;
      CL_COMMAND_CREATE(host_ptr,
        clEnqueueMapBuffer,
        queue, memobj, CL_TRUE, CL_MAP_WRITE,
        0, length_in_bytes,
        0, nullptr, nullptr);
      apply((E*)host_ptr);
      CL_COMMAND(clEnqueueUnmapMemObject,
        queue, memobj, host_ptr, 0, nullptr, nullptr);
    }
    // read and write
    void access(std::function<void (E *)> apply) {
      void *host_ptr = nullptr;
      CL_COMMAND_CREATE(host_ptr,
        clEnqueueMapBuffer,
        queue, memobj, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE,
        0, length_in_bytes,
        0, nullptr, nullptr);
      apply((E*)host_ptr);
      CL_COMMAND(clEnqueueUnmapMemObject,
        queue, memobj, host_ptr, 0, nullptr, nullptr);
    }
    //
    ////////////////////////////////////////////////////////////////
    // single access functions (very expensive)
    //
    E operator[] (size_t ix) const {return readIndex(ix);}
    //
    E readIndex(size_t ix) const {
      return reduce<E>([&](const E *es) {return es[ix];});
    }
    void writeIndex(size_t ix, const E &e) {
      return write([&](E *es) {return es[ix] = e;});
    }
  };

  ////////////////////////////////////////////////////////////////
  // buffer reading functions
  template <typename E>
  static void read(
    const buffer<E> &b,
    std::function<void (const E *)> apply)
  {
    buffer_mapping<E> m(b, CL_MAP_READ);
    //
    apply(m.elems);
  }
  template <typename E>
  inline void buffer<E>::read(std::function<void (const E *)> apply) const {
    mincl::read<E>(*this, apply);
  }

  template <typename E1, typename E2 = E1>
  static void read(
    const buffer<E1> &b1,
    const buffer<E2> &b2,
    std::function<void (const E1 *, const E2 *)> apply)
  {
    buffer_mapping<E1> m1(b1, CL_MAP_READ);
    buffer_mapping<E2> m2(b2, CL_MAP_READ);
    //
    apply(m1.elems, m2.elems);
  }
  template <typename E1, typename E2 = E1, typename E3 = E1>
  static void read(
    const buffer<E1> &b1,
    const buffer<E2> &b2,
    const buffer<E3> &b3,
    std::function<void (const E1 *, const E2 *, const E3 *)> apply)
  {
    buffer_mapping<E1> m1(b1, CL_MAP_READ);
    buffer_mapping<E2> m2(b2, CL_MAP_READ);
    buffer_mapping<E3> m3(b3, CL_MAP_READ);
    //
    apply(m1.elems, m2.elems, m3.elems);
  }
  template <typename E>
  static void read(
    const std::vector<buffer<E>> &bs,
    std::function<void (const std::vector<const E *> &)> apply)
  {
    std::vector<buffer_mapping<E>> ms;
    std::vector<const E *> es;
    ms.reserve(bs.size());
    for (const buffer &b : bs) {
      ms.emplace_back(b, CL_MAP_READ);
      es.push_back(bs.back().elems);
    }
    apply(es);
  }

  class config {
  public:
    config(
      cl_device_id _dev_id,
      std::string _file_name,
      std::string _file_contents,
      std::string _build_options,
      cl_command_queue_properties qprops = 0);

    config(
      cl_device_id _dev_id,
      std::string _file_name,
      std::string _build_options,
      cl_command_queue_properties qprops = 0);

#ifdef CL_VERSION_2_0
    config(
      cl_device_id _dev_id,
      std::string _file_name,
      std::string _build_options,
      const cl_queue_properties *_qprops);
#endif // CL_VERSION_2_0

    ~config();

    cl_device_id getDeviceId() const {return device_id;}
    cl_context getContext() const {return context;}
    cl_command_queue getQueue() const {return queue;}

    // given "foo.xyz" uses that
    // given "foo" uses "foo.bin" or "foo.ptx" based on device vendor
    void save_binary(std::string file_name) const;

    template <typename E>
    buffer<E> buffer_allocate(
      size_t length, cl_mem_flags flags = CL_MEM_READ_WRITE);

    template <typename E>
    buffer<E> buffer_allocate_init_const(
      size_t length, E init, cl_mem_flags flags = CL_MEM_READ_WRITE);

    template <typename E>
    buffer<E> buffer_allocate_init_mem(
      size_t length, const E *init, cl_mem_flags flags = CL_MEM_READ_WRITE);

    template <typename E>
    buffer<E> buffer_allocate_init_lambda(
      size_t length, cl_mem_flags flags, std::function<E(size_t)> init);

    // explicitly delete a buffer (rarely needed)
    //
    // This should be rarely needed and only used if you must delete a buffer.
    // Normally, all buffers will destruct on config destruction.
    template <typename E>
    void buffer_deallocate(buffer<E> b);

    ///////////////////////////////////////////////////////////////////////////
    // dispatching
    using hrtimepoint = std::chrono::high_resolution_clock::time_point;
    // kernel dispatch
    struct kdispatch {
      const hrtimepoint started, ended; // host timer
      const cl_command_queue queue;
      cl_event event;
      bool is_profiling_queue;

      int64_t host_ns = -1, prof_ns = -1;

      kdispatch(cl_command_queue q, cl_event e, bool _is_profq)
        : started(std::chrono::high_resolution_clock::now())
        , queue(q)
        , event(e)
        , is_profiling_queue(_is_profq)
      {
      }

      ~kdispatch() {
        if (event != nullptr) {
          CL_COMMAND(clReleaseEvent, event);
          event = nullptr;
        }
      }

      cl_long get_prof_ns() const;

      cl_long sync();
      cl_int status() const;

      bool poll() const {
        return status() == CL_COMPLETE;
      }
    }; // struct kdispatch

    // a kernel instance (wraps cl_kernel)
    struct kinstance {
      config *parent;
      cl_kernel kernel;
      std::string name;

      kinstance(config *_parent, cl_kernel _kernel);

      template <typename T> void set_arg_uniform(cl_uint arg_ix, T t);
      template <typename T> void set_arg_mem(cl_uint arg_ix, buffer<T> &b);
      void set_arg_slm(cl_uint arg_ix, size_t slm_len);

      // dispatch a kernel (clEnqueueNDRangeKernel)
      kdispatch dispatch(
        ndr gsz,
        ndr lsz = ndr(),
        cl_event *evt = nullptr);
    };

    kinstance &get_kernel(const std::string &kernel_name);
    bool has_kernel(const std::string &kernel_name) const;
    const std::vector<kinstance> &get_kernels() const;

    void syncAll() {CL_COMMAND(clFinish, queue);}

    // users can override this for different behavior
    virtual void contextCallback(
      const char * /* errinfo */,
      const void * /* private_info */,
      size_t /* cb */) { }

  private:
    cl_device_id                                   device_id;
    std::string                                    file_name;
    cl_context                                     context;
    cl_command_queue                               queue;
    bool                                           is_profiling_queue;
    cl_program                                     program;
    std::vector<kinstance>                         kernels;
    std::vector<buffer_base *>                      buffers;

    struct Source {
      enum class Kind {
        OPENCL_C = 1,
        SPIRV,
        BINARY
      } kind;
      std::vector<char> bits;
    };

    static Source load_bits(std::string file_name, Source::Kind source_kind);
    static Source load_source(std::string _file_name);

    void construct(
      cl_device_id _dev_id,
      std::string _file_name,
      const Source &_source,
      std::string _build_options,
      cl_command_queue_properties qprops);

    void construct_with_queue_and_context(
      cl_device_id _dev_id,
      std::string _file_name,
      const Source &_source,
      std::string _build_options);
  }; // class config

  static void contextCallbackDispatcher(
    const char *errinfo, const void *private_info, size_t cb, void *env)
  {
    ((config *)env)->contextCallback(errinfo, private_info, cb);
  }


  /////////////////////////////////////////////////////////////////////////////
  // OpenCL C Type Generic Function Support
  //
  // Adds basic arithmetic support for OpenCL C Vector Types and gives
  // similar functions to them as scalars.  Below, T means all types
  // (both vectors and scalars), V means vectors, E means scalars.
  // For example, float4 is a vector type with scalar component of float and
  // 4 vectorElement()
  //
  // The following are some helpful type-level functions for OpenCL types
  //   - typeNameToString<cl_uint>() ==> "uint"
  //   - vectorElements<cl_uint4>() ==> 4
  //   - elementTypeNameToString<cl_float4>() ==> "float4"
  //   - broadcast<cl_int4,cl_int>(2) ==> (int2)2
  //   - dot(T,T) ==> dot product (both vectors and scalars supported)
  //   - format_t(std::ostream&, T) ==> formats an element
  //         signed are decimal and unsigned are uppercase hex fixed to
  //         data type width
  //   - operators for vectors: ==, !=,
  //
  // TODO: need a type-level function to extract the inner type of a vector type
  //    cl_int4 -> cl_int
  // Would be better to have the template return a constant for vectorElements
  template <typename T> static constexpr const char *typeNameToString();
  template <typename T> static constexpr const char *elementTypeNameToString();
//  template <typename T>
//  static std::string format_t(std::ostream &os, const T &t, int prec);
  template <typename V>
  static void format_t(std::ostream &os, const V &v, int prec = -1) {
    os << "(";
    for (int i = 0; i < vectorElements<V>(); i++) {
      if (i > 0)
        os << ",";
      mincl::format_t(os, v.s[i], prec);
    }
    os << ")";
  }
  // relation between vector and element ypes
  template <typename T>
  struct cl_type_info {
    // using vtype = cl_float4;
    // using etype = cl_float;
    // ...
  };

  template <typename T>
  static std::string format_t(const T &t, int prec = -1) {
    std::stringstream ss;
    format_t<T>(ss, t, prec);
    return ss.str();
  }

  template <typename V> static constexpr int vectorElements();
  // needs to be template so that scalar case can distinguish itself
  // from vector (can't overload on return type only)
  template <typename V, typename E>
  static constexpr V broadcast(E e) {
    V v;
    for (int i = 0; i < vectorElements<V>(); i++)
      v.s[i] = e;
    return v;
  }

  template <typename V, typename E>
  static V vecCompl(const V &v) {
    V c;
    for (int i = 0; i < vectorElements<V>(); i++)
      c.s[i] = ~v.s[i];
    return c;
  }
  template <typename V, typename E>
  static V vecAnd(const V &v1, const V &v2) {
    V c;
    for (int i = 0; i < vectorElements<V>(); i++)
      c.s[i] = v1.s[i] & v2.s[i];
    return c;
  }
  template <typename V, typename E>
  static V vecXor(const V &v1, const V &v2) {
    V c;
    for (int i = 0; i < vectorElements<V>(); i++)
      c.s[i] = v1.s[i] ^ v2.s[i];
    return c;
  }
  template <typename V, typename E>
  static V vecOr(const V &v1, const V &v2) {
    V c;
    for (int i = 0; i < vectorElements<V>(); i++)
      c.s[i] = v1.s[i] | v2.s[i];
    return c;
  }
  template <typename V, typename E, typename IV>
  static V vecShl(const V &v, const IV &n) {
    static_assert(vectorElements<V>() == vectorElements<IV>());
    V c;
    for (int i = 0; i < vectorElements<V>(); i++)
      c.s[i] = v.s[i] << n.s[i];
    return c;
  }
  template <typename V, typename E, typename IV>
  static V vecShr(const V &v, const IV &n) {
    static_assert(vectorElements<V>() == vectorElements<IV>());
    V c;
    for (int i = 0; i < vectorElements<V>(); i++)
      c.s[i] = v.s[i] >> n.s[i];
    return c;
  }
  //
  ////////////////////////////////////////////////////////////////////////
  // We define these as templates and then define operators as overloads.
  // This gives the debugger a breakpoint outside the macro hell.
  //
  template <typename V, typename E>
  static V &vecAssign(V &eq, const V &rhs) {
    std::copy(rhs.s, rhs.s + vectorElements<V>(), eq.s);
    return eq;
  }
  //
  template <typename V, typename E>
  static bool vecEq(const V &v1, const V &v2) {
    return memcmp(&v1,&v2,sizeof(v2)) == 0;
  }
  template <typename V, typename E>
  static bool vecNe(const V &v1, const V &v2) {
    return !vecEq<V,E>(v1,v2);
  }
  template <typename V, typename E>
  static bool vecLt(const V &v1, const V &v2) {
    for (int i = vectorElements<V>() - 1; i >= 0; i--)
      if (!(v1.s[i] < v2.s[i])) return false;
    return true;
  }
  template <typename V, typename E>
  static bool vecLe(const V &v1, const V &v2) {
    for (int i = vectorElements<V>() - 1; i >= 0; i--)
      if (!(v1.s[i] <= v2.s[i])) return false;
    return true;
  }
  template <typename V, typename E>
  static bool vecGt(const V &v1, const V &v2) {
    for (int i = vectorElements<V>() - 1; i >= 0; i--)
      if (!(v1.s[i] > v2.s[i])) return false;
    return true;
  }
  template <typename V, typename E>
  static bool vecGe(const V &v1, const V &v2) {
    for (int i = vectorElements<V>() - 1; i >= 0; i--)
      if (!(v1.s[i] >= v2.s[i])) return false;
    return true;
  }
  //
  template <typename V, typename E>
  static V vecAdd(const V &v1, const V &v2) {
    V sum {};
    for (int i = 0; i < vectorElements<V>(); i++)
      sum.s[i] = v1.s[i] + v2.s[i];
    return sum;
  }
  template <typename V, typename E>
  static V vecSub(const V &v1, const V &v2) {
    V diff {};
    for (int i = 0; i < vectorElements<V>(); i++)
      diff.s[i] = v1.s[i] - v2.s[i];
    return diff;
  }
  template <typename V, typename E>
  static V vecMul(const V &v1, const V &v2) {
    V prod {};
    for (int i = 0; i < vectorElements<V>(); i++)
      prod.s[i] = v1.s[i] * v2.s[i];
    return prod;
  }
  template <typename V, typename E>
  static V vecDiv(const V &v1, const V &v2) {
    V qot {};
    for (int i = 0; i < vectorElements<V>(); i++)
      qot.s[i] = v1.s[i] / v2.s[i];
    return qot;
  }
  template <typename V, typename E>
  static V vecMod(const V &v1, const V &v2) {
    V rem {};
    for (int i = 0; i < vectorElements<V>(); i++)
      rem.s[i] = v1.s[i] % v2.s[i];
    return rem;
  }
  template <typename V, typename E>
  static V vecNegate(const V &v) {
    V vn {};
    for (int i = 0; i < vectorElements<V>(); i++)
      vn.s[i] = -v.s[i];
    return vn;
  }
  template <typename V, typename E>
  static V vecIabs(const V &v) {
    V va {};
    for (int i = 0; i < vectorElements<V>(); i++)
      // no std::abs for short and char (fails overload resolution)
      // so manually expand
      va.s[i] = v.s[i] < 0 ? -v.s[i] : v.s[i];
    return va;
  }
  template <typename V, typename E>
  static V vecFabs(const V &v) {
    V va {};
    for (int i = 0; i < vectorElements<V>(); i++)
      va.s[i] = std::fabs(v.s[i]);
    return va;
  }
  template <typename V, typename E>
  static E vecDot(const V &v1, const V &v2) {
    E sum = 0;
    for (int i = 0; i < vectorElements<V>(); i++)
      sum += v1.s[i] * v2.s[i];
    return sum;
  }
  // traditional names (use directly)
  template <typename V, typename E>
  static E max(const V &v) {
    E mx = v.s[0];
    for (int i = 1; i < vectorElements<V>(); i++)
      mx = std::max<E>(mx,v.s[i]);
    return mx;
  }
  template <typename V, typename E>
  static E min(const V &v) {
    E mn = v.s[0];
    for (int i = 1; i < vectorElements<V>(); i++)
      mn = std::min<E>(mn,v.s[i]);
    return mn;
  }
  template <typename V, typename E>
  static E sum(const V &v) {
    E sum = v.s[0];
    for (int i = 1; i < vectorElements<V>(); i++)
      sum += v.s[i];
    return sum;
  }
  //
  template <typename V, typename E>
  static V apply(const V &v, std::function<E (const E &)> func)
  {
    V r;
    for (int i = 0; i < vectorElements<V>(); i++)
      r.s[i] = apply(v.s[i]);
    return r;
  }
  template <typename V, typename E>
  static V apply(const V &v1, const V &v2,
    std::function<E (const E &e1,const E &e2)> func)
  {
    V r;
    for (int i = 0; i < vectorElements<V>(); i++)
      r.s[i] = apply(v1.s[i], v2.s[i]);
    return r;
  }


// TODO: can we do a conversion operator?
// (seems like not)
//  static V operator(const E &e) {
//    return broadcast<V,E>(e);
//  }

#define MAKE_VECTOR_FUNCTIONS_COMMON(V,E)\
  static std::ostream &operator<<(std::ostream &os, const V &v) {\
    mincl::format_t(os, v);\
    return os;\
  }\
  static bool operator==(const V &v1, const V &v2) {\
    return vecEq<V,E>(v1,v2);\
  }\
  static bool operator!=(const V &v1, const V &v2) {\
    return vecNe<V,E>(v1,v2);\
  }\
  static bool operator<(const V &v1, const V &v2) {\
    return vecLt<V,E>(v1,v2);\
  }\
  static bool operator<=(const V &v1, const V &v2) {\
    return vecLe<V,E>(v1,v2);\
  }\
  static bool operator>(const V &v1, const V &v2) {\
    return vecGt<V,E>(v1,v2);\
  }\
  static bool operator>=(const V &v1, const V &v2) {\
    return vecGe<V,E>(v1,v2);\
  }\
  static V operator+(const V &v1, const V &v2) {\
    return vecAdd<V,E>(v1, v2);\
  }\
  static V operator+(const V &v, const E &e) {\
    return v + broadcast<V,E>(e);\
  }\
  static V operator+(const E &e,const V &v) {\
    return v + e;\
  }\
  static V operator-(const V &v1, const V &v2) {\
    return vecSub<V,E>(v1, v2);\
  }\
  static V operator-(const V &v, const E &e) {\
    return v - broadcast<V,E>(e);\
  }\
  static V operator-(const E &e, const V &v) {\
    return broadcast<V,E>(e) - v;\
  }\
  static V operator*(const V &v1, const V &v2) {\
    return vecMul<V,E>(v1,v2);\
  }\
  static V operator*(const E &e,const V &v) {\
    return broadcast<V,E>(e)*v;\
  }\
  static V operator*(const V &v, const E &e) {\
    return broadcast<V,E>(e)*v;\
  }\
  static V operator/(const V &v1, const V &v2) {\
    return vecDiv<V,E>(v1,v2);\
  }\
  static V &operator+=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1 + v2);\
  }\
  static V &operator+=(V &v, const E &e) {\
    return v += broadcast<V,E>(e);\
  }\
  static V &operator-=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1 - v2);\
  }\
  static V &operator-=(V &v, const E &e) {\
    return v -= broadcast<V,E>(e);\
  }\
  static V &operator*=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1*v2);\
  }\
  static V &operator*=(V &v, const E &e) {\
    return v *= broadcast<V,E>(e);\
  }\
  static V &operator/=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1 / v2);\
  }\
  static V &operator/=(V &v, const E &e) {\
    return v /= broadcast<V,E>(e);\
  }\
  static V operator++(V &v) {\
    return v += 1;\
  }\
  static V operator++(V &v,int) {\
    auto t = v; v += 1; return t;\
  }\
  static V operator--(V &v) {\
    return v -= 1;\
  }\
  static V operator--(V &v,int) {\
    auto t = v; v -= 1; return t;\
  }

#define MAKE_VECTOR_FUNCTIONS_INT_COMMON(V,E,IV)\
  static V operator%(const V &v1, const V &v2) {\
    return vecMod<V,E>(v1,v2);\
  }\
  static V operator%(const V &v, const E &e) {\
    return v % broadcast<V,E>(e);\
  }\
  static V operator%(const E &e,const V &v) {\
    return broadcast<V,E>(e) % v;\
  }\
  static V operator&(const V &v1, const V &v2) {\
    return vecAnd<V,E>(v1,v2);\
  }\
  static V operator&(const V &v, const E &e) {\
    return v & broadcast<V,E>(e);\
  }\
  static V operator&(const E &e, const V &v) {\
    return broadcast<V,E>(e) & v;\
  }\
  static V operator^(const V &v1, const V &v2) {\
    return vecXor<V,E>(v1,v2);\
  }\
  static V operator^(const V &v, const E &e) {\
    return v ^ broadcast<V,E>(e);\
  }\
  static V operator^(const E &e, const V &v) {\
    return broadcast<V,E>(e) ^ v;\
  }\
  static V operator|(const V &v1, const V &v2) {\
    return vecOr<V,E>(v1,v2);\
  }\
  static V operator|(const V &v, const E &e) {\
    return v | broadcast<V,E>(e);\
  }\
  static V operator|(const E &e, const V &v) {\
    return broadcast<V,E>(e) | v;\
  }\
  static V operator<<(const V &v, const IV &n) {\
    return vecShl<V,E>(v,n);\
  }\
  static V operator<<(const V &v, cl_int n) {\
    return v << broadcast<IV,cl_int>(n);\
  }\
  static V operator>>(const V &v, const IV &n) {\
    return vecShr<V,E>(v,n);\
  }\
  static V operator>>(const V &v, cl_int n) {\
    return v >> broadcast<IV,cl_int>(n);\
  }\
  static V operator~(const V &v) {\
    return vecCompl<V,E>(v);\
  }\
  static V &operator%=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1 % v2);\
  }\
  static V &operator%=(V &v, const E &e) {\
    return v %= broadcast<V,E>(e);\
  }\
  static V &operator&=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1 & v2);\
  }\
  static V &operator&=(V &v, const E &e) {\
    return v &= broadcast<V,E>(e);\
  }\
  static V &operator^=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1 ^ v2);\
  }\
  static V &operator^=(V &v, const E &e) {\
    return v ^= broadcast<V,E>(e);\
  }\
  static V &operator|=(V &v1, const V &v2) {\
    return vecAssign<V,E>(v1, v1 | v2);\
  }\
  static V &operator|=(V &v, const E &e) {\
    return v |= broadcast<V,E>(e);\
  }\
  static V &operator<<=(V &v1, const IV &v2) {\
    return vecAssign<V,E>(v1, v1 << v2);\
  }\
  static V &operator<<=(V &v, cl_int i) {\
    return v <<= broadcast<IV,cl_int>(i);\
  }\
  static V &operator>>=(V &v1, const IV &v2) {\
    return vecAssign<V,E>(v1, v1 >> v2);\
  }\
  static V &operator>>=(V &v, cl_int i) {\
    return v >>= broadcast<IV,cl_int>(i);\
  }


#define MAKE_VECTOR_FUNCTIONS_SINT(V,E,IV)\
  MAKE_VECTOR_FUNCTIONS_COMMON(V,E)\
  MAKE_VECTOR_FUNCTIONS_INT_COMMON(V,E,IV)\
  static V operator-(const V &v) {\
    return vecNegate<V,E>(v);\
  }\
  static V abs(const V &v) {\
    return vecIabs<V,E>(v);\
  }\
  static E dot(const V &v1, const V &v2) {\
    return vecDot<V,E>(v1,v2);\
  }

// unsigned
//   - unary negation acts as signed (~ bits and add 1)
//   - abs is identity
#define MAKE_VECTOR_FUNCTIONS_UINT(V,E,IV)\
  MAKE_VECTOR_FUNCTIONS_COMMON(V,E)\
  MAKE_VECTOR_FUNCTIONS_INT_COMMON(V,E,IV)\
  static V operator-(const V &v) {\
    return vecCompl<V,E>(v) + broadcast<V,E>(1);\
  }\
  static V abs(const V &v) {\
    return v;\
  }

#define MAKE_VECTOR_FUNCTIONS_FLOATING(V,E)\
  MAKE_VECTOR_FUNCTIONS_COMMON(V,E)\
  static V operator-(const V &v) {\
    return vecNegate<V,E>(v);\
  }\
  static V abs(const V &v) {\
    return vecFabs<V,E>(v);\
  }

#define MAKE_VECTOR_TYPE_SUPPORT_COMMON(T,N)\
  template <> static constexpr const char *typeNameToString<cl_ ## T ## N>() {return #T ## #N;}\
  template <> static constexpr const char *elementTypeNameToString<cl_ ## T ## N>() {return #T;}\
  template <> static constexpr int vectorElements<cl_ ## T ## N>() {return N;}\
  template <> struct cl_type_info<cl_ ## T ## N> {\
    using vtype = typename cl_ ## T ## N;\
    using etype = typename cl_ ## T;\
    static constexpr unsigned elems() {return N;}\
    static constexpr const char *name() { return #T; }\
    static constexpr const char *cl_name() { return "cl_" #T; }\
    static constexpr etype elem(vtype v, unsigned i) {return v.s[i];}\
    static constexpr vtype bcast(etype e) {return broadcast<vtype,etype>(e);}\
  };

#define MAKE_VECTOR_TYPE_SUPPORT_SINT(T,N)\
  MAKE_VECTOR_TYPE_SUPPORT_COMMON(T,N)\
  MAKE_VECTOR_FUNCTIONS_SINT(cl_ ## T ## N,cl_ ## T, cl_int ## N)
#define MAKE_VECTOR_TYPE_SUPPORT_UINT(T,N)\
  MAKE_VECTOR_TYPE_SUPPORT_COMMON(T,N)\
  MAKE_VECTOR_FUNCTIONS_UINT(cl_ ## T ## N,cl_ ## T, cl_int ## N)
#define MAKE_VECTOR_TYPE_SUPPORT_FLOATING(T,N)\
  MAKE_VECTOR_TYPE_SUPPORT_COMMON(T,N)\
  MAKE_VECTOR_FUNCTIONS_FLOATING(cl_ ## T ## N,cl_ ## T)

// T==CL_T for "half" only
#define MAKE_TYPE_SUPPORT_COMMON_WITH_HOST_TYPE(T, CL_T, FMTR)\
  template <> static constexpr const char *typeNameToString<CL_T>() {return #T;}\
  template <> static constexpr const char *elementTypeNameToString<CL_T>() {return #T;}\
  template <> static constexpr int vectorElements<CL_T>() {return 1;}\
  template <> static constexpr CL_T broadcast<CL_T,CL_T>(CL_T t) {return t;}\
  static CL_T dot(const CL_T &e1, const CL_T &e2) {\
    return e1*e2;\
  }\
  template <> static void format_t(std::ostream &os, const CL_T &t, int prec) {\
    FMTR(os, t, prec);\
  }\
  /* identity functions (for "vector" of size 1) */\
  template <> static CL_T sum<CL_T,CL_T>(const CL_T &e) {return e;}\
  template <> static CL_T min<CL_T,CL_T>(const CL_T &e) {return e;}\
  template <> static CL_T max<CL_T,CL_T>(const CL_T &e) {return e;}\
  template <> struct cl_type_info<CL_T> {\
    using vtype = typename CL_T;\
    using etype = typename CL_T;\
    static constexpr unsigned elems() {return 1;}\
    static constexpr const char *name() { return #T; }\
    static constexpr const char *cl_name() { return #CL_T; }\
    static constexpr etype elem(vtype v, unsigned i) {return v;}\
    static constexpr vtype bcast(etype e) {return broadcast<vtype,etype>(e);}\
  };
#define MAKE_TYPE_SUPPORT_COMMON(T)\
  MAKE_TYPE_SUPPORT_COMMON_WITH_HOST_TYPE(T, cl_ ## T, default_format<cl_ ## T>)

template <typename T>
static void default_format(std::ostream &os, T t, int prec = -1) {
  std::stringstream ss;
  if (std::is_floating_point<T>::value) {
    if (prec >= 0)
      ss << std::fixed << std::setprecision(prec) << t;
    else
      ss << t;
  } else if (std::is_signed<T>::value) {
    if (sizeof(T) == 1)
      ss << std::dec << (int)t;
    else
      ss << std::dec << t;
  } else if (std::is_unsigned<T>::value) {
    ss << "0x";
    if (sizeof(T) == 1)
      ss << std::uppercase << std::setfill('0') << std::hex <<
        std::setw(2) << (unsigned)t;
    else
      ss << std::uppercase << std::setfill('0') << std::hex <<
        std::setw(2*sizeof(t)) << t;
  } else {
    ss << t;
  }
  os << ss.str();
}

// e.g. called with a token such as 'long'
#define MAKE_TYPE_SUPPORT_SINT(T)\
  MAKE_TYPE_SUPPORT_COMMON(T)\
  MAKE_VECTOR_TYPE_SUPPORT_SINT(T,2)\
  MAKE_VECTOR_TYPE_SUPPORT_SINT(T,4)\
  MAKE_VECTOR_TYPE_SUPPORT_SINT(T,8)\
  MAKE_VECTOR_TYPE_SUPPORT_SINT(T,16)

#define MAKE_TYPE_SUPPORT_UINT(T)\
  MAKE_TYPE_SUPPORT_COMMON(T)\
  MAKE_VECTOR_TYPE_SUPPORT_UINT(T,2)\
  MAKE_VECTOR_TYPE_SUPPORT_UINT(T,4)\
  MAKE_VECTOR_TYPE_SUPPORT_UINT(T,8)\
  MAKE_VECTOR_TYPE_SUPPORT_UINT(T,16)

#define MAKE_TYPE_SUPPORT_FLOATING(T)\
  MAKE_TYPE_SUPPORT_COMMON(T)\
  MAKE_VECTOR_TYPE_SUPPORT_FLOATING(T,2)\
  MAKE_VECTOR_TYPE_SUPPORT_FLOATING(T,4)\
  MAKE_VECTOR_TYPE_SUPPORT_FLOATING(T,8)\
  MAKE_VECTOR_TYPE_SUPPORT_FLOATING(T,16)

// TODO: refactor to clm.hpp
//
MAKE_TYPE_SUPPORT_UINT(uchar)
MAKE_TYPE_SUPPORT_UINT(ushort)
MAKE_TYPE_SUPPORT_UINT(uint)
MAKE_TYPE_SUPPORT_UINT(ulong)
//
MAKE_TYPE_SUPPORT_SINT(char)
MAKE_TYPE_SUPPORT_SINT(short)
MAKE_TYPE_SUPPORT_SINT(int)
MAKE_TYPE_SUPPORT_SINT(long)
//
MAKE_TYPE_SUPPORT_FLOATING(double)
MAKE_TYPE_SUPPORT_FLOATING(float)
// MAKE_TYPE_SUPPORT_FLOATING(half)
// have to expand this manually since we use cl_## all over the place
// cl_half expands to ushort, so we defined mincl::half to be distinct
// so we can use our wrapper class (otherwise cl_ushort == cl_half)
//
// template <> static const char *typeNameToString<half>() {return "half";}
// template <> static const char *elementTypeNameToString<half>() {return "half";}
// template <> static int vectorElements<half>() {return 1;}
// MAKE_VECTOR_FUNCTIONS_FLOATING(half,half)
// MAKE_VECTOR_TYPE_SUPPORT_FLOAT(half,2)\
// MAKE_VECTOR_TYPE_SUPPORT_FLOAT(half,4)\
// MAKE_VECTOR_TYPE_SUPPORT_FLOAT(half,8)\
// MAKE_VECTOR_TYPE_SUPPORT_FLOAT(half,16)
static void default_format_half(std::ostream &os, half t, int prec = -1) {
  default_format<cl_float>(os, (cl_float)t, prec);
}
MAKE_TYPE_SUPPORT_COMMON_WITH_HOST_TYPE(half, half, default_format_half)
#ifdef cl_half2
MAKE_VECTOR_TYPE_SUPPORT_COMMON(half, 2)
#endif

///////////////////////////////////////////////////////////////////////////////
#undef MAKE_VECTOR_FUNCTIONS_COMMON
#undef MAKE_VECTOR_FUNCTIONS_SINT
#undef MAKE_VECTOR_FUNCTIONS_UINT
#undef MAKE_VECTOR_FUNCTIONS_FLOATING
#undef MAKE_TYPE_SUPPORT_COMMON
#undef MAKE_TYPE_SUPPORT_SINT
#undef MAKE_TYPE_SUPPORT_UINT
#undef MAKE_TYPE_SUPPORT_FLOATING
#undef MAKE_VECTOR_TYPE_SUPPORT_COMMON
#undef MAKE_VECTOR_TYPE_SUPPORT_SINT
#undef MAKE_VECTOR_TYPE_SUPPORT_UINT
#undef MAKE_VECTOR_TYPE_SUPPORT_FLOAT

///////////////////////////////////////////////////////////////////////////////
static std::string get_device_info_string(cl_device_id d,
                                          cl_device_info param_name) {
  size_t slen = 0;
  CL_COMMAND(clGetDeviceInfo, d, param_name, 0, nullptr, &slen);
  auto_alloc<char> sbuf(slen + 1);
  CL_COMMAND(clGetDeviceInfo, d, param_name, slen, sbuf, nullptr);
  sbuf[slen] = 0;
  return std::string(sbuf);
}
  static std::string get_device_name(cl_device_id d) {
    return get_device_info_string(d, CL_DEVICE_NAME);
  }

  template <typename T>
  static T get_device_info_t(cl_device_id dev_id, cl_device_info param) {
    T x = 0;
    CL_COMMAND(clGetDeviceInfo, dev_id, param, sizeof(x), &x, nullptr);
    return x;
  }

  static cl_uint get_device_info_uint(cl_device_id dev_id, cl_device_info param)
  {
    return get_device_info_t<cl_uint>(dev_id, param);
  }
  static cl_ulong get_device_info_ulong(cl_device_id dev_id,
                                        cl_device_info param) {
    return get_device_info_t<cl_ulong>(dev_id, param);
  }

  // multiply by 1000*1000 to get cycles/second
  static cl_uint get_device_frequency_mhz(cl_device_id dev_id)
  {
    return get_device_info_uint(dev_id, CL_DEVICE_MAX_CLOCK_FREQUENCY);
  }

  static bool is_intel_gpu(cl_device_id dev_id)
  {
    return get_device_info_uint(dev_id, CL_DEVICE_VENDOR_ID) == 0x8086 &&
           get_device_info_t<cl_device_type>(dev_id, CL_DEVICE_TYPE) ==
               CL_DEVICE_TYPE_GPU;
  }

  static bool is_nvidia_gpu(cl_device_id dev_id) {
    // or CL_DEVICE_VENDOR = NVidia Corporation
    return get_device_info_uint(dev_id, CL_DEVICE_VENDOR_ID) == 0x10DE &&
           get_device_info_t<cl_device_type>(dev_id, CL_DEVICE_TYPE) ==
               CL_DEVICE_TYPE_GPU;
  }
  static int get_device_alu_count(cl_device_id dev_id) {
    auto dev_name = get_device_name(dev_id);
    auto matches = [&](const char *ss) {
      return dev_name.find(ss) != std::string::npos;
    };
    auto matchesAny = [&](std::initializer_list<const char *> es) {
      for (const char *e : es)
        if (matches(e))
          return true;
      return false;
    };
    //
    cl_uint cus = 0;
    CL_COMMAND(clGetDeviceInfo, dev_id, CL_DEVICE_MAX_COMPUTE_UNITS,
               sizeof(cus), &cus, nullptr);
    // https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units
    if (is_intel_gpu(dev_id)) {
      if (matchesAny({"A310", "A380", "A580", "A750", "A770", "A30M", "A40",
                     "A50", "A60"})) {
        return 8 * cus; // 1xSIMD8
      } else if (matches("Data Center GPU Max")) {
        return 16 * cus; // 1xSIMD16 per EU
      } else if (matches("HD Graphics")) {
        // https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units
        if (matchesAny({
                        // GEN12
                        " 710", " 730", " 740", " 750", " 770",
                        //
                        " 600", " 605", " 620", " 630", " 640", " 655", " 650",
                        " Gen9",
                        //
                        " 520", " 530",
                        //
                        " 4200", " 4400", " 4600", " 4700", " 5000", " 5100",
                        " 5200", " 5300", " 5500", " 5600", " 5700", " 6000",
                        " 6100", " 6200", " 6300", " Gen8"})) {
          return 8 * cus; // 2xSIMD4's per EU; GEN12 is 1xSIMD8
        } else {
          return 0;
        }
      } else {
        // unknown Intel
        return 0;
      }
    } else if (is_nvidia_gpu(dev_id)) {
      // FIXME: the Ti series have more ALUs...

      /////////////////////////////////////////////////////
      // NVIDIA:
      // https://en.wikipedia.org/wiki/List_of_Nvidia_graphics_processing_units
      if (matches("GTX 10") || matches("GTX 9")) {
        // https://en.wikipedia.org/wiki/GeForce_10_series
        // https://en.wikipedia.org/wiki/GeForce_900_series
        // 4 blocks (SIMD32) per SM
        return 4 * 32 * cus;
      } else if (matches("RTX 20") || matches("GTX 16")) {
        // https://en.wikipedia.org/wiki/GeForce_20_series
        // https://en.wikipedia.org/wiki/GeForce_16_series
        // 2 blocks (SIMD32) per SM
        return 2 * 32 * cus;
      } else if (matches("RTX 30")) {
        // [1] https://en.wikipedia.org/wiki/GeForce_30_series
        // [2] https://www.anandtech.com/show/16135/nvidia-delays-geforce-rtx-3070-launch-to-october-29th
        // e.g. for 3070, we get 46 SMs; 2 reports 5888 CUDA cores, so 4 blocks
        // (SIMD32) per SM
        return 4 * 32 * cus;
      // } else if (matches("RTX 40")) {
        // https://en.wikipedia.org/wiki/GeForce_40_series
        // return ???;
      } else {
        return 0;
      }
    } else {
      // unsupported vendor (need AMD strings!)
      // https://en.wikipedia.org/wiki/List_of_AMD_graphics_processing_units
      return 0;
    }
  }

  static std::vector<std::pair<cl_device_id, std::string>>
  all_devices_with_names()
  {
    std::vector<std::pair<cl_device_id,std::string>> all_ds;

    cl_uint num_ps = 0;
    CL_COMMAND(clGetPlatformIDs, 0, nullptr, &num_ps);
    cl_platform_id *ps = new cl_platform_id[num_ps];
    CL_COMMAND(clGetPlatformIDs, num_ps, ps, nullptr);
    for (cl_uint p_ix = 0; p_ix < num_ps; p_ix++) {
      cl_uint num_ds = 0;
      CL_COMMAND(clGetDeviceIDs,
        ps[p_ix], CL_DEVICE_TYPE_ALL, 0, nullptr, &num_ds);
      cl_device_id *ds = new cl_device_id[num_ds];
      CL_COMMAND(clGetDeviceIDs,
        ps[p_ix], CL_DEVICE_TYPE_ALL, num_ds, ds, nullptr);
      for (cl_uint d_ix = 0; d_ix < num_ds; d_ix++) {
        all_ds.emplace_back(ds[d_ix], get_device_name(ds[d_ix]));
      }
      delete[] ds;
    }
    delete[] ps;

    return all_ds;
  }

  static void list_devices()
  {
    int ix = 0;
    std::cout << "DEVICES:\n";
    for (const auto &p : all_devices_with_names()) {
      std::cout << "  #" << ix++ << "   " << p.second << "\n";
    }
  }

  static uint64_t parse_uint64(
    const std::string &s,
    const char *what = "integer",
    bool allow_suffix = false)
  {
    try {
      if (s.size() >= 1 && s[0] == '-')
        fatal(s,": must be positive ", what);

      int radix = 10;
      const char *str = s.c_str();
      if (s.size() > 2 && s[0] == '0' && (s[1] == 'x' || s[1] == 'X')) {
        radix = 16;
        str += 2;
      }
      char *end = nullptr;
      uint64_t i = std::strtoull(str, &end, radix);

      while (*end == ' ')
        end++;
      if (allow_suffix && *end) {
        if (*end == 'K' || *end == 'k')
          i *= 1024;
        else if (*end == 'M' || *end == 'm')
          i *= 1024*1024;
        else if (*end == 'G' || *end == 'g')
          i *= 1024*1024*1024;
        else
          fatal(s, ": malformed ", what, " suffix");
        end++;
      }
      if (*end)
        fatal(s,": malformed ", what);
      return i;
    } catch (...) {
      fatal(s,": malformed ", what);
      return -1;
    }
  }
  static int64_t parse_int64(
    const std::string &s,
    const char *what = "integer",
    bool allow_suffix = false)
  {
    try {
      int radix = 10;
      const char *str = s.c_str();

      int64_t sign = 1;
      if (s.size() > 0 && s[0] == '-') {
        str++;
        sign = -1;
      }

      if (strlen(str) > 2 && str[0] == '0' && (str[1] == 'x' || str[1] == 'X')) {
        radix = 16;
        str += 2;
      }
      char *end = nullptr;
      int64_t i = std::strtoll(str, &end, radix);

      while (*end == ' ')
        end++;
      if (allow_suffix && *end) {
        if (*end == 'K' || *end == 'k')
          i *= 1024;
        else if (*end == 'M' || *end == 'm')
          i *= 1024*1024;
        else if (*end == 'G' || *end == 'g')
          i *= 1024*1024*1024;
        else
          fatal(s, ": malformed ", what, " suffix");
        end++;
      }
      if (*end)
        fatal(s,": malformed ", what);

      return i * sign;
    } catch (...) {
      fatal(s,": malformed ", what);
      return -1;
    }
  }

  static cl_device_id find_device(std::string dev_str) {
    int target_dev_ix = -1;
    try {
      char *end = nullptr;
      target_dev_ix = (int)std::strtol(dev_str.c_str(), &end, 10);
      if (*end)
        target_dev_ix = -1;
    } catch (...) {
      // match by name
    }

    cl_device_id picked_device = nullptr;
    std::string picked_device_name;

    auto all_ds = all_devices_with_names();
    if (target_dev_ix >= 0) {
      // find by index
      if (target_dev_ix >= (int)all_ds.size()) {
        fatal("device index is out of bounds");
      }
      picked_device = all_ds[target_dev_ix].first;
      picked_device_name = all_ds[target_dev_ix].second;
    } else {
      // find by name
      std::stringstream all_matching_devices;
      int matched = 0;
      for (const auto &d : all_ds) {
        if (d.second.find(dev_str) != std::string::npos) {
          picked_device = d.first;
          picked_device_name = d.second;
          all_matching_devices << " * " << d.second << "\n";
          matched++;
        }
      }
      if (matched == 0) {
        fatal(dev_str, ": unable to match device string");
      } else if (matched > 1) {
        fatal(dev_str, ": is ambiguous amongst:\n", all_matching_devices.str());
      }
    }
    // std::cout << "DEVICE: " << picked_device_name << "\n";
    return picked_device;
  }


  static const uint32_t F32_SIGN_BIT  = 0x80000000;
  static const uint32_t F32_EXP_MASK  = 0x7F800000;
  static const uint32_t F32_MANT_MASK = 0x007FFFFF;
  static const uint32_t F32_QNAN_BIT  = 0x00400000;
  static const int F32_MANTISSA_BITS = 23;
  static const uint16_t F16_SIGN_BIT  = 0x8000;
  static const uint16_t F16_EXP_MASK  = 0x7C00;
  static const uint16_t F16_MANT_MASK = 0x03FF;
  static const uint16_t F16_QNAN_BIT  = 0x0200;
  static const int F16_MANTISSA_BITS = 10;


  static constexpr uint32_t float_to_bits(float f) {
    union {
      float f;
      uint32_t i;
    } u;
    u.f = f;
    return u.i;
  }

  static constexpr float float_from_bits(uint32_t f) {
    union {
      float f;
      uint32_t i;
    } u;
    u.i = f;
    return u.f;
  }

  static constexpr float half_bits_to_float(uint16_t h)
  {
    uint16_t u16 = h;
    constexpr int MANTISSA_DIFFERENCE = // 23 - 10
       F32_MANTISSA_BITS - F16_MANTISSA_BITS;
    const int F32_F16_BIAS_DIFFERENCE = 127 - 15;

    uint32_t s32 = ((uint32_t)u16 & F16_SIGN_BIT) << 16;
    uint32_t e16 = (u16 & F16_EXP_MASK) >> F16_MANTISSA_BITS;
    uint32_t m16 = u16 & F16_MANT_MASK;

    uint32_t m32, e32;
    if (e16 != 0 && e16 < (F16_EXP_MASK >> F16_MANTISSA_BITS)) { // e16 < 0x1F
      //  normal number
      e32 = e16 + F32_F16_BIAS_DIFFERENCE;
      m32 = m16 << MANTISSA_DIFFERENCE;
    } else if (e16 == 0 && m16 != 0) {
      // denorm/subnorm number (e16 == 0)
      // shift the mantissa left until the hidden one gets set
      for (e32 = (F32_F16_BIAS_DIFFERENCE + 1);
          (m16 & (F16_MANT_MASK + 1)) == 0;
          m16 <<= 1, e32--)
          ;
      m32 = (m16 << MANTISSA_DIFFERENCE) & F32_MANT_MASK;
    } else if (e16 == 0) { // +/- 0.0
      e32 = 0;
      m32 = 0;
    } else {
      e32 = F32_EXP_MASK >> F32_MANTISSA_BITS;
      if (m16 == 0) { // Infinity
        m32 = 0;
      } else { // NaN:  m16 != 0 && e16 == 0x1F
        m32 = (u16 & F16_QNAN_BIT) << MANTISSA_DIFFERENCE; // preserve sNaN bit
        m32 |= (F16_MANT_MASK >> 1) & m16;
        if (m32 == 0) {
            m32 = 1; // ensure still NaN
        }
      }
    }
    return float_from_bits(s32 | (e32 << F32_MANTISSA_BITS) | m32);
  }

  constexpr uint16_t float_to_half_bits(float f)
  {
    uint32_t f32 = float_to_bits(f);

    uint32_t m32 = F32_MANT_MASK & f32;
    uint32_t e32 = (F32_EXP_MASK & f32) >> F32_MANTISSA_BITS;

    uint32_t m16;
    uint32_t e16;

    if (e32 == (F32_EXP_MASK >> F32_MANTISSA_BITS)) {
      // NaN or Infinity
      e16 = F16_EXP_MASK;
      m16 = (F16_MANT_MASK >> 1) & f32;
      if (m32 != 0) {
        // preserve the bottom 9 bits of the NaN payload and
        // shift the signaling bit (high bit) down as bit 10
        m16 |= (F32_QNAN_BIT & f32) >>
            (F32_MANTISSA_BITS - F16_MANTISSA_BITS);
        // s eeeeeeee mmmmmmmmmmmmmmmmmmmmmm
        //            |            |||||||||
        //            |            vvvvvvvvv
        //            +---------->mmmmmmmmmm
        if (m16 == 0) {
            // if the nonzero payload is in the high bits and and gets
            // dropped and the signal bit is non-zero, then m16 is 0,
            // to maintain it as a qnan, we must set at least one bit
            m16 = 0x1;
        }
      }
    } else if (e32 > (127 - 15) + 0x1E) { // e16 overflows 5 bits after bias fix
      // Too large for f16 => infinity
      e16 = F16_EXP_MASK;
      m16 = 0;
    } else if (e32 <= (127 - 15) && e32 >= 0x66) {
      // Denorm/subnorm float
      //
      // Normal floats are:
      //   (1 + sum{m[i]^(23-i)*2^(-i)}) * 2^(e - bias)
      //   (each mantissa bit is a fractional power of 2)
      // Denorms are:
      //   (0 + ...)
      // This is a zero exponent, but non-zero mantissa
      //
      // set leading bit past leading mantissa bit (low exponent bit)
      // (hidden one)
      m32 |= (F32_QNAN_BIT << 1);
      // exponent
      // repeatedly increment the f32 exponent and divide the denorm
      // mantissa until the exponent reachs a non-zero value
      for (; e32 <= 127 - 15; m32 >>= 1, e32++)
          ;
      e16 = 0;
      m16 = m32 >> (F32_MANTISSA_BITS - F16_MANTISSA_BITS);
    } else if (e32 < 0x66) {
      // Too small: rounds to +/-0.0
      e16 = 0;
      m16 = 0;
    } else {
      // Normalized float
      e16 = (e32 - (127 - 15)) << F16_MANTISSA_BITS;
      m16 = m32 >> (F32_MANTISSA_BITS - F16_MANTISSA_BITS);
      // TODO: rounding modes?
      // if (m32 & 0x1000)
      //   h16++;
      // c.f. https://gist.github.com/rygorous/2156668
      // if (((m32 & 0x1fff) > 0x1000) || (m16 & 1)) // above halfway point or unrounded result is odd
      //   h16++;
    }

    uint32_t s16 = (f32 >> 16) & F16_SIGN_BIT;
    uint16_t h {(uint16_t)(s16 | e16 | m16)};

    return h;
  }

  inline constexpr bool half::is_nan() const {
    return ((bits & F16_EXP_MASK) == F16_EXP_MASK) &&
      (bits & F16_MANT_MASK) != 0;
  }

  static std::string status_to_symbol(cl_int error)
  {
#define CASE(X) case X: return #X
    switch (error) {
    CASE(CL_SUCCESS);
    CASE(CL_DEVICE_NOT_FOUND);
    CASE(CL_DEVICE_NOT_AVAILABLE);
    CASE(CL_COMPILER_NOT_AVAILABLE);
    CASE(CL_MEM_OBJECT_ALLOCATION_FAILURE);
    CASE(CL_OUT_OF_RESOURCES);
    CASE(CL_OUT_OF_HOST_MEMORY);
    CASE(CL_PROFILING_INFO_NOT_AVAILABLE);
    CASE(CL_MEM_COPY_OVERLAP);
    CASE(CL_IMAGE_FORMAT_MISMATCH);
    CASE(CL_IMAGE_FORMAT_NOT_SUPPORTED);
    CASE(CL_BUILD_PROGRAM_FAILURE);
    CASE(CL_MAP_FAILURE);
    CASE(CL_MISALIGNED_SUB_BUFFER_OFFSET);
    CASE(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST);
    CASE(CL_COMPILE_PROGRAM_FAILURE);
    CASE(CL_LINKER_NOT_AVAILABLE);
    CASE(CL_LINK_PROGRAM_FAILURE);
    CASE(CL_DEVICE_PARTITION_FAILED);
    CASE(CL_KERNEL_ARG_INFO_NOT_AVAILABLE);
    CASE(CL_INVALID_VALUE);
    CASE(CL_INVALID_DEVICE_TYPE);
    CASE(CL_INVALID_PLATFORM);
    CASE(CL_INVALID_DEVICE);
    CASE(CL_INVALID_CONTEXT);
    CASE(CL_INVALID_QUEUE_PROPERTIES);
    CASE(CL_INVALID_COMMAND_QUEUE);
    CASE(CL_INVALID_HOST_PTR);
    CASE(CL_INVALID_MEM_OBJECT);
    CASE(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
    CASE(CL_INVALID_IMAGE_SIZE);
    CASE(CL_INVALID_SAMPLER);
    CASE(CL_INVALID_BINARY);
    CASE(CL_INVALID_BUILD_OPTIONS);
    CASE(CL_INVALID_PROGRAM);
    CASE(CL_INVALID_PROGRAM_EXECUTABLE);
    CASE(CL_INVALID_KERNEL_NAME);
    CASE(CL_INVALID_KERNEL_DEFINITION);
    CASE(CL_INVALID_KERNEL);
    CASE(CL_INVALID_ARG_INDEX);
    CASE(CL_INVALID_ARG_VALUE);
    CASE(CL_INVALID_ARG_SIZE);
    CASE(CL_INVALID_KERNEL_ARGS);
    CASE(CL_INVALID_WORK_DIMENSION);
    CASE(CL_INVALID_WORK_GROUP_SIZE);
    CASE(CL_INVALID_WORK_ITEM_SIZE);
    CASE(CL_INVALID_GLOBAL_OFFSET);
    CASE(CL_INVALID_EVENT_WAIT_LIST);
    CASE(CL_INVALID_EVENT);
    CASE(CL_INVALID_OPERATION);
    CASE(CL_INVALID_GL_OBJECT);
    CASE(CL_INVALID_BUFFER_SIZE);
    CASE(CL_INVALID_MIP_LEVEL);
    CASE(CL_INVALID_GLOBAL_WORK_SIZE);
    CASE(CL_INVALID_PROPERTY);
    CASE(CL_INVALID_IMAGE_DESCRIPTOR);
    CASE(CL_INVALID_COMPILER_OPTIONS);
    CASE(CL_INVALID_LINKER_OPTIONS);
    CASE(CL_INVALID_DEVICE_PARTITION_COUNT);
    CASE(CL_INVALID_PIPE_SIZE);
    CASE(CL_INVALID_DEVICE_QUEUE);
    CASE(CL_INVALID_SPEC_ID);
    CASE(CL_MAX_SIZE_RESTRICTION_EXCEEDED);
    // from extensions
    // CASE(CL_INVALID_ACCELERATOR_INTEL);
    // CASE(CL_INVALID_ACCELERATOR_TYPE_INTEL);
    // CASE(CL_INVALID_ACCELERATOR_DESCRIPTOR_INTEL);
    // CASE(CL_ACCELERATOR_TYPE_NOT_SUPPORTED_INTEL);
    default:
      std::stringstream ss;
      ss << error << "?";
      return ss.str();
#undef CASE
    }
  } // status_to_symbol


  static inline void build_program(
    cl_program program,
    cl_device_id device_id,
    const char *build_options = "",
    const char *file_name = nullptr)
  {
    const char *maybe_build_opts =
      build_options && *build_options ? build_options : nullptr;
    cl_int err =
      clBuildProgram(program, 1, &device_id, build_options, nullptr, nullptr);
    if (err == CL_BUILD_PROGRAM_FAILURE) {
      size_t log_len = 0;
      CL_COMMAND(clGetProgramBuildInfo,
        program, device_id, CL_PROGRAM_BUILD_LOG, 0, nullptr, &log_len);
      char *log = new char[log_len + 1];
      memset(log, 0, log_len + 1);
      CL_COMMAND(clGetProgramBuildInfo,
        program, device_id, CL_PROGRAM_BUILD_LOG, log_len, log, nullptr);
      fatal(file_name == nullptr ? "<immediate string>" : file_name,
        ": build failure\n", log);
      delete[] log;
    } else if (err != CL_SUCCESS) {
      fatal(file_name == nullptr ? "<immediate string>" : file_name,
        ": ", status_to_symbol(err), "\n");
    }
  }

  inline config::Source config::load_bits(
    std::string file_name, Source::Kind source_kind)
  {
    Source s;
    s.kind = source_kind;
    //
    std::ifstream ifs(file_name, std::ios::binary);
    if (!ifs.good()) {
      fatal(file_name, ": file not found");
    }

    ifs.seekg(std::ios_base::end);
    size_t len = ifs.tellg();
    ifs.seekg(std::ios_base::beg);
    s.bits.reserve(len);

    while (true) {
      int c = ifs.get();
      if (c == EOF)
        break;
      s.bits.push_back((char)c);
    }
    return s;
  }

  inline config::Source config::load_source(std::string _file_name) {
    size_t ext_off = _file_name.rfind('.');
    std::string ext;
    if (ext_off != std::string::npos)
      ext = _file_name.substr(ext_off);
    Source src;
    if (ext == ".cl" || ext == ".clc")
      src = load_bits(_file_name, Source::Kind::OPENCL_C);
    else if (ext == ".spv")
      src = load_bits(_file_name, Source::Kind::SPIRV);
    else if (ext == ".bin" || ext == ".ptx" || ext == ".o" || ext == ".elf")
      src = load_bits(_file_name, Source::Kind::BINARY);
    else
      fatal("cannot infer CL source kind from file extension");
    return src;
  }


  inline void config::construct(
    cl_device_id _dev_id,
    std::string _file_name,
    const Source &_source,
    std::string _build_options,
    cl_command_queue_properties qprops)
  {
    CL_COMMAND_CREATE(context,
      clCreateContext,
      nullptr, 1, &_dev_id, contextCallbackDispatcher, this);
    CL_COMMAND_CREATE(queue,
      clCreateCommandQueue, context, device_id, qprops);
    is_profiling_queue = ((qprops & CL_QUEUE_PROFILING_ENABLE) != 0);
    construct_with_queue_and_context(_dev_id, _file_name, _source,
                                     _build_options);
  }

  inline void config::construct_with_queue_and_context(
      cl_device_id _dev_id, std::string _file_name, const Source &_source,
      std::string _build_options)
  {
    const char *bits_s8 = _source.bits.data();
    const unsigned char *bits_u8 = (const unsigned char *)bits_s8;
    size_t len = _source.bits.size();
    if (_source.kind == Source::Kind::OPENCL_C) {
      CL_COMMAND_CREATE(program,
        clCreateProgramWithSource,
        context, 1, &bits_s8, &len);
    } else if (_source.kind == Source::Kind::SPIRV) {
#ifdef MINCL_ENABLE_SPIRV
      // including this forces the program to have v2.1 of OpenCL.dll
      CL_COMMAND_CREATE(program,
        clCreateProgramWithIL,
          context, (const void *)bits_s8, len);
#else
      fatal("mincl.hpp: SPIRV not supported");
#endif // MINCL_ENABLE_SPIRV
    } else if (_source.kind == Source::Kind::BINARY) {
      cl_int status = 0;
      CL_COMMAND_CREATE(program,
        clCreateProgramWithBinary,
        context, 1, &_dev_id, &len, &bits_u8, &status);
    } else {
      fatal("mincl.hpp: unknown source flavor");
    }
    //
    build_program(
      program,
      device_id,
      _build_options.c_str(),
      _file_name.c_str());
    //
    cl_uint num_kernels = 0;
    CL_COMMAND(clCreateKernelsInProgram,
      program, 0, nullptr, &num_kernels);
    auto_alloc<cl_kernel> ks(num_kernels);
    CL_COMMAND(clCreateKernelsInProgram,
      program, num_kernels, ks, nullptr);
    kernels.reserve(num_kernels);
    for (cl_uint i = 0; i < num_kernels; i++) {
      kernels.emplace_back(this, ks[i]);
    }
  }

  inline config::config(cl_device_id _dev_id, std::string _file_name,
                        std::string _file_contents, std::string _build_options,
                        cl_command_queue_properties qprops)
      : device_id(_dev_id), file_name(_file_name)
  {
    Source src {};
    src.kind = Source::Kind::OPENCL_C;
    src.bits.reserve(_file_contents.size());
    for (size_t i = 0; i < _file_contents.size(); i++)
      src.bits.push_back(_file_contents[i]);
    src.bits.push_back('\0');
    //
    construct(_dev_id, file_name, src, _build_options, qprops);
  }

  inline config::config(
    cl_device_id _dev_id,
    std::string _file_name,
    std::string _build_options,
    cl_command_queue_properties qprops)
    : device_id(_dev_id)
    , file_name(_file_name)
  {
    Source src = load_source(_file_name);
    //
    construct(_dev_id, _file_name, src, _build_options, qprops);
  }

#ifdef CL_VERSION_2_0
  inline config::config(
    cl_device_id _dev_id,
    std::string _file_name,
    std::string _build_options,
    const cl_queue_properties *_qprops)
    : device_id(_dev_id)
    , file_name(_file_name)
  {
    Source src = load_source(_file_name);
    //
    // check if it's a profiling queue
    is_profiling_queue = false;
    const cl_queue_properties *qp = _qprops;
    while (qp && *qp) {
      if (*qp == CL_QUEUE_PROPERTIES)
        is_profiling_queue |= (CL_QUEUE_PROFILING_ENABLE & *qp) != 0;
      qp += 2;
    }
    //
    CL_COMMAND_CREATE(context,
      clCreateContext,
      nullptr, 1, &_dev_id, contextCallbackDispatcher, this);
    CL_COMMAND_CREATE(queue,
      clCreateCommandQueueWithProperties, context, device_id, _qprops);
    //
    construct_with_queue_and_context(_dev_id, _file_name, src, _build_options);
  }
#endif // CL_VERSION_2_0


  inline config::~config() {
    for (buffer_base *b : buffers) {
      CL_COMMAND(clReleaseMemObject, b->memobj);
      delete b;
    }
    buffers.clear();

    for (auto k : kernels) {
      CL_COMMAND(clReleaseKernel, k.kernel);
    }
    kernels.clear();

    CL_COMMAND(clReleaseProgram, program);
    CL_COMMAND(clReleaseCommandQueue, queue);
    CL_COMMAND(clReleaseContext, context);
  }

  inline void config::save_binary(std::string file_name) const {
    std::string bin_file_name = file_name;
    //
    auto dot_ix = bin_file_name.rfind('.');
    if (dot_ix == std::string::npos) {
      // bin_file_name = bin_file_name.substr(0, dot_ix);
      if (is_nvidia_gpu(device_id)) {
        bin_file_name += ".ptx";
      } else {
        bin_file_name += ".bin";
      }
    }

    size_t bin_size = 0;
    CL_COMMAND(clGetProgramInfo,
      program, CL_PROGRAM_BINARY_SIZES, sizeof(bin_size), &bin_size, nullptr);
    char *bits = new char[bin_size];
    CL_COMMAND(clGetProgramInfo,
      program, CL_PROGRAM_BINARIES, sizeof(bits), &bits, nullptr);

    std::ofstream ofs(bin_file_name, std::ios::binary);
    if (!ofs.good())
      fatal(bin_file_name, ": failed to open output buffer file for writing");
    ofs.write((const char *)bits, bin_size);
    if (!ofs.good())
      fatal(bin_file_name, ": error writing file");

    delete[] bits;
  }

  inline config::kinstance &config::get_kernel(const std::string &kernel_name) {
    for (auto &k : kernels) {
      if (k.name == kernel_name)
        return k;
    }
    fatal(kernel_name, ": unable to find kernel");
    return kernels.front(); // unreachable
  }

  inline bool config::has_kernel(const std::string &kernel_name) const {
    for (auto &k : kernels)
      if (k.name == kernel_name)
        return true;
    return false;
  }
  inline const std::vector<config::kinstance> &config::get_kernels() const
  {
    return kernels;
  }

  template <typename E>
  inline buffer<E> config::buffer_allocate(size_t length, cl_mem_flags flags)
  {
    buffer<E> *be = new buffer<E>(queue, length, nullptr);
    CL_COMMAND_CREATE(be->memobj,
      clCreateBuffer,
      context, flags, be->length_in_bytes, nullptr);
    buffers.push_back(be);
    return *be;
  }

  template <typename E>
  inline buffer<E> config::buffer_allocate_init_const(
    size_t length, E init_const, cl_mem_flags flags)
  {
    buffer<E> be = buffer_allocate<E>(length, flags);
    //
    // TODO: could use clEnqueueFillBuffer if CL version is >=1.2
    buffer_mapping<E> bm(be, CL_MAP_WRITE);
    for (size_t i = 0; i < length; i++)
      bm.elems[i] = init_const;
    //
    return be;
  }

  template <typename E>
  inline buffer<E> config::buffer_allocate_init_mem(
    size_t length, const E *init, cl_mem_flags flags)
  {
    buffer<E> be = buffer_allocate<E>(length, flags);
    //
    buffer_mapping<E> bm(be, CL_MAP_WRITE);
    for (size_t i = 0; i < length; i++)
      bm.elems[i] = init[i];
    //
    return be;
  }

  template <typename E>
  inline buffer<E> config::buffer_allocate_init_lambda(
    size_t length, cl_mem_flags flags, std::function<E(size_t)> init)
  {
    buffer<E> be = buffer_allocate<E>(length, flags);
    //
    buffer_mapping<E> bm(be, CL_MAP_WRITE);
    for (size_t i = 0; i < length; i++)
      bm.elems[i] = init(i);
    //
    return be;
  }


  template <typename E>
  inline void config::buffer_deallocate(buffer<E> b) {
    auto itr = buffers.begin();
    while (itr != buffers.end()) {
      buffer_base *bb = *itr;
      if (bb->memobj == b.memobj) {
        CL_COMMAND(clReleaseMemObject, bb->memobj);
        bb->memobj = nullptr;
        delete bb;
        buffers.erase(itr);
        return;
      }
      itr++;
    }
    fatal("mincl.hpp: buffer_deallocate: failed to find buffer");
  }

  inline config::kinstance::kinstance(config *_parent, cl_kernel _kernel)
    : parent(_parent), kernel(_kernel)
  {
    size_t slen = 0;
    CL_COMMAND(clGetKernelInfo,
      kernel, CL_KERNEL_FUNCTION_NAME, 0, nullptr, &slen);
    auto_alloc<char> knm(slen + 1);
    CL_COMMAND(clGetKernelInfo,
      kernel, CL_KERNEL_FUNCTION_NAME, slen, knm, nullptr);
    knm[slen] = 0;
    name = knm;
  }

  template <typename T>
  inline void config::kinstance::set_arg_uniform(cl_uint arg_ix, T t) {
    CL_COMMAND(clSetKernelArg, kernel, arg_ix, sizeof(T), &t);
  }
  template <typename T>
  inline void config::kinstance::set_arg_mem(cl_uint arg_ix, buffer<T> &b)
  {
    CL_COMMAND(clSetKernelArg, kernel, arg_ix, sizeof(cl_mem), &b.memobj);
  }
  inline void config::kinstance::set_arg_slm(cl_uint arg_ix, size_t slm_len)
  {
    CL_COMMAND(clSetKernelArg, kernel, arg_ix, sizeof(slm_len), &slm_len);
  }

  // dispatch a kernel (clEnqueueNDRangeKernel)
  inline config::kdispatch config::kinstance::dispatch(
    ndr gsz, ndr lsz, cl_event *evt)
  {
    const size_t *lsz_ptr = lsz.num_dims == 0 ? nullptr : &lsz.dims[0];
    cl_event auto_event = nullptr;
    if (evt == nullptr && parent->is_profiling_queue) {
      // we need to create the event for them since we're profiling
      evt = &auto_event;
    }
    CL_COMMAND(clEnqueueNDRangeKernel,
      parent->queue, kernel, (cl_uint)gsz.num_dims,
      nullptr, &gsz.dims[0], lsz_ptr,
      0, nullptr, evt);
    return kdispatch(
      parent->queue,
      evt ? *evt : nullptr,
      parent->is_profiling_queue);
  }

  inline cl_long config::kdispatch::get_prof_ns() const {
    if (!is_profiling_queue)
      fatal("mincl.hpp: kdispatch::get_prof_ns() underlying queue must be "
            "profiling");
    else if (prof_ns == -1)
      fatal("mincl.hpp: kdispatch::get_prof_ns() lacks prof time (sync on it "
            "first)");
    else if (event == nullptr)
      fatal("mincl.hpp: kdispatch::get_prof_ns() dispatch needs an event for "
            "this");
    return prof_ns;
  }

  inline cl_long config::kdispatch::sync() {
    if (event) {
      CL_COMMAND(clWaitForEvents, 1, &event);
    } else {
      CL_COMMAND(clFinish, queue);
    }
    if (is_profiling_queue && event != nullptr) {
      cl_ulong ts = 0, te = 0;
      CL_COMMAND(clGetEventProfilingInfo,
        event, CL_PROFILING_COMMAND_START, sizeof(ts), &ts, nullptr);
      CL_COMMAND(clGetEventProfilingInfo,
        event, CL_PROFILING_COMMAND_END, sizeof(te), &te, nullptr);
      prof_ns = te - ts;
    }

    auto now = std::chrono::high_resolution_clock::now();
    host_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(
      now - started).count();

    if (event && is_profiling_queue) {
      return prof_ns;
    } else {
      return host_ns;
    }
  }

  inline cl_int config::kdispatch::status() const {
    if (!event)
      fatal("mincl.hpp: poll()/status() on kernel dispatch without event");
    cl_int status = 0;
    CL_COMMAND(clGetEventInfo,
      event,
      CL_EVENT_COMMAND_EXECUTION_STATUS,
      sizeof(status),
      &status,
      nullptr);
    return status;
  }
} //  mincl namespace
#endif // MINCL_HPP