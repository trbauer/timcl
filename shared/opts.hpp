#ifndef OPTS_HPP
#define OPTS_HPP

#include "mincl.hpp"

#include <vector>
#include <string>
#include <sstream>

// common instruction options
struct common_opts {
  std::vector<cl_device_id>   devices;
  std::string                 extra_build_options;
  //
  int                         seed = 2020;
  int                         verbosity = 0;

  bool verbose() const {return verbosity >= 1;}
  bool debug() const {return verbosity >= 2;}

  static std::string commonOptsHelp();
  bool tryParseCommonOpt(std::string arg, std::string key, std::string val);
  void parseCommonOpt(std::string arg, std::string key, std::string val);
};

inline std::string common_opts::commonOptsHelp()
{
  static const common_opts DEFAULTS;

  std::stringstream ss;
  ss <<
    "  -b=STRING      adds extra build options\n"
    "  -d=INT/STRING  specifies a device index (linear) or "
    "device name substring\n"
    "  -h             prints this\n"
    "  -s=SEED        random number generator seed for initial data\n"
    "                 (defaults to " << DEFAULTS.seed << ")\n"
    "  -v/-v=INT      sets verbosity\n";
  return ss.str();
}

inline bool common_opts::tryParseCommonOpt(
  std::string arg, std::string key, std::string val)
{
  if (key == "-b=") {
    if (!extra_build_options.empty())
      extra_build_options += ' ';
    extra_build_options += val;
    return true;
  } else if (key == "-d=") {
    devices.push_back(mincl::find_device(val));
    return true;
  } else if (key == "-s=") {
    seed = (int)mincl::parse_int64(val, "seed integer", false);
    return true;
  } else if (key == "-v") {
    verbosity = 1;
    return true;
  } else if (key == "-v2") {
    verbosity = 2;
    return true;
  } else if (key == "-v=") {
    verbosity = (int)mincl::parse_int64(val);
    return true;
  } else if (key == "-d" || key == "-b" || key == "-s") {
    mincl::fatal(arg, ": must be of the form ", key, "=...");
  }
  return false;
}

inline void common_opts::parseCommonOpt(
  std::string arg, std::string key, std::string val)
{
  if (tryParseCommonOpt(arg, key, val)) {
    return;
  } else if (key.size() > 0 && key[0] == '-') {
    mincl::fatal(arg, ": unrecognized option");
  } else {
    mincl::fatal(arg, ": unrecognized argument");
  }
}

#endif // OPTS_HPP