#include "mincl.hpp"

#include "lodepng.h"

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <limits>
#include <string>

using namespace mincl;

static const int DEFAULT_QUEUE_SIZE = 16*1024*1024;
static const char *DEFAULT_OUTPUT_FILE = "output.png";
static const char *DEFAULT_KERNEL_FILE = "sierpinski.cl";
static const int DEFAULT_WIDTH = 6561;
static const int DEFAULT_ITERATIONS = 1;

int main(int argc, char **argv)
{
  int device_index = 0;
  std::string kernel_file = DEFAULT_KERNEL_FILE;
  std::string build_opts;
  int queue_size = DEFAULT_QUEUE_SIZE;
  cl_int width = DEFAULT_WIDTH;
  int iterations = DEFAULT_ITERATIONS;

  std::string output_file = DEFAULT_OUTPUT_FILE;

  for (int ai = 1; ai < argc; ai++) {
    std::string arg = argv[ai];
    std::string arg_val;
    auto off = arg.find('=');
    if (off != std::string::npos) {
      arg_val = arg.substr(off+1);
    }
    auto parseIntArg = [&] () {
      return (int)mincl::parse_int64(arg_val, "malformed integer");
    };

    if (arg == "-h" || arg == "--help") {
      std::cout <<
        "Sierpinksi Tester " << SIERPINSKI_VERSION_STRING << "\n" <<
        " " << arg << " OPTIONS\n" <<
        "where OPTIONS are:\n" <<
        "  -b=STRING      adds extra build options "
                            "(-cl-std=CL2.0 is already included)\n" <<
        "  -d=INT         given a device index\n"

        "  -f=FILE        sets the .cl file "
          "(defaults to " << DEFAULT_KERNEL_FILE << ")\n"

        "  -o=OUTPUT      writes output image "
          "(defaults to " << DEFAULT_OUTPUT_FILE << ")\n"

        "  -qs=INT        device queue size " <<
        "(defaults to " << DEFAULT_QUEUE_SIZE << ")\n"

        "  -w=WIDTH       sets the width and height "
          "(defaults to " << DEFAULT_WIDTH << ")\n"
        "\n"
        "  -i=ITRS        runs this many iterations (defaults to " <<
          DEFAULT_ITERATIONS << ")\n"
        "  -h             prints the help banner\n";
      exit(EXIT_SUCCESS);
    } else if (arg.substr(0,3) == "-b=") {
      if (!build_opts.empty())
        build_opts += ' ';
      build_opts += arg_val;
    } else if (arg.substr(0,3) == "-d=") {
      device_index = parseIntArg();
    } else if (arg.substr(0,3) == "-f=") {
      kernel_file = arg_val;
    } else if (arg.substr(0,3) == "-i=") {
      iterations = parseIntArg();
    } else if (arg.substr(0,4) == "-qs=") {
      queue_size = parseIntArg();
    } else if (arg.substr(0,3) == "-w=") {
      width = parseIntArg();
    } else {
      fatal(arg, ": unxpected argument");
    }
  }
  cl_int height = width;

  auto all_ds = all_devices_with_names();
  if (device_index >= all_ds.size())
    fatal(device_index, ": device index out of bounds");

  std::cout << "width x height: " << width << " x " << height << "\n";
  std::cout << "using device: " << all_ds[device_index].second << "\n";
  std::cout << "and build options: " <<
     (build_opts.empty() ? "nullptr" : build_opts) << "\n";

  /////////////////////////////////////////////////////////////////////////////
  //
  std::string bops = "-cl-std=CL2.0";
  if (!build_opts.empty()) {
    bops += " ";
    bops += build_opts;
  }

  cl_device_id dev_id = all_ds[device_index].first;
  config cfg(
    dev_id,
    kernel_file.c_str(),
    bops.c_str());
  auto clver = get_device_info_string(cfg.getDeviceId(), CL_DEVICE_VERSION);
  auto hasSs = [&](const char *ss) {
      return clver.find(ss) != std::string::npos;
    };
  if (hasSs("OpenCL 1.0") || hasSs("OpenCL 1.1") || hasSs("OpenCL 1.2")) {
    std::cerr << "device does not support device-side enqueue\n";
    return EXIT_FAILURE;
  }

  // create and set the device queue for a given context and device
  static const cl_queue_properties qprops[] {
    CL_QUEUE_SIZE, (cl_queue_properties)queue_size,
    CL_QUEUE_PROPERTIES,
    (cl_command_queue_properties)(
      CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE |
      CL_QUEUE_ON_DEVICE |
      CL_QUEUE_ON_DEVICE_DEFAULT),
    0
  };
  cl_command_queue dev_queue;
  CL_COMMAND_CREATE(dev_queue,
    clCreateCommandQueueWithProperties,
      cfg.getContext(), cfg.getDeviceId(), qprops);
  cl_int qsz = 0;
  CL_COMMAND(clGetCommandQueueInfo,
    dev_queue, CL_QUEUE_SIZE, sizeof(cl_int), &qsz, 0);
  std::cout << "CL_QUEUE_SIZE: " << qsz << "\n";

  /////////////////////////////////////////////////////////////////////////////
  //
  const cl_int zero = 0;
  auto buf = cfg.buffer_allocate<uint8_t>(width*height, CL_MEM_WRITE_ONLY);
  auto krn = cfg.get_kernel("sierpinski_mix");
  krn.set_arg_mem(0, buf);
  krn.set_arg_uniform(1, width);
  krn.set_arg_uniform(2, zero);
  krn.set_arg_uniform(3, zero);
  //
  int64_t total_ns = 0;
  for (int i = 0; i < iterations; i++) {
    // we sync between each because we are not using profiling queues
    // and we need to sync to return a host elapsed sample time
    auto d = krn.dispatch(ndr(width,height));
    total_ns += d.sync();
  }

  // timing run
  double avg_ns = iterations == 0 ?
      std::numeric_limits<double>::quiet_NaN() : (double)(total_ns/iterations);
  std::cout << "FPS: " <<
    std::setw(8) << std::setprecision(5) << std::fixed <<
      (1000000000.0/avg_ns);
  std::cout << " (host timing)\n";
  //
  buf.read(
    [&] (const uint8_t *buf) {
      // for (int hi = 0; hi < height; hi++) {
      //   for (int wi = 0; wi < width; wi++) {
      //     std::cout << (buf[hi*width + wi] != 0 ? "X" : " ");
      //   }
      //   std::cout << "\n";
      // }
      auto err = lodepng_encode_file(
        output_file.c_str(),
        buf,
        (unsigned)width,
        (unsigned)height,
        LCT_GREY,
        8);
      if (err) {
        fatal(output_file, ": failed to encode file");
      }
    });
  std::cout << "wrote: " << output_file << "\n";

  return EXIT_SUCCESS;
}