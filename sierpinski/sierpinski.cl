constant unsigned int BLACK = 0x00;
constant unsigned int WHITE = 0xFF;


unsigned int isSierpinskiCarpetPixelFilled(int x, int y, int depth) {
  while (x > 0 || y > 0) {
    if (x % 3 == 1 && y % 3 == 1)
      return BLACK;
    x /= 3;
    y /= 3;
  }
  return WHITE/depth;
}
__kernel void sierpinski_iterate(
  __global char *src, int width, int offsetx, int offsety, int depth)
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  src[(y + offsety) * width + (x + offsetx)] =
      isSierpinskiCarpetPixelFilled(x, y, depth);
}
__kernel void sierpinski_mix_with_depth(
  __global char *src, int width, int offsetx, int offsety, int depth)
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  queue_t q = get_default_queue();
  int one_third = (int)get_global_size(0) / 3;
  int two_thirds = 2*one_third;
  if (x >= one_third && x < two_thirds && y >= one_third && y < two_thirds) {
    src[(y + offsety) * width + (x + offsetx)] = BLACK;
  } else {
    if (one_third > 81) {
      if (x % one_third == 0 && y % one_third == 0) {
        const size_t grid[2] = {one_third, one_third};
        const size_t lrid[2] = {get_local_size(0), get_local_size(1)};
        enqueue_kernel(q, 0, ndrange_2D(grid, lrid), ^{
          sierpinski_mix_with_depth(
            src, width, x + offsetx, y + offsety, depth + 1);
        });
      }
    } else {
      sierpinski_iterate(src, width, offsetx, offsety, depth);
    }
  }
}

__kernel void sierpinski_mix(
  __global char *src, int width, int offsetx, int offsety)
{
  sierpinski_mix_with_depth(src, width, offsetx, offsety, 1);
}
